﻿using AutoserviceDiplom.Models;
using PagedList;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;

namespace AutoserviceDiplom.Controllers
{
    [Authorize]
    public class CarController : Controller
    {
        autoserviceDEntities db = new autoserviceDEntities();
        SelectList transmiss;
        List<int> years = new List<int>();
        public CarController()
        {   
            List<string> tr = new List<string> { "Механическая (МКПП)", "Автоматическая (АКПП)", "Роботизированная (РКПП)", "Вариаторная КПП (Вариатор)" };
            transmiss = new SelectList(tr,tr);
            
            for (int y = 1920; y <= DateTime.Now.Year; y++)
            { years.Add(y); }
            
        }

        [HttpGet]
        public ActionResult Search()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Search(string vin, string mark, string regNumber, int? page)
        {
            vin = vin ?? "";
            mark = mark ?? "";
            regNumber = regNumber ?? "";
            List<Car> cars = db.Cars.Where(s => s.VIN_number.Contains(vin) && s.carModification.carSery.series_name.Contains(mark) && s.Registr_Number.Contains(regNumber)).ToList();
            if (cars.Count == 0)
            { Response.Write("Нет данных по данному запросу..."); return null; }
            else
            {
                int pageSize = 6;
                int pageNumber = (page ?? 1);
                return PartialView("SearchResults",cars.ToPagedList(pageNumber, pageSize));
            }
        }

        public ActionResult AutoCompleteSearch(string term)
        {
            var VINnumb = db.Cars.Where(c => c.VIN_number.Contains(term)).Select(c => new { value = c.VIN_number}).Distinct().Take(15);
            return Json(VINnumb, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult AttachCar(string vin)
        {
            var VINnumb = db.Cars.Where(c => c.VIN_number == vin).Select(v => new {model=db.CarListFulls.Where(f=>f.modification_id==v.modification_id).FirstOrDefault().Model, number=v.Registr_Number, vnumb=v.VIN_number});
            return Json(VINnumb);
        }
        public ActionResult Create()
        {
            List<string> lst = new List<string>(); lst.Add("Выбрать...");
            ViewBag.emptylist = new SelectList(lst, lst);
            CarEditerViewModel carEdit = new CarEditerViewModel();

            carEdit.BrandCarList = new SelectList(db.carBrands, "brand_id", "brand_name");
            carEdit.BodyColorList = new SelectList(db.carColor_table, "id_color", "colorNameDescription");
            ViewBag.Transmiss = transmiss;
            return PartialView(carEdit);
        }
        [HttpPost]
       [ValidateAntiForgeryToken]
        public ActionResult Create(CarEditerViewModel car)
        {
            List<string> lst = new List<string>(); lst.Add("Выбрать...");
            ViewBag.emptylist = new SelectList(lst, lst);
            var carExist = db.Cars.Find(car.VIN_number);
            if (ModelState.IsValid && carExist == null)
            {
                Car carmodel = this.FillCarModelForSave(car);
                db.Entry(carmodel).State = EntityState.Added;
                try
                {
                    db.SaveChanges();
                    return null;
                }
                catch {
                    return null;
                }
            }
            ViewBag.Transmiss = transmiss;
            car.BodyColorList = new SelectList(db.carColor_table, "id_color", "colorNameDescription");
            car.BrandCarList = new SelectList(db.carBrands, "brand_id", "brand_name");
            return PartialView(car);

        }
        [HttpGet]
        public ActionResult Edit(string VIN)
        {
            CarEditerViewModel carEdit = new CarEditerViewModel();
            if (this.FillCarModel(carEdit, VIN) > 0)
            {
                ViewBag.Transmiss = transmiss;
                return PartialView(carEdit);
            }
            else
                return HttpNotFound();    
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CarEditerViewModel car)
        {
            if (ModelState.IsValid)
            {
                Car carmodel = this.FillCarModelForSave(car);
                db.Entry(carmodel).State = EntityState.Modified;
                try
                {
                    db.SaveChanges(); return null;
                }
                catch
                {
                    return null;
                }
            }
            ViewBag.Transmiss = transmiss;
            List<string> lst = new List<string>(); lst.Add("Выбрать...");
            ViewBag.emptylist = new SelectList(lst, lst);
            car.BodyColorList = new SelectList(db.carColor_table, "id_color", "colorNameDescription");
            car.BrandCarList = new SelectList(db.carBrands, "brand_id", "brand_name");
            return PartialView(car);
            
        }
        //////////
        [HttpGet]
        public ActionResult EditFull(string VIN)
        {
            ViewBag.Transmiss = transmiss;
            CarEditerViewModel carEdit = new CarEditerViewModel();
            if (this.FillCarModel(carEdit, VIN)>0)
            {
                return View(carEdit);
            }
            else
            return HttpNotFound();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditFull(CarEditerViewModel car)
        {
            if (ModelState.IsValid)
            {
                //byte[] imageData = null;
                //HttpPostedFileBase viewCarSource- аргумент
                //// считываем переданный файл в массив байтов
                //using (var binaryReader = new BinaryReader(viewCarSource.InputStream))
                //{
                //    imageData = binaryReader.ReadBytes(viewCarSource.ContentLength);
                //}
                //// установка массива байтов
                //car.ViewCar = imageData;
                Car carmodel = this.FillCarModelForSave(car);
                db.Entry(carmodel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Detail", "Car", new {vin= car.VIN_number });
            }
            ViewBag.Transmiss = transmiss;
            List<string> lst= new List<string>();lst.Add("Выбрать...");
            ViewBag.emptylist = new SelectList(lst, lst);
            car.BodyColorList = new SelectList(db.carColor_table, "id_color", "colorNameDescription");
            car.BrandCarList = new SelectList(db.carBrands, "brand_id", "brand_name");
            return View(car);
        }
        [HttpGet]
        //////////
        public ActionResult Detail(string vin)
        {
            var car = db.Cars.Find(vin);
            if (car != null)
            {
               
                CarListFull carlist = db.CarListFulls.Where(s => s.modification_id == car.modification_id).FirstOrDefault();
                CarViewModel carModel = new CarViewModel
                {
                    VIN_number = car.VIN_number,
                    DataSheetCar = car.DataSheetCar,
                    IssueYear = car.IssueYear,
                    OwnerName = car.OwnerName,
                    TransmissionType = car.TransmissionType,
                    BodyColorCode = car.carColor_table.color_hex,
                    BodyPaintName = car.carColor_table.colorName,
                    BodyColorName = car.carColor_table.colorNameDescription,
                    BrandCar = carlist.brand_name,
                    ModelCar = carlist.model_name,
                    SeriesCar = carlist.series_name,
                    ModifCar = carlist.modification_name,
                    CountRepare = car.OrderServs.Count,
                    ViewCar = db.carBrands.Find(carlist.brand_id).view_brand,
                    Registr_Number = car.Registr_Number
                };

                if (Request.UrlReferrer.Segments[2] != "Search" && Request.UrlReferrer.Segments[2] != "EditFull")
                    return PartialView(carModel);
                else
                {
                    

                    return View("DetailFull", carModel);
                }
                    
            }
            else
                return HttpNotFound("В БД нет Автомобиля с VIN номером: "+vin);
        }

        public ActionResult Delete(string vin)
        {
            Car car = db.Cars.Find(vin);
            if (car != null)
            {
                if (car.OrderServs.Count > 0)
                {
                    db.Entry(car).State = EntityState.Deleted;
                    db.SaveChanges();
                    return View("Search");
                }
                else
                    return null;
            }
            else
                return HttpNotFound("В БД нет Автомобиля с VIN номером: " + vin);
        }
        ////////
        public ActionResult CreateFull()
        {

            List<string> lst = new List<string>(); lst.Add("Выбрать...");
            ViewBag.emptylist = new SelectList(lst, lst);
            CarEditerViewModel carEdit = new CarEditerViewModel();

                carEdit.BrandCarList = new SelectList(db.carBrands, "brand_id", "brand_name");
                carEdit.BodyColorList = new SelectList(db.carColor_table, "id_color", "colorNameDescription");
                ViewBag.Transmiss = transmiss;
                return View(carEdit);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateFull(CarEditerViewModel car)
        {
            List<string> lst = new List<string>(); lst.Add("Выбрать...");
            ViewBag.emptylist = new SelectList(lst, lst);
            var carExist = db.Cars.Find(car.VIN_number);
            if (ModelState.IsValid && carExist==null)
            {
                Car carmodel = this.FillCarModelForSave(car);
                db.Entry(carmodel).State = EntityState.Added;
                db.SaveChanges();
                return RedirectToAction("Detail", "Car", new { vin = car.VIN_number });
            }
            ViewBag.Transmiss = transmiss;
            car.BodyColorList = new SelectList(db.carColor_table, "id_color", "colorNameDescription");
            car.BrandCarList = new SelectList(db.carBrands, "brand_id", "brand_name");
            return View(car);
        }

        [HttpPost]
        public ActionResult GetSelectList(int? selectedValue, string nameSelectList) 
        {
            if (selectedValue.HasValue && nameSelectList != null)
                switch (nameSelectList)
                {
                    case "IDBrandCar":
                        {
                            var ModelCarList = new SelectList(db.carModels.Where(f => f.brand_id == selectedValue), "model_id", "model_name");
                            return PartialView("model_ddl_partial", ModelCarList);
                        }
                    case "IDModelCar":
                        {
                            var ModelCarList = new SelectList(db.carSeries.Where(f => f.model_id == selectedValue), "series_id", "series_name");
                            return PartialView("model_ddl_partial", ModelCarList);
                        }
                    case "IDSeriesCar":
                        {
                            var ModelCarList = new SelectList(db.carModifications.Where(f => f.series_id == selectedValue), "modification_id", "modification_name");
                            return PartialView("model_ddl_partial", ModelCarList);
                        }
                    case "IDModifCar":
                        {
                            var carmodif = db.carModifications.Find(selectedValue);
                            if (carmodif != null)
                            {
                                var ModelCarList = new SelectList(years.Where(item => item >= carmodif.start_production_year && item <= (carmodif.end_production_year??DateTime.Now.Year)), years);
                                return PartialView("model_ddl_partial", ModelCarList);
                            }
                            else return null;
                        }
                    default: return null;
                }
            else
                return null;
        }

        private int FillCarModel(CarEditerViewModel carEditModel, string VIN)
        {
            Car car = db.Cars.Find(VIN);
            if (car != null)
            {
                
                CarListFull carmodif = db.CarListFulls.Where(f => f.modification_id == car.modification_id).FirstOrDefault();
                carEditModel.VIN_number = car.VIN_number;
                carEditModel.DataSheetCar = car.DataSheetCar;
                carEditModel.Registr_Number = car.Registr_Number;
                carEditModel.OwnerName = car.OwnerName;
                carEditModel.IDBrandCar = carmodif.brand_id;
                carEditModel.BrandCarList = new SelectList(db.carBrands, "brand_id", "brand_name");
                carEditModel.IDModelCar = carmodif.model_id;
                carEditModel.ModelCarList = new SelectList(db.carModels.Where(f => f.brand_id == carmodif.brand_id), "model_id", "model_name");
                carEditModel.IDSeriesCar = carmodif.series_id;
                carEditModel.SeriesCarList = new SelectList(db.carSeries.Where(f => f.model_id == carmodif.model_id), "series_id", "series_name");
                carEditModel.IDModifCar = carmodif.modification_id;
                carEditModel.ModifCarList = new SelectList(db.carModifications.Where(f => f.series_id == carmodif.series_id), "modification_id", "modification_name");
                carEditModel.BodyColorId = car.id_body_color;
                carEditModel.BodyColorList = new SelectList(db.carColor_table, "id_color", "colorNameDescription");
                carEditModel.IssueYearList = new SelectList(years.Where(item => item >= carmodif.modificationYearStart && item <= (carmodif.modificationYearEnd ?? DateTime.Now.Year)), years);
                return 1;
            }
            else return -1;
        }
        private Car FillCarModelForSave(CarEditerViewModel car)
        {
            Car carmodel = new Car
            {
                VIN_number = car.VIN_number,
                id_body_color = car.BodyColorId,
                modification_id = car.IDModifCar,
                OwnerName = car.OwnerName,
                Registr_Number = car.Registr_Number,
                DataSheetCar = car.DataSheetCar,
                TransmissionType = car.TransmissionType,
                IssueYear = car.IssueYear
            };
            return carmodel;
        }
        ////////
    }
}