﻿function showMenu() {
    if ($('#menuInvoice').css('display') == 'none') {
        $('#menuInvoice').slideDown(400);
        setTimeout(function () {
            $('#menuInvoice').slideUp(400);
        }, 5000);
    }
}
function showFindSearch() {
    if ($('#row_find_part').css('display') == 'none')
        $('i[onclick="extend()"]').click();
}

function DeleteInvoice() {
    $.ajax(
   {
       url: '/AcceptanceCustom/DeleteInvoice',
       data: 'id_accept=' + $('#idAccept').val(),
       type: "Post",
       datatype: "json"
   })
}

function EditInvoice() {
    $.ajax(
    {
        url: '/AcceptanceCustom/EditInvoice',
        data: 'id_accept=' + $('#idAccept').val(),
        type: "Post",
        datatype: "json",
        success: function (result) {
            if (result != '')
                $('#invoice').html(result);
        }
    })
}
function ClearInvoice() {
    $.ajax(
    {
        url: '/AcceptanceCustom/ClearInvoice',
        data: 'id_accept=' + $('#idAccept').val(),
        type: "Get",
        datatype: "json",
        success: function (result) {
            if (result.indexOf('table') != -1)
                $('#updTable').html(result);
        }
    })
}
//функция для  удаления запчасти из акта приемки
function del(id_prt) {
    $.ajax(
    {
        url: '/AcceptanceCustom/Delete',
        data: 'id_prt=' + id_prt + '&' + 'id_accept=' + $('#idAccept').val(),
        type: "Post",
        datatype: "json",
        success: function (result) {
            if (result.indexOf('table') != -1)
                $('#updTable').html(result);
        }
    })
}