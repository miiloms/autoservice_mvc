﻿$().ready(function () {
    $('#btn-ajax').click();
})
var data = {
    labels: [],
    datasets: [
        {
            label: "Число заказов за период",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(75,192,192,1)",
            borderColor: "rgba(75,192,192,1)",
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            data: [],
            pointRadius: 1,
            pointHitRadius: 10,
            spanGaps: true,
        }
    ]
};
var ctx = document.getElementById("myChart");
var myLineChart = new Chart(ctx, {
    type: 'line',
    data: data,
    options: {
        title: {
            display: false,
            text: 'Число заказов за период'
        },
        responsive: true
    }
});
function UpdateDiagram(result) {
    var listMonth = new Array();
    for (key in result) {
        listMonth.push(result[key].nameMonth == null ? result[key].x + ' ' + $('#month option:selected').text() : result[key].nameMonth);
    }
    data.datasets[0].data = result;
    data.labels = listMonth;
    myLineChart.update();
}