﻿function attachToList() {
    $('#ddlist option').each(function () {
        $(this).removeAttr('selected');
    });
    $("#ddlist").prepend($('<option value=' + $("#VIN_number").val() + '>' + $("#IDBrandCar option:selected").text() + '(' + $("#Registr_Number").val() + ')' + '</option>'));
    $("#ddlist :first").attr("selected", "selected");
    $('#modal_close').click();
}
$().ready(function () {
    //для маски vin кода
    try{
        $('#formcar input[name="VIN_number"]').mask("*****************");
    }catch(e){};
    //для манипуляциями дроп даун листов
    $('#formcar').on('change', '#IDBrandCar, #IDModelCar, #IDSeriesCar,#IDModifCar', function () {
        var selectedValue = $(this).val();
        var nameSelectList = $(this).attr('name');
        $.ajax(
        {
            url: '/Car/GetSelectList',
            data: { 'selectedValue': selectedValue, 'nameSelectList': nameSelectList },
            type: "POST",
            datatype: "json",
            success: function (result) {
                if (result != '') {
                    switch (nameSelectList) { //замена соответствующего дроп даун листа
                        case "IDBrandCar":
                            {
                                $('#IDSeriesCar,#IDModifCar, #IssueYear').attr('disabled', 'disabled').empty();
                                $("#IDModelCar").html(result).removeAttr('disabled'); break;
                            }
                        case "IDModelCar":
                            {
                                $('#IDModifCar, #IssueYear').attr('disabled', 'disabled').empty();
                                $("#IDSeriesCar").html(result).removeAttr('disabled'); break;
                            }
                        case "IDSeriesCar":
                            {
                                $('#IssueYear').attr('disabled', 'disabled').empty();
                                $("#IDModifCar").html(result).removeAttr('disabled'); break;
                            }
                        case "IDModifCar":
                            {
                                $("#IssueYear").html(result).removeAttr('disabled'); break;
                            }
                        default: return null;
                    }
                }
            }
        })
    })
})

//for close modal window
function attachToList() {
    $('#modal_close').click();
}