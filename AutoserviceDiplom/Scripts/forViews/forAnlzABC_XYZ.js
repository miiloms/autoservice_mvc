﻿    $().ready(function () {
        ////для визуализации анализа
        var arr = [];
        $('table tr:has(td)').each(function () {
            if ($(this).children().eq(4).text() == 'A')
                $(this).children().eq(4).addClass('anlzA');
            if ($(this).children().eq(4).text() == 'B')
                $(this).children().eq(4).addClass('anlzB');
            if ($(this).children().eq(4).text() == 'C')
                $(this).children().eq(4).addClass('anlzC');

            if ($(this).children().eq(5).text() == 'X')
                $(this).children().eq(5).addClass('anlzA');
            if ($(this).children().eq(5).text() == 'Y')
                $(this).children().eq(5).addClass('anlzB');
            if ($(this).children().eq(5).text() == 'Z')
                $(this).children().eq(5).addClass('anlzC');

            if ($(this).children().eq(6).text() == 'AZ') {
                $(this).children().eq(6).addClass('anlzAZ');
                var df = arr.indexOf('AZ');
                if (arr.indexOf('AZ') < 0)
                    arr.push('AZ');
            }

            if ($(this).children().eq(6).text() == 'BZ') {
                $(this).children().eq(6).addClass('anlzBZ');
                if (arr.indexOf('BZ') < 0)
                    arr.push('BZ');
            }
            if ($(this).children().eq(6).text() == 'CZ') {
                $(this).children().eq(6).addClass('anlzCZ');
                if (arr.indexOf('CZ') < 0)
                    arr.push('CZ');
            }

            if ($(this).children().eq(6).text() == 'AX') {
                $(this).children().eq(6).addClass('anlzAX');
                var df = arr.indexOf('AX');
                if (arr.indexOf('AX') < 0)
                    arr.push('AX');
            }

            if ($(this).children().eq(6).text() == 'BX') {
                $(this).children().eq(6).addClass('anlzBX');
                if (arr.indexOf('BX') < 0)
                    arr.push('BX');
            }
            if ($(this).children().eq(6).text() == 'CX') {
                $(this).children().eq(6).addClass('anlzCX');
                if (arr.indexOf('CX') < 0)
                    arr.push('CX');
            }
            if ($(this).children().eq(6).text() == 'AY') {
                $(this).children().eq(6).addClass('anlzAY');
                var df = arr.indexOf('AY');
                if (arr.indexOf('AY') < 0)
                    arr.push('AY');
            }

            if ($(this).children().eq(6).text() == 'BY') {
                $(this).children().eq(6).addClass('anlzBY');
                if (arr.indexOf('BY') < 0)
                    arr.push('BY');
            }
            if ($(this).children().eq(6).text() == 'CY') {
                $(this).children().eq(6).addClass('anlzCY');
                if (arr.indexOf('CY') < 0)
                    arr.push('CY');
            }
        })
        /////// walk by everything items and  call function for render item-info
        var timeOut = 0;
        arr.forEach(function (elem) {
            switch (elem) {
                case 'AZ': createItemInfo('#974478', '#B15E92', 0, elem, 'Поставки = мин. партиями<br>Поставщики = дистрибьютеры<br>Контроль = ежедневно', timeOut); break;
                case 'BZ': createItemInfo('#F8C301', '#FFE422', 0, elem, 'Поставки = мин. партиями<br>Поставщики = дистрибьютеры<br>Контроль = 2 раза в неделю', timeOut); break;
                case 'CZ': createItemInfo('#DA251C', '#ED564D', 1, elem, 'Позиции данного раздела<br>исключаются из ассортимента', timeOut); break;

                case 'AX': createItemInfo('#00923F', '#17A956', 55, elem, 'Поставки = ритмично<br>Поставщики = производители<br>Контроль = ежедневно', timeOut); break;
                case 'BX': createItemInfo('#0093DD', '#1DB0FA', 55, elem, 'Поставки = ритмично<br>Поставщики = производители<br>Контроль = 2 раза в неделю', timeOut); break;
                case 'CX': createItemInfo('#BAB223', '#CCC435', 55, elem, 'Поставки = редко, макс. партиями<br>Поставщики = производители<br>Контроль = раз в неделю', timeOut); break;

                case 'AY': createItemInfo('#797196', '#938BB0', 30, elem, 'Поставки = умерен. партиями<br>Поставщики = производители<br>Контроль = ежедневно', timeOut); break;
                case 'BY': createItemInfo('#42929D', '#5EAEB9', 30, elem, 'Поставки = умерен. партиями<br>Поставщики = производители<br>Контроль = 2 раза в неделю', timeOut); break;
                case 'CY': createItemInfo('#F19ABD', '#FFA8CB', 30, elem, 'Поставки = редко, макс. партиями<br>Поставщики = дистрибьютеры<br>Контроль = раз в неделю', timeOut); break;
            }
            timeOut += 400;
        })
    })