﻿$(document).ready(function () {
    //Инициализация dtPick_From и dtPick_Before
    $(".date").datetimepicker({ language: 'ru', pickTime: false, format: 'YYYY-MM-DD' });
    //При изменении даты в dtPick_From, она устанавливается как минимальная для dtPick_Before
    $("#dtPick_From").on("dp.change", function (e) {
        $("#dtPick_Before").data("DateTimePicker").setMinDate(e.date);
    });
    //При изменении даты в dtPick_Before, она устанавливается как максимальная для dtPick_From
    $("#dtPick_Before").on("dp.change", function (e) {
        $("#dtPick_From").data("DateTimePicker").setMaxDate(e.date);
    });

    //для пейджинга-сброс номера отсылаемой страницы при изменении строки поиска
    $('#form-search input').change(function () {
        $('#page').attr('value', '')
    })

    //for checkbox - it should has three state (true, false and indeterminate)
    $('.checkbox').prop("indeterminate", true);
    $('.checkbox').change(function () {
        switch ($(this).attr('value')) {
            case 'true':
                $(this).prop('checked', true).attr('value', false).prop("indeterminate", false);
                $('.checkbox-custom').removeClass('checkbox-custom-checked'); break;
            case 'false': $(this).prop("indeterminate", true).attr('value', '');
                $('.checkbox-custom').removeClass('checkbox-custom-checked'); break;
            case '': $(this).prop('checked', true).attr('value', true).prop("indeterminate", false);
                $('.checkbox-custom').addClass('checkbox-custom-checked'); break;
        }
    })
})