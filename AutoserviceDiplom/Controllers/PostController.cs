﻿using AutoserviceDiplom.Models;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace AutoserviceDiplom.Controllers
{
    [Authorize]
    public class PostController : Controller
    {
        private autoserviceDEntities db = new autoserviceDEntities();

        // GET: /Post/
        public ActionResult List()
        {
            return View(db.Posts.ToList());
        }

        // GET: /Post/Create
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="NamePost,ID_post")] Post post)
        {
            if (ModelState.IsValid&&db.Posts.Find(post.ID_post)==null)
            {
                db.Posts.Add(post);
                db.SaveChanges();
                return RedirectToAction("List");
            }

            return View(post);
        }

        // GET: /Post/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="NamePost,ID_post")] Post post)
        {
            if (ModelState.IsValid)
            {
                db.Entry(post).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("List");
            }
            return View(post);
        }

        [HttpGet, ActionName("Delete")]
        public ActionResult DeleteConfirmed(string id)
        {
            Post post;
            post = db.Posts.Find(id);
            if (post != null)
            {
                db.Posts.Remove(post);
                db.SaveChanges();
                return RedirectToAction("List");
            }
            return HttpNotFound();   
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult CheckID(string ID_post)
        {
            if (db.Posts.Find(ID_post) != null)
            {
                return Json(ID_post);
            }
            return null;
        }
    }
}
