﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoserviceDiplom.Models.ReportModel
{
        public class ReportOrderForClientCustomQueryModel
        {
            public List<GetTotalInfoServicesByOrder_Result> ServiceList { set; get; }
            public OrderServ Order { set; get; }
        }
}