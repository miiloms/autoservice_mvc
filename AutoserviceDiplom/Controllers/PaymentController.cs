﻿using AutoserviceDiplom.Models;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace AutoserviceDiplom.Controllers
{
    [Authorize]
    public class PaymentController : Controller
    {
        private autoserviceDEntities db = new autoserviceDEntities();

        // GET: /Payment/
        public ActionResult List()
        {
            return View(db.TypeOfPayments.ToList());
        }

        // GET: /Payment/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TypeOfPayment typeofpayment = db.TypeOfPayments.Find(id);
            if (typeofpayment == null)
            {
                return HttpNotFound();
            }
            return View(typeofpayment);
        }

        // GET: /Payment/Create
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="ID_Payment,PaymentName")] TypeOfPayment typeofpayment)
        {
            if (ModelState.IsValid && db.TypeOfPayments.Find(typeofpayment.ID_Payment) == null)
            {
                db.TypeOfPayments.Add(typeofpayment);
                db.SaveChanges();
                return RedirectToAction("List");
            }

            return View(typeofpayment);
        }

        // GET: /Payment/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TypeOfPayment typeofpayment = db.TypeOfPayments.Find(id);
            if (typeofpayment == null)
            {
                return HttpNotFound();
            }
            return View(typeofpayment);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="ID_Payment,PaymentName")] TypeOfPayment typeofpayment)
        {
            if (ModelState.IsValid)
            {
                db.Entry(typeofpayment).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("List");
            }
            return View(typeofpayment);
        }


        [HttpGet, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            TypeOfPayment payment = db.TypeOfPayments.Find(id);
            if (payment != null && payment.OrderServs.Count == 0)
            {
                db.TypeOfPayments.Remove(payment);
                db.SaveChanges();
                return Json(1, JsonRequestBehavior.AllowGet);
            }
            return Json(0, JsonRequestBehavior.AllowGet);
        }
        public ActionResult CheckID(int ID_payment)
        {
            if (db.TypeOfPayments.Find(ID_payment) != null)
                return Json(ID_payment, JsonRequestBehavior.AllowGet);
            return null;
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
