﻿using AutoserviceDiplom.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace AutoserviceDiplom.Controllers
{
    [Authorize]
    public class ProviderController : Controller
    {
        private autoserviceDEntities db = new autoserviceDEntities();

        // GET: /Provider/
        public ActionResult List()
        {
            return View(db.Providers.ToList());
        }

        public ActionResult Search(string nameProv, string id, string adress)
        {
            List<Provider> providers = db.Providers.Where(s=>s.Name.Contains(nameProv)||s.ID_provider.Contains(id)||s.Adress.Contains(adress)).ToList();
            return PartialView("SearchResultPartial", providers);
        }
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Provider provider = db.Providers.Find(id);
            if (provider == null)
            {
                return HttpNotFound();
            }
            return View(provider);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Name,ZipCode,Adress,Phone,CertificateNumber,E_mail")] Provider provider)
        {
            ObjectParameter idProv= new ObjectParameter("id_provider", typeof(string));
            if (ModelState.IsValid)
            {
                db.AddProvider(provider.Name, provider.ZipCode, provider.Adress, provider.Phone, provider.CertificateNumber, provider.CertificateNumber, idProv);
                return RedirectToAction("Details", new { id = idProv.Value});
            }

            return View(provider);
        }

        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Provider provider = db.Providers.Find(id);
            if (provider == null)
            {
                return HttpNotFound();
            }
            return View(provider);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Name,ZipCode,Adress,Phone,CertificateNumber,ID_provider,E_mail")] Provider provider)
        {
            if (ModelState.IsValid)
            {
                db.Entry(provider).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Details", new { id=provider.ID_provider});
            }
            return View(provider);
        }

        [HttpGet, ActionName("Delete")]
        public ActionResult DeleteConfirmed(string id)
        {
            Provider provider = db.Providers.Find(id);
            if(provider!=null&&provider.Invoices.Count==0)
            {
                db.Providers.Remove(provider);
                db.SaveChanges();
                return Json(1, JsonRequestBehavior.AllowGet);
            }
            return Json(0, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
