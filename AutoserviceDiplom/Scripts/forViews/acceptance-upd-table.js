﻿//функция для  удаления запчасти из инвойс
function del(id_pos) {
    $.ajax(
    {
        url: '/Acceptance/Delete',
        data: 'id_pos=' + id_pos + '&lot_nmb=' + $('#idLotNumb').val(),
        type: "Post",
        datatype: "json",
        success: function (result) {
            if (result.indexOf('table') != -1)
                $('#updTable').html(result);
        }
    })
}
function showMenu() {
    if ($('#menuInvoice').css('display') == 'none') {
        $('#menuInvoice').slideDown(400);
        setTimeout(function () {
            $('#menuInvoice').slideUp(400);
        }, 5000);
    }
}

function DeleteInvoice() {
    $.ajax(
   {
       url: '/Acceptance/DeleteInvoice',
       data: 'lot_nmb=' + $('#idLotNumb').val(),
       type: "Post",
       datatype: "json"
   })
}

function EditInvoice() {
    $.ajax(
    {
        url: '/Acceptance/EditInvoice',
        data: 'lot_nmb=' + $('#idLotNumb').val(),
        type: "Post",
        datatype: "json",
        success: function (result) {
            if (result != '')
                $('#invoice').html(result);
        }
    })
}
function ClearInvoice() {
    $.ajax(
    {
        url: '/Acceptance/ClearInvoice',
        data: 'lot_nmb=' + $('#idLotNumb').val(),
        type: "Get",
        datatype: "json",
        success: function (result) {
            if (result.indexOf('table') != -1)
                $('#updTable').html(result);
        }
    })
}