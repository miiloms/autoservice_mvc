# README #
#МНОГОПОЛЬЗОВАТЕЛЬСКАЯ ИНФОРМАЦИОННАЯ СИСТЕМА АВТОСЕРВИС "BOSCH"
***
##1 Общие сведения
###1.1 Назначение системы:
* обработка, выдача и хранение информации;
* учет клиентов, автомобилей, сотрудников;
* формирование заказов;
* выдача актов сдачи-приемки выполненных работ, актов сдачи-приемки автомобиля и автозапчастей, заявок клиентов;
* оформление предварительных заявок;
* формирование загрузки мастеров;
* формирование отчетов (ABC и XYZ анализ реализации запчастей, выработка мастеров за период, детализация заказов за период);
* и др.

###1.2 Стек технологий:
* ASP.net MVC 5;
* Серверный язык программирования - C#;
* Identity 2.0;
* HTML 5;
* CSS 3;
* JavaScript;
* JQuery;
* SQL Server;
* Entity Framework 6;
* GIT.

*Функциональное тестирование проводилось в браузере Mozilla FireFox*
***
##2 Требования для запуска проекта
* Установленная .NET библиотека версии 4.5 или выше;
* IDE - MS Visual Studio 2013 или выше;
* СУБД - MS SQL Server 2014 или выше;
* Произвести Attach к БД. Скачать [mdf-файл](https://www.dropbox.com/s/dfb8nrn8vq23owb/autoserviceD.mdf?dl=0) ;
* Прописать в файле Web.config проекта  строки подключения в разделе <connectionStrings>...</connectionStrings>, где {server address} заменить на адрес своего SQL сервера *(прим. HOME-PC\DB_YURI_SQL)*. Строки см. ниже.
```
#!xml
<add name="IdentityDb" connectionString="data source={server address};initial catalog=autoserviceD; integrated security=True;" providerName="System.Data.SqlClient" />
<add name="autoserviceDEntities" connectionString="metadata=res://*/Models.AutoserviceDModel.csdl|res://*/Models.AutoserviceDModel.ssdl|res://*/Models.AutoserviceDModel.msl;provider=System.Data.SqlClient;provider connection string=&quot;data source={server address};initial catalog=autoserviceD;integrated security=True;MultipleActiveResultSets=True;App=EntityFramework&quot;" providerName="System.Data.EntityClient" />
```
* Данные для авторизации - login: test_log  password: 123456
***
##3 Описание веб-приложения
###3.1 Окно  идентификации пользователя

![Screenshot_1.png](https://bitbucket.org/repo/LG5bz4/images/4172096016-Screenshot_1.png)

###3.2 Главное меню

![Screenshot_25.png](https://bitbucket.org/repo/LG5bz4/images/2093181067-Screenshot_25.png)

###3.3 Поиск клиентов

![Screenshot_2.png](https://bitbucket.org/repo/LG5bz4/images/3950792088-Screenshot_2.png)

###3.4 Оформление заказов

![Screenshot_16.png](https://bitbucket.org/repo/LG5bz4/images/868510994-Screenshot_16.png)

###3.5 Работа с заказом (добавление работ, закрепление автозапчастей за работами)

![Screenshot_4.png](https://bitbucket.org/repo/LG5bz4/images/1535331071-Screenshot_4.png)

###3.6 Поиск заказов

![Screenshot_3.png](https://bitbucket.org/repo/LG5bz4/images/1233632526-Screenshot_3.png)

###3.7 Акт сдачи-приемки выполненных работ

![Screenshot_5.png](https://bitbucket.org/repo/LG5bz4/images/3691548529-Screenshot_5.png)

###3.8 Акт сдачи-приемки автомобиля и автозапчастей

![Screenshot_19.png](https://bitbucket.org/repo/LG5bz4/images/910210269-Screenshot_19.png)

###3.9 Регистрация предварительной заявки

![Screenshot_22.png](https://bitbucket.org/repo/LG5bz4/images/714951329-Screenshot_22.png)

###3.10 Поиск предварительных заявок

![Screenshot_24.png](https://bitbucket.org/repo/LG5bz4/images/4043870844-Screenshot_24.png)

###3.11 Формирование загрузки мастеров (резервация времени работ заказа или пред. записи по мастерам)

![Screenshot_23.png](https://bitbucket.org/repo/LG5bz4/images/1817329354-Screenshot_23.png)

###3.12 Работа со складом (приемка автозапчастей от поставщиков или клиентов)

![Screenshot_10.png](https://bitbucket.org/repo/LG5bz4/images/3969086671-Screenshot_10.png)

###3.13 Поиск материалов на складе. Возможность повысить или понизить торговую надбавку для одной или группы автозапчастей.

![Screenshot_28.png](https://bitbucket.org/repo/LG5bz4/images/2140934211-Screenshot_28.png)

###3.14 Пуш уведомления о предварительных заявках (три вида: за 15 мин. до приезда клиента, ровно во время приезда клиента и после 15 мин. если не приехал клиент)

![Screenshot_13.png](https://bitbucket.org/repo/LG5bz4/images/2131474408-Screenshot_13.png)

###3.15 Перевод предварительной заявки в заказ (производится подбор наиболее подходящих по фамилии клиентов из уже имеющихся в базе) 

![Screenshot_26.png](https://bitbucket.org/repo/LG5bz4/images/125264129-Screenshot_26.png)

###3.16 Отчет - детализация заказов за период

![Screenshot_14.png](https://bitbucket.org/repo/LG5bz4/images/1484028188-Screenshot_14.png)

###3.17 ABC и XYZ анализ реализации автозапчастей

![Screenshot_12.png](https://bitbucket.org/repo/LG5bz4/images/3782500802-Screenshot_12.png)

###3.18 График доходов по автозапчастям и услугам за период

![Screenshot_15.png](https://bitbucket.org/repo/LG5bz4/images/467289881-Screenshot_15.png)