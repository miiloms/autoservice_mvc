﻿//показать контрол с поиском клиента
function addProvider(elem) {
    var coord = $(elem).offset();
    if ($('#findClientsControl').css('display') == "none")
        $('#findClientsControl').css('top', coord.top + 35).css('left', coord.left).slideDown(300);
    else
        $('#findClientsControl').slideUp(300);
}
//добавление новго клиента в комбобокс
function addToSet(row) {
    var name = $(row).children().eq(0).text();
    var id = $(row).children().eq(1).text();
    $("#ID_client").prepend($('<option selected="selected" value=' + id + '>' + name + '</option>'));
    $("#findClientsControl").slideUp(500);
}

//функция для  отправки запроса на добавление  инвойс
function AddInvoice() {
    if ($('#row_find_part').css('display') == 'none') {
        $('i[onclick="extend()"]').click();
    }
    $.ajax(
    {
        url: '/AcceptanceCustom/AddInvoice',
        data: $('#Params').serialize(),
        type: "Post",
        datatype: "json",
        success: function (result) {
            $('#updTable').html(result);
        }
    });
}
//для расширения таблицы инвойса
function extend() {
    if ($('#resultSearch').css('display') != 'none') {
        $('#resultSearch, #updTable, #row_find_part, #row_find_invoice').fadeOut(200);
        setTimeout(function () {
            $('#updTable table th:nth-child(3), #updTable table td:nth-child(3)').show();
            $('#updTable, #row_find_invoice').attr('class', 'col-md-12');
            $('#updTable, #row_find_invoice').fadeIn(200);
        }, 200)
        $('i[onclick="extend()"]').attr('class', 'glyphicon glyphicon-circle-arrow-right');
    }
    else {
        $(' #updTable, #row_find_invoice').fadeOut(200);
        setTimeout(function () {
            $('#updTable table th:nth-child(3), #updTable table td:nth-child(3)').hide();
            $('#updTable, #row_find_invoice').attr('class', 'col-md-6');
            $('#resultSearch, #updTable, #row_find_part, #row_find_invoice').fadeIn(200);

        }, 200)
        $('i[onclick="extend()"]').attr('class', 'glyphicon glyphicon-circle-arrow-left');
    }

}

//функция для  отправки запроса на добавление запчасти в инвойс
function AddPart(btn) {
    $.ajax(
    {
        url: '/AcceptanceCustom/AddPart',
        data: $(btn).parent().parent().find('input, select').add('#idAccept').serialize(),////////
        type: "Post",
        datatype: "json",
        success: function (result) {
            if (result.indexOf('table') == -1) {
                $('#popErorr').html('Данная запчасть ID (' + result + ') уже есть в документе').slideDown(1000).delay(3000).slideUp(1000); return 0;
            }
            if (result != '') {
                $('#updTable').html(result);
                $('#popErorr').fadeOut(1000);
            }
        }
    });
}

function showbar() {
    if ($('#createInvoice').css('display') == 'none') {
        $('#createInvoice').fadeIn(300);
        $('button[onclick="showbar()"]').css('borderRadius', '10px').html('Оформление нового акта сдачи-приемки материалов');
    }
    else {
        $("#findClientsControl").fadeOut(300);
        $('#createInvoice').fadeOut(300);
        $('button[onclick="showbar()"]').css('borderRadius', '0px').html('Оформить новый акт сдачи-приемки материалов');
    }

}