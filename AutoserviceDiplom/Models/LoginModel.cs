﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace AutoserviceDiplom.Models
{
    public class LoginModel
    {
        [Required(ErrorMessage="Требуется логин")]
        public string LoginName { get; set; }
        [Required(ErrorMessage = "Требуется пароль")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}