﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Identity;
using AutoserviceDiplom.Models;

namespace AutoserviceDiplom.Models.UserModels
{
    public class EditModel
    {
        [Required]
        public string NewLoginName { get; set; }
        [Required]
        public string LoginName { get; set; }
        public string[] NameRoles { get; set; }
        [Required]
        public string[] NewNameRoles { get; set; }
        [Required]
        public int PersonnelNumber { get; set; }
    }
}