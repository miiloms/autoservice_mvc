﻿//для развертывания конфиденциальных данных
$(document).ready(function () {
    $('.table tr').slice(6, 12).each(function () {
        $(this).hide();
    });
    //
    $('.table tr').slice(6, 12).each(function () {
        $(this).hide();
    });
    $('#dataPassport').click(function () {
        var intIndex = 0;

        if ($('.table tr').slice(6).css('display') == 'none')
            $('.table tr').slice(6, 12).each(function () {
                intIndex += 50;
                $(this).delay(intIndex).slideDown(50);
            })
        else
            $('.table tr').slice(6, 12).each(function () {
                intIndex += 50;
                $(this).delay(intIndex).slideUp(50);
            })
    })

    //для добавления контракта мод окно
    $('#Create').click(function (event) {
        event.preventDefault(); // выключaем стaндaртную рoль элементa
        $('#overlay').fadeIn(400, // снaчaлa плaвнo пoкaзывaем темную пoдлoжку
            function () { // пoсле выпoлнения предъидущей aнимaции
                $('#modal_form')
                    .css('display', 'block') // убирaем у мoдaльнoгo oкнa display: none;
                    .animate({ opacity: 1, top: '50%' }, 200); // плaвнo прибaвляем прoзрaчнoсть oднoвременнo сo съезжaнием вниз
            });
    });
    /* Зaкрытие мoдaльнoгo oкнa, тут делaем тo же сaмoе нo в oбрaтнoм пoрядке */
    $('#modal_close, #overlay').click(function () { // лoвим клик пo крестику или пoдлoжке
        $('#modal_form')
            .animate({ opacity: 0, top: '45%' }, 100,  // плaвнo меняем прoзрaчнoсть нa 0 и oднoвременнo двигaем oкнo вверх
                function () { // пoсле aнимaции
                    $(this).css('display', 'none'); // делaем ему display: none;
                    $('#overlay').fadeOut(400); // скрывaем пoдлoжку
                }
            );
    });
})
//for delete
function GoDelete(PersNumb) {
    $.ajax(
    {
        url: ' /Employee/Delete',
        data: 'PersonnelNumber=' + PersNumb,
        type: "GET",
        datatype: "json"
    });
}
//функция для  отправки запроса на добавление контракта
function AddNewContract() {
    $.ajax(
    {
        url: '/Contract/CreateContract',
        data: 'PersonnelNumber=' + $('#PersonnelNumber').val(),
        type: "Get",
        datatype: "json",
        success: function (result) { $("#contents_m").html(result); }
    });
}
//функция для  отправки запроса на редактирование контракта
function EditContract(id) {
    $.ajax(
    {
        url: '/Contract/Edit',
        data: 'IDcontract=' + id,
        type: "Get",
        datatype: "json",
        success: function (result) {
            $('table tr>td:contains(' + id + ')').parent().replaceWith(result);
        }
    });
}
//используется при  сохранении после изменения
function Save(id, btn) {
    $.ajax(
        {
            url: '/Contract/Edit',
            data: $(btn).parent().parent().find('input').add('#PersonnelNumber').serialize(),
            type: "POST",
            datatype: "json",
            success: function (result) {
                if (!$(result).hasClass('script')) {
                    $('table tr>td:contains(' + id + ')').parent().replaceWith(result);
                }
            }
        })
}
//для отмены редактирования
function Cancel(id) {
    $.ajax(
        {
            url: '/Contract/CancelEdit',
            data: 'IDcontract=' + id,
            type: "Get",
            datatype: "json",
            success: function (result) {
                $('table tr>td:contains(' + id + ')').parent().replaceWith(result);
            }
        })
}
//для отмены редактирования
function Delete(id) {
    if (confirm("Вы уверены, что хотите удалить этот контракт?"))
        $.ajax(
            {
                url: '/Contract/Delete',
                data: 'IDcontract=' + id,
                type: "Get",
                datatype: "json",
                success: function (result) { }
            })
}
//используется при  расторжении(закрытии контракта)
function UnLock(id) {
    $.ajax(
        {
            url: '/Contract/UnLock',
            data: 'IDcontract=' + id,
            type: "Get",
            datatype: "json",
            success: function (result) {
                $('table tr>td:contains(' + id + ')').parent().replaceWith(result);
            }
        })
}