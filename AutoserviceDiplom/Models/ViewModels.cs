﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Validation;
using System.Web.Mvc;
using System.Data.Entity;

namespace AutoserviceDiplom.Models
{
    public class OrderViewModel
    {
        [Display(Name = "ФИО клиента")]
        public string NameSerName { get; set; }
        [HiddenInput]
        public string ID_client { get; set; }
        [Display(Name = "Дата оформления")]
        [DisplayFormat(DataFormatString = "{0:dd MMMM yyyy}")]
        public Nullable<System.DateTime> DateMakingOrder { get; set; }
        public string Descriptions { get; set; }
        [Range(10,1000000,ErrorMessage="Пробег вне диапазона!")]
        [Required(ErrorMessage="Требуеться ввести пробег")]
        public Nullable<int> CurrentMileageCar { get; set; }
        [HiddenInput]
        [Required(ErrorMessage="Требуеться выбрать автомобиль по VIN номеру")]
        public string VIN_number { get; set; }
        public SelectList ListEmplyees { get; set;}
        public int PersonnelNumber { get; set;}
        public long? ID_record { set; get; }
        public int[] NumberWheelCaps { get; set; }
        public int[] NumberWipers { get; set; }
        public int[] NumberWipersArms { get; set; }
        public bool IsAntenna { get; set; }
        public bool IsSpareWheel { get; set; }
        public bool IsCoverDecorEngine { get; set; }
        [Range(0, 100, ErrorMessage = "Неверное значение уровня топлива!")]
        [Required(ErrorMessage="Требуеться указать уровень топлива")]
        public byte? FluelLevelPercent { get; set; }
        public bool IsTuner { get; set; }
    }
    public class ExecServViewModel
    {
        public string ID_order { get; set; }
        public virtual Service Service { get; set; }
        public virtual Employee Employee { get; set; }
        public virtual SelectList ListEmployees { get; set; }
        public int? PersonnelNumber { set; get; }
        public string Notes { get; set; }
        [Required(ErrorMessage="Требуеться ввести время")]
        [Display(Name = "Затраченное время")]
        public Nullable<double> TakeTime { get; set; }
        [Display(Name = "Дата")]
        public Nullable<System.DateTime> DateCompleting { get; set; }
        public string ID_service { get; set; }
        public int  TaxAddedValue { get; set; }
        public bool IsEnableChange { set; get; }
    }
    public class AttachedSparePartViewModel
    {
        public string Name_service { set; get; }
        public string ID_client { get; set; }
        public string ID_order { get; set; }
        public  string ID_service { get; set; }
        public string ID_part { get; set; }
        public string Name { get; set; }
        public string Unit { get; set; }
        public string Manufacture { get; set; }
        [Required(ErrorMessage="!!!")]
        [Range(0.1,150.01, ErrorMessage="1...150")]
        public double  Number{ get; set; }
        public IEnumerable<GetAttachedParts_Result> AttachedPartsByService { set; get; }
        public IEnumerable<GetAttachedCustomParts_Result> AttachedCustomPartsByService { set; get; }
        public IQueryable<StockOfSparePart> StockOfParts { get; set; }
        public IEnumerable<GetStockCustomPartsById_Result> StockCustomOfParts { get; set; }
    }

    public class SearchOrdersViewModel
    {
        public string ID_client { set; get; }
        public string ID_order { set; get; }
        public string Model { set; get; }
        public string NameSerName { set; get; }
        public string RegNumber { set; get; }
        public DateTime? DateMakingFrom { set; get; }
        public DateTime? DateMakingBefore { set; get; }
        public bool IsRejection { set; get;}
        public bool IsClosed { set; get; }
        public byte NumbSortRow { get; set; }
        public bool IsAsc { get; set; }

    }
    public class ContractsViewModel
    {
        public string NameSerName { set; get; }
        public int PersonnelNumber { set; get; }
        public IEnumerable<ContractToEmployee> contracts { set; get; } 
    }

    public class NewContractViewModel
    {
        public string NameSerName { set; get; }
        public int? PersonnelNumber { set; get; }
        public DateTime RecruitDate { set; get; }
        public string TypeC { set; get; }
        public int? Term { set; get; }
    }

    public class SearchParmsContractViewModel
    {
        public int? ID_contract { set; get; }
        public string NameEmployee { set; get; }
        public string TypeContract { set; get; }
        public int? Term { set; get; }
        public bool IsOn { set; get; }
        public DateTime? DateFrom { set; get; }
        public DateTime? DateBefore { set; get; }
    }

    public class AcceptanceParamsViewModel
    {
        public SelectList ListProvider { set; get;}
        public SelectList ListEmployee { set; get; }
        public int PersonnelNumber { set; get;}
        public string ID_provider{set;get;}
        public int? LotNumber { set; get;}
        public DateTime? DateLotStart { set; get; }
        public DateTime? DateLotEnd { set; get; }
        public Nullable<DateTime> DeliveryDate { set; get;}
        public string ID_part { set; get; }
        public string NamePart { set; get; }
        public SelectList ListProvidersSearch { get; set; }
        public string IdProv { get; set; }
    }

    public class AcceptancedPartsViewModel
    {
        public List<GetAcceptancedSpareParts_Result> AcceptencedParts { set; get; }
        public GetInfoInvoiceByLotNumber_Result InfoInvoice { set; get; }
        public decimal? TotallSummByParts { set; get; }
    }
    public class AcceptanceCustomParamsViewModel
    {
        public SelectList ListClients { set; get; }
        public SelectList ListEmployee { set; get; }
        public int PersonnelNumber { set; get; }
        public string ID_client { set; get; }
        public int? ID_acceptance { set; get; }
        public Nullable<DateTime> AcceptDate { set; get; }
        public string ID_part { set; get; }
        public string NamePart { set; get; }
    }

    public class AcceptedCustomPartsViewModel
    {
        public List<GetAcceptedCustomParts_Result> AcceptencedParts { set; get; }
        public GetInfoCustomInvoiceByIdDoc_Result InfoInvoice { set; get; }
    }

    public class TableLoadViewModel
    {
        public DateTime? CurrentDate { set; get; }
        public OrderServ Order { set; get; }
        public string NameCar { set; get; }
        public List<PreRecordService> PreRecordServices { set; get; }
        public List<TableLoad_Result> LoadEmployees {set; get;}

    }
    public class TableLoadPreRecordViewModel
    {
        public PreRecord Record { set; get; }
        public List<PreRecordService> PreRecordServices { set; get; }
        public List<TableLoad_Result> LoadEmployees { set; get; }

    }
    public class PreRecordViewModel
    {
        public PreRecord PreRecord { set; get; }
        public SelectList  ListServices { set; get; }

    }

    public class PreRecordRecivedDataViewModel
    {
        [Required]
        [Display(Name = "ФИО клиента")]
        public string NameSerName { set; get; }
        [Display(Name = "Тел. номер")]
        public string Phone_number { set; get; }
        [Display(Name = "Марка/модель авто")]
        [Required]
        public string MarkModelCar { set; get; }
        [Display(Name = "Год выпуска")]
        public Nullable<int> IssueYear { set; get; }
        [Display(Name = "Рег. номер авто")]
        public string RegNumberCar { set; get; }
        [Required]
        public string[] Id_serv { set; get; }

    }

    public class SearchPreRecordsViewModel
    {
        private string nameClient;
        private string markCar;
        private string regNumber;
        private DateTime? dateMakingFrom;
        private DateTime? dateMakingBefore;
        /// <summary>
        /// /////////////////////////////////
        /// </summary>
        public long? ID_record { set; get; }
        public string NameClient { set { this.nameClient = value ?? string.Empty; } get { return this.nameClient; } }
        public string MarkCar { set { this.markCar = value ?? string.Empty; } get { return this.markCar; } }
        public string RegNumber { set { this.regNumber = value ?? string.Empty; } get { return this.regNumber; } }
        public DateTime? DateMakingFrom { set { this.dateMakingFrom = value ?? new DateTime(); } get { return this.dateMakingFrom; } }
        public DateTime? DateMakingBefore { set { this.dateMakingBefore = value ?? new DateTime(2200,1,1); } get { return this.dateMakingBefore; } }
        public Nullable<bool> IsRejection { set; get; }
    }

    public class SearchPartsOfStoreViewModel
    {
        private DateTime? dateIvoiceFrom;
        private DateTime? dateIvoiceBefore;
        private string id_part;
        private string name;
        private string manufacture;
        private decimal priceBefore;
        private decimal priceAfter;
        private float stockPercent;
        public Nullable<int> personnelNumber;
        public string ID_part { set { this.id_part = value ?? string.Empty; } get { return this.id_part; } }
        public string Name { set { this.name = value ?? string.Empty; } get { return this.name; } }
        public string Manufacture { set { this.manufacture = value ?? string.Empty; } get { return this.manufacture; } }
        public Nullable<decimal> PriceBefore { set { this.priceBefore = value ?? 100000000; } get { return this.priceBefore; } }
        public Nullable<decimal> PriceAfter { set { this.priceAfter = value ?? 0; } get { return this.priceAfter; } }
        public Nullable<int> ID_invoice { get; set; }
        public Nullable<int> PersonnelNumber { set { this.personnelNumber = value==0 ? null:value; } get { return this.personnelNumber; } }
        public Nullable<float> StockPercent { set { this.stockPercent = value ?? 100; } get { return this.stockPercent; } }
        public DateTime? DateIvoiceFrom { set { this.dateIvoiceFrom = value ?? new DateTime(); } get { return this.dateIvoiceFrom; } }
        public DateTime? DateIvoiceBefore { set { this.dateIvoiceBefore = value ?? new DateTime(2200, 1, 1); } get { return this.dateIvoiceBefore; } }
    }

    public class ChoosingParamsAnalysis
    {
        public ChoosingParamsAnalysis()
        {
            Dictionary<int, String> collect = new Dictionary<int, String> { { 0, "Выбрать период..." }, { 1, "Месяц" }, { 2, "Квартал" }, { 3, "День" } };
            this.TypePeriod = new SelectList(collect, "Key", "Value");
        }
        public SelectList TypePeriod { set; get; }
        public byte TypePeriodId { set; get; }
        public Nullable<DateTime> StartDate { set; get; }
        public Nullable<DateTime> EndDate { get; set; }
    }

    public class CarViewModel
    {
        public string ModelCar { get; set; }
        public string BrandCar { get; set; }
        public string SeriesCar { get; set; }
        public string ModifCar { get; set; }
        [Required(ErrorMessage = "Обязательное поле")]
        [Display(Name = "Регистрационный номер")]
        public string Registr_Number { get; set; }
        public string BodyColorCode { get; set; }
        public string BodyPaintName { get; set; }
        public string BodyColorName { get; set; }
        [Display(Name = "Год выпуска")]
        public Nullable<int> IssueYear { get; set; }
        [StringLength(17, MinimumLength = 17, ErrorMessage = "VIN номер состоит из 17 символов")]
        [Display(Name = "VIN номер кузова")]
        public string VIN_number { get; set; }
        [Display(Name = "Ф.И.О. владельца")]
        public string OwnerName { get; set; }
        [Display(Name = "Номер техпаспорта")]
        [Required(ErrorMessage = "Обязательное поле")]
        public string DataSheetCar { get; set; }
         [Display(Name = "Тип КПП")]
        public string TransmissionType { get; set; }
        public byte[] ViewCar { get; set; }
        public  Nullable<int> CountRepare { get; set; }
    }
    public class CarEditerViewModel
    {
        public Nullable<int> IDBrandCar { get; set; }
        public SelectList BrandCarList { get; set; }

        public Nullable<int> IDModelCar { get; set; }
        public SelectList ModelCarList { get; set; }

        public Nullable<int> IDSeriesCar { get; set; }
        public SelectList SeriesCarList { get; set; }
        [Required(ErrorMessage = "Укажите модификацию")]
        public Nullable<int> IDModifCar { get; set; }
        
        public SelectList ModifCarList { get; set; }
        [Required(ErrorMessage="Укажите регистрационный номер")]
        [Display(Name = "Регистрационный номер")]
        public string Registr_Number { get; set; }
        public Nullable<int> BodyColorId { get; set; }
        public SelectList BodyColorList { get; set; }
        public SelectList IssueYearList { get; set; }
        
        [Required(ErrorMessage = "Укажите год выпуска")]
        public int IssueYear { get; set; }
        [StringLength(17, MinimumLength = 17, ErrorMessage = "VIN номер состоит из 17 символов")]
        [Display(Name = "VIN номер кузова")]
        [Required (ErrorMessage="Введите VIN номер")]
        public string VIN_number { get; set; }
        [Required(ErrorMessage = "Укажите владельца")]
        [Display(Name = "Ф.И.О. владельца")]
        public string OwnerName { get; set; }
        [Display(Name = "Номер техпаспорта")]
        [Required(ErrorMessage = "Укажите номер тех. паспорта")]
        public string DataSheetCar { get; set; }
        public SelectList TransmissionTypeList { get; set; }
        [Display(Name = "Тип КПП")]
        public string TransmissionType { get; set; }
    }
    public class SearchParamsService
    {
        public SelectList ListTypeServices { set; get; }
        public int? IDTypeServices { set; get; }
        public string ServName { set; get; }
        public string ServId { set; get; }
        public Nullable<int>  PriceStart { set; get; }
        public Nullable<int> PriceEnd { set; get; }
        public Nullable<bool> isAvlble { set; get; }
    }
    public class CarMarkViewModel
    { 
    public byte? TypeMark{ set; get; }
    public  string addonInfo { set; get; }
    public byte? tempID { set; get; }
    public int? X_Axis { set; get; }
    public int? Y_Axis { set; get; }
    public bool IsEdit { set; get; }
    }
    public  class RemarkToStateCarViewModel
    {
        public int ID_remark { get; set; }
        public string ID_order { get; set; }
        public int X_Axis_pos { get; set; }
        public int Y_Axis_pos { get; set; }
        public string RemarkText { get; set; }
        public Nullable<byte> NumberType { get; set; }

    }
}
