﻿$(document).ready(function () {
    //для календаря
    $('.date').datetimepicker({ language: 'ru', pickTime: false, format: 'YYYY-MM-DD' });
    //выделяем по цветам график загрузки мастеров
    $('.tbl-load  td:not(:first-child)').each(function () {
        if ($(this).children(':last-child').val() == 0)
            $(this).addClass('vacancy');
        else {
            if (!$(this).hasClass('busy-current-order')) {
                if ($(this).hasClass('busy-pre-reserv')) {
                    $(this).append('<span>' + $(this).children(':last-child').val() + ':00' + '</span>'); return 0;
                }
                $(this).addClass('busy');
                $(this).append($(this).children(':last-child').val() + ':00');

            }
            else
                $(this).append('<span>' + $(this).children(':last-child').val() + ':00' + '</span><div class="circle"></div>');
        }

    })
    //вешаем на строки с услугами выделение при нажатии
    $('.tbl-service tr:has(td)').click(function () {
        var summTime = 0;
        if ($(this).children().eq(3).html() == '') //можно выделять только те, что еще не задействаованны
            if ($(this).hasClass('selected-row'))
                $(this).removeClass('selected-row')
            else
                $(this).addClass('selected-row');
        $(".selected-row td:nth-child(3)").each(function () {
            summTime += Number($(this).html().replace(",", "."));
        })
        $('#totTime').html(summTime.toFixed(2));

    })

    //вешаем на ячейки на таблице загрузок выделение для добавления
    $('.tbl-load td:not(:first-child)').click(function () {
        if ($(this).hasClass('vacancy')) {
            var sender = $(this).children('input:first-child').add($(this).parent().children().eq(0).find('input'));
            $.ajax(
            {
                url: '/WorkLoading/ReservTime',
                data: $(".selected-row td input").add(sender).add('input[name="CurrentDate"]').add('#ID_order').serialize(),
                type: "POST",
                datatype: "json"
            });
        }

    })
    //вешаем на ячейки на таблице загрузок для удаления услуги 
    $('.circle').click(function () {
        $.ajax(
        {
            url: '/WorkLoading/DeleteReservTime',
            data: $(this).parent().children('input[name="vb"]').add('input[name="CurrentDate"]').add($(this).parent().parent().children().eq(0).find('input')).serialize(),
            type: "POST",
            datatype: "json"
        });
    })
    //вешаем на календарь  на его изменение
    $('.datepicker tbody, .backward-day, .forward-day').click(function () {
        var culpritObject = $(this);
        setTimeout(function () {
            var currentDay = new Date($('#CurrentDate').val());

            if (culpritObject.hasClass('backward-day'))
                currentDay.setDate(currentDay.getDate() - 1)
            else
                if (culpritObject.hasClass('forward-day'))
                    currentDay.setDate(currentDay.getDate() + 1)
            var stringDate = currentDay.getFullYear() + '-' + (currentDay.getMonth() + 1) + '-' + (currentDay.getDate() >= 10 ? currentDay.getDate() : '0' + currentDay.getDate());

            window.location = '/WorkLoading/TableLoad/?id_order=' + $('#ID_order').val() + '&CurrentDate=' + stringDate;

        }, 1)

    })
    /////для закрытия инфо-окна
    $('.close-sign').click(function () {
        $('.my-comment').fadeOut(400);
    })
    //вешаем на ячейки на таблице загрузок для подгрузки инфы
    $('.busy, .busy-current-order span, .busy-pre-reserv').click(function () {
        if ($(this).hasClass('busy') || $(this).hasClass('busy-pre-reserv'))
            var my_data = $(this).children('input[name="vb"]').add('input[name="CurrentDate"]').add($(this).parent().children().eq(0).find('input'));
        else
            var my_data = $(this).parent().children('input[name="vb"]').add('input[name="CurrentDate"]').add($(this).parent().parent().children().eq(0).find('input'));
        var elem = $(this);
        $.ajax(
        {
            url: '/WorkLoading/GetDetailsForTime',
            data: my_data.serialize(),
            type: "POST",
            datatype: "json",
            success: function (content) {
                $('#contents_info').html(content);
                var coord = elem.offset();
                var w = elem.width();
                var h = elem.height();
                var h_box = $('.my-comment').height();
                $('.my-comment').hide();
                $('.my-comment').css('top', coord.top + h * 0.5 - 0.5 * h_box + 5).css('left', coord.left + w + 12);
                $('.my-comment').fadeIn(400);
            }
        });
    })
})