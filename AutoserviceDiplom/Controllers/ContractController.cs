﻿using AutoserviceDiplom.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace AutoserviceDiplom.Controllers
{
    [Authorize]
    public class ContractController : Controller
    {
        autoserviceDEntities db = new autoserviceDEntities();
        [HttpGet]
        public ActionResult CreateContract(int? PersonnelNumber)
        {
            NewContractViewModel newContract;
            if (PersonnelNumber != null)
            {
                newContract = new NewContractViewModel() { PersonnelNumber = PersonnelNumber, RecruitDate=DateTime.Today};
                return PartialView(newContract);
            }
            else
                return HttpNotFound();
        }
        [HttpPost]
        public ActionResult CreateContract(NewContractViewModel newContract)
        {
            if (newContract != null && ModelState.IsValid)
            {
                if (db.CreateNewContract(newContract.PersonnelNumber, newContract.RecruitDate, newContract.TypeC, newContract.Term) > 0)
                {
                    return PartialView("UpdatingTable", db.ContractToEmployees.Where(e=>e.PersonnelNumber==newContract.PersonnelNumber&&e.DismissDate==null).FirstOrDefault()); 
                }
                return JavaScript("$('#contents_m').text('Невозможно создать контракт так как существует действующий'); setTimeout(function(){$('#modal_close').click();}, 2000);");
            }
            else return HttpNotFound();
     
        }
        [HttpGet]
        public ActionResult Edit(int? IDcontract)
        {
            if (IDcontract != null)
            {
                ContractToEmployee contract = db.ContractToEmployees.Find(IDcontract);

                if (contract != null)
                {
                    Session[IDcontract.ToString()] = contract;
                    return PartialView(contract);
                }
                return HttpNotFound();
            }
            else
                return HttpNotFound();
        }
        [HttpPost]
        public ActionResult Edit(ContractToEmployee contract)
        {
            if (contract != null&&ModelState.IsValid&&contract.PersonnelNumber!=0)
            {
                db.Entry(contract).State = EntityState.Modified;
                db.SaveChanges();
                return PartialView("UpdatingTable", contract);
            }
            else
                return HttpNotFound();
        }

        [HttpGet]
        public ActionResult CancelEdit(int? IDcontract)
        {
            if (IDcontract != null)
            {
                ContractToEmployee contract;
                if (Session[IDcontract.ToString()] != null)
                {
                    contract = Session[IDcontract.ToString()] as ContractToEmployee;
                    return PartialView("UpdatingTable", contract);
                }
                return HttpNotFound();
            }
            else
                return HttpNotFound();
        }

         [HttpGet]
        public ActionResult Delete(int? IDcontract)
        {
            ContractToEmployee contract = db.ContractToEmployees.Find(IDcontract);
            if (contract != null)
            {
                db.Entry(contract).State = EntityState.Deleted;
                if(db.SaveChanges()>0)
                  return JavaScript(" $('table tr>td:contains("+IDcontract+")').parent().remove() ");
                return HttpNotFound();
            }
            else
                return HttpNotFound();
        }
         [HttpGet]
         public ActionResult UnLock(int? IDcontract)
         {
             if (IDcontract != null)
             {
                 if ( db.UnLockContract(IDcontract) > 0)
                     return PartialView("UpdatingTable", db.ContractToEmployees.Find(IDcontract));
                 return HttpNotFound();
             }
             else
                 return HttpNotFound();
         }
         public ActionResult Search()
         {
             return View();
         }

         [HttpPost]
         public ActionResult Search(SearchParmsContractViewModel param)
         {
             if(param !=null)
             {
                IEnumerable<FindContractByParams_Result> searchResults= db.FindContractByParams(param.ID_contract, param.NameEmployee, param.TypeContract, param.Term, param.IsOn, param.DateFrom, param.DateBefore);
                return PartialView("SearchResults", searchResults.ToList());
             }
             return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
         }
	}
}