//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AutoserviceDiplom.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class UnAgregatedLoadTable
    {
        public Nullable<double> C9_00_HOUR { get; set; }
        public Nullable<double> C10_00_HOUR { get; set; }
        public Nullable<double> C11_00_HOUR { get; set; }
        public Nullable<double> C12_00_HOUR { get; set; }
        public Nullable<double> C13_00_HOUR { get; set; }
        public Nullable<double> C14_00_HOUR { get; set; }
        public Nullable<double> C15_00_HOUR { get; set; }
        public Nullable<double> C16_00_HOUR { get; set; }
        public Nullable<double> C17_00_HOUR { get; set; }
        public Nullable<double> C18_00_HOUR { get; set; }
        public string NameSerName { get; set; }
        public int PersonnelNumber { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
    }
}
