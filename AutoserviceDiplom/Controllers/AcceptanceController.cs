﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Web.Mvc;
using System.Data.Entity.Core.Objects;
using AutoserviceDiplom.Models;
using AutoserviceDiplom.MyCustomClasses;
namespace AutoserviceDiplom.Controllers
{
    [Authorize]
    public class AcceptanceController : Controller
    {
        private autoserviceDEntities db = new autoserviceDEntities();
        public ActionResult Manage(int? LotNumber)
        {
            AcceptanceParamsViewModel paramAccept = new AcceptanceParamsViewModel();
            var empls = db.Employees.Where(f => f.Post.ID_post == "mngparts"&&f.ContractToEmployees.FirstOrDefault(d=>d.DismissDate==null)!=null).ToList();
            paramAccept.ListEmployee = new SelectList(empls, "PersonnelNumber", "NameSerName");
            var prvder = db.MostQueriedProviders.ToList();
            prvder.Add(new MostQueriedProvider { ID_provider = "0", Name = "Выбрать поставщика..." });
            paramAccept.ListProvider = new SelectList(prvder, "ID_provider", "Name");
            var listProv=db.Providers.ToList();
            listProv.Add(new Provider{ID_provider="", Name="Выбрать поставщика"});
            paramAccept.ListProvidersSearch = new SelectList(listProv, "ID_provider", "Name", "");
            ViewBag.LotNumberCurrent = LotNumber;
            return View(paramAccept);
        }
        [HttpPost]
        public ActionResult SearchParts(string ID_part, string NamePart)
        {
            List<SparePartMaterial> sParts = db.SparePartMaterials.Where(s => s.ID_part.Contains(ID_part) && s.Name.Contains(NamePart)).Take(10).ToList();
            if (sParts.Count != 0)
                return PartialView("SearchPartResults",sParts);
            else
                Response.Write("Нет данных по запросу...");
            return null;
        }
        [HttpPost]
        public ActionResult AddPart(string Number, string Price, int? LotNumber, string ID_part)
        {
            if (!LotNumber.HasValue)
                return null;
            double _Number;
            decimal _Price;
            if (double.TryParse(Number, out _Number) && decimal.TryParse(Price, out _Price))
            {
                int status =db.AddSparePartInvoice(_Number, _Price, ID_part, LotNumber);
                if (status == -1)
                    return Json(ID_part);
                var viewModel = this.GetAcceptedParts(LotNumber.Value);
                return PartialView("UpdateTable", viewModel);
            }
            else
            {
                return null;
            }
        }

        [HttpPost]
        public ActionResult AddInvoice(Invoice invoice)
        {
            if (ModelState.IsValid)
            {
                var foundInv= db.Invoices.Find(invoice.LotNumber);
                if ( foundInv== null&&invoice.ID_provider!="0")
                {
                    db.AddInvoice(invoice.ID_provider, invoice.PersonnelNumber, invoice.DeliveryDate, invoice.LotNumber);
                    var infoInvoice = db.GetInfoInvoiceByLotNumber(invoice.LotNumber).FirstOrDefault();
                    return PartialView("EmptyTableInvoice", infoInvoice);
                }
                else
                {
                    if (foundInv.Acceptance_Invoice.Count > 0)
                    {
                        var viewModel = this.GetAcceptedParts(invoice.LotNumber);
                        return PartialView("UpdateTable", viewModel);
                    }
                    else
                    {
                        var infoInvoice = db.GetInfoInvoiceByLotNumber(invoice.LotNumber).FirstOrDefault();
                        return PartialView("EmptyTableInvoice", infoInvoice);
                    }
                    
                }
            }
            else return Json("Неверно введены параметры");
        }
        [HttpPost]
        public ActionResult SearchInvoice(int? LotNumber, DateTime? DateLotStart, DateTime? DateLotEnd, string IdProv)
        {
                int lotNumberLow=LotNumber??0;
                int lotNumberHi=LotNumber??int.MaxValue;
                DateLotStart=DateLotStart?? new DateTime(1);
                DateLotEnd=DateLotEnd?? DateTime.Now;
                List<Invoice> invoices = db.Invoices.Where(f => f.LotNumber >= lotNumberLow && f.LotNumber <= lotNumberHi && 
                    f.DeliveryDate >= DateLotStart && f.DeliveryDate <= DateLotEnd && 
                    f.ID_provider.Contains(IdProv)).ToList();
                if (invoices.Count >0)
                {
                    if (invoices.Count == 1)
                    {
                        var viewModel = this.GetAcceptedParts(invoices.FirstOrDefault().LotNumber);
                        if (viewModel.AcceptencedParts.Count > 0)
                            return PartialView("UpdateTable", viewModel);
                        else
                            return PartialView("EmptyTableInvoice", viewModel.InfoInvoice);
                    }
                    else 
                    {
                        return PartialView("ListInvoices", invoices.OrderBy(o=>o.DeliveryDate).ToList());
                    }
                }
                else
                    return JavaScript("$('#popErorr').html('Акт сдачи-приемки под номером № " + LotNumber + " отсутсвует в БД').slideDown(500).delay(3000).slideUp(500); ");
                
        }

        [HttpPost]
        public ActionResult Delete(int? id_pos, int? lot_nmb)
        {
            if (id_pos.HasValue&& lot_nmb.HasValue)
            {
                var delPart = new Acceptance_Invoice { ID_position = id_pos.Value };
                if (db.UsingPartMaterials.Where(i=>i.ID_position==id_pos).ToList().Count==0)// если позиции запчаcтей еще не были задействаованы
                {
                    db.Entry(delPart).State = EntityState.Deleted;
                    db.SaveChanges();
                    var viewModel = this.GetAcceptedParts(lot_nmb.Value);
                    if (viewModel.AcceptencedParts.Count > 0)
                        return PartialView("UpdateTable", viewModel);
                    else
                        return PartialView("EmptyTableInvoice", viewModel.InfoInvoice);
                }
                else return JavaScript("$('#popErorr').html('Данный материал (запчасть) не может быть исключена из документа, так как уже задействована в услугах (работах)').slideDown(1000).delay(3000).slideUp(1000); ");
            }
            else return null;
        }

        public ActionResult DeleteInvoice(int? lot_nmb)
        {
            if (lot_nmb.HasValue)
            {
                var inputParams = new Dictionary<string, object> {{"lotNumb",lot_nmb}};
                ConnectionLayerDB dbConnection = new ConnectionLayerDB("autoserviceDB");
                int stat=dbConnection.ExecutionProcedure("DeleteInvoice", inputParams);
                switch (stat)
                {
                    case -1: return null;
                    case -2: return JavaScript("$('#popErorr').html('" + dbConnection.InfoMessage + "').slideDown(1000).delay(3000).slideUp(1000); ");
                    default: return JavaScript("$('#updTable').html(''); $('#popSuccess').html('Акт сдачи-приемки №"+lot_nmb+" удален из БД!').slideDown(1000).delay(3000).slideUp(1000); ");
                }
            }
            return null;
        }
        [HttpPost]
        public ActionResult EditInvoice(int? lot_nmb)
        {
            if (lot_nmb.HasValue)
            {
                var infoInvoice = db.GetInfoInvoiceByLotNumber(lot_nmb).FirstOrDefault();
                if (infoInvoice != null)
                {
                   Session["InfoEditInvoice"] = infoInvoice;
                   var listEmpl=db.Employees.Where(f => f.Post.ID_post == "mngparts" && f.ContractToEmployees.FirstOrDefault(d => d.DismissDate == null) != null).ToList();
                   ViewBag.ListEmployee = new SelectList(listEmpl, "PersonnelNumber", "NameSerName");
                   ViewBag.ListProvider = new SelectList(db.Providers.Take(5), "ID_provider", "Name");
                    return PartialView("EditInvoice",infoInvoice);
                }
            }
            return null;
        }

        [HttpPost]
        public ActionResult EditSaveInvoice(Invoice invoice)
        {
            if (ModelState.IsValid)
            {
                db.Entry(invoice).State = EntityState.Modified;
                db.SaveChanges();
                var viewModel = this.GetAcceptedParts(invoice.LotNumber);
                return PartialView("UpdateTable", viewModel);
            }
            return null;
        }
        [HttpGet]
        public ActionResult CancelEditInvoice(int? lot_nmb)
        {
            if (lot_nmb.HasValue)
            {
                var infoInvoice = db.GetInfoInvoiceByLotNumber(lot_nmb).FirstOrDefault();
                if (infoInvoice != null)
                {
                    infoInvoice = Session["InfoEditInvoice"] as GetInfoInvoiceByLotNumber_Result;
                    return PartialView("RefreshInfoInvoice", infoInvoice);
                }
            }
            return null;
        }
        [HttpGet]
        public ActionResult ClearInvoice(int? lot_nmb)
        {
            if (lot_nmb.HasValue)
            {
                if (lot_nmb.HasValue)
                {
                    ConnectionLayerDB dbConnection = new ConnectionLayerDB("autoserviceDB");
                    var inputParams = new Dictionary<string, object> { { "lotNumb", lot_nmb } };
                    int status = dbConnection.ExecutionProcedure("ClearInvoice", inputParams);
                    if (status == 0)
                    {
                        var viewModel = this.GetAcceptedParts(lot_nmb.Value);
                        return PartialView("EmptyTableInvoice", viewModel.InfoInvoice);
                    }
                    else
                        return JavaScript("$('#popErorr').html('" + dbConnection.InfoMessage + "').slideDown(1000).delay(3000).slideUp(1000); ");
                }
            }
            return null;
        }
        public ActionResult SearchProvider(string id_prov, string name_prov)
        {
             IEnumerable<Provider> providers= db.Providers.Where(s=>s.ID_provider.Contains(id_prov)&&s.Name.Contains(name_prov)).Take(5);
             return PartialView("ResultSearchProvider", providers);
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [ChildActionOnly]
        public AcceptancedPartsViewModel GetAcceptedParts(int lot_nmb)
        {
            var totMoney = new ObjectParameter("totalMoney", typeof(decimal));
            List<GetAcceptancedSpareParts_Result> accpParts = db.GetAcceptancedSpareParts(lot_nmb, totMoney).ToList();
            var infoInvoice = db.GetInfoInvoiceByLotNumber(lot_nmb).FirstOrDefault();
            AcceptancedPartsViewModel viewModel = new AcceptancedPartsViewModel {AcceptencedParts = accpParts, TotallSummByParts = totMoney.Value as decimal?, InfoInvoice = infoInvoice};
            return viewModel;
        }
	}
}