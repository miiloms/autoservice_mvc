﻿using AutoserviceDiplom.Models;
using PagedList;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace AutoserviceDiplom.Controllers
{
    [Authorize]
    public class ServiceController : Controller
    {
        private autoserviceDEntities db = new autoserviceDEntities();

        public ActionResult List()
        {
            ServiceType allTypes= new ServiceType{IDtype=0, TypeName="Все категории"};
            List<ServiceType> listtype = db.ServiceTypes.ToList();
            listtype.Add(allTypes);
            SearchParamsService model = new SearchParamsService {ListTypeServices=new SelectList(listtype, "IDtype","TypeName",0)};
            return View(model);
        }

        public ActionResult Search(SearchParamsService parameters, int? page)
        {
            parameters.PriceStart=parameters.PriceStart ?? 0;
            parameters.PriceEnd = parameters.PriceEnd ?? int.MaxValue;
            parameters.ServId = parameters.ServId ?? "";
            parameters.ServName = parameters.ServName ?? "";
            var lowTypeId= parameters.IDTypeServices==0?1:parameters.IDTypeServices;
            var hiTypeId= parameters.IDTypeServices==0?int.MaxValue:parameters.IDTypeServices;
            List<Service> services = db.Services.Where(f => f.ID_service.Contains(parameters.ServId) && (f.IDtype >= lowTypeId&&f.IDtype<=hiTypeId)&&f.ServiceName.Contains(parameters.ServName)&&
                                                            f.PriceHour>=parameters.PriceStart&&f.PriceHour<=parameters.PriceEnd&&
                                                            (f.Available == (parameters.isAvlble??false) || f.Available == (parameters.isAvlble??true))).ToList();
            int pageSize = 12;
            int pageNumber = (page ?? 1);
            return PartialView("SearchResult", services.ToPagedList(pageNumber, pageSize));
        }
        // GET: /Service/Create
        public ActionResult Create()
        {
            ViewBag.ListTypes = new SelectList(db.ServiceTypes, "IDtype", "TypeName");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="ID_service,ServiceName,RateTime,PriceHour,IDtype,Available")] Service service)
        {
            if (ModelState.IsValid&&db.Posts.Find(service.ID_service) == null)
            {
                db.Services.Add(service);
                db.SaveChanges();
                return RedirectToAction("List");
            }

            ViewBag.ListTypes = new SelectList(db.ServiceTypes, "IDtype", "TypeName", service.IDtype);
            return View(service);
        }

        [HttpGet]
        public ActionResult Edit(string ID_service)
        {
            if (ID_service == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Service service = db.Services.Find(ID_service);
            if (service == null)
            {
                return HttpNotFound();
            }
            ViewBag.ListTypes = new SelectList(db.ServiceTypes, "IDtype", "TypeName");
            return View(service);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="ID_service,ServiceName,RateTime,PriceHour,IDtype,Available")] Service service)
        {
            if (ModelState.IsValid)
            {
                db.Entry(service).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("List");
            }
            ViewBag.IDtype = new SelectList(db.ServiceTypes, "IDtype", "TypeName", service.IDtype);
            return View(service);
        }

        [HttpGet, ActionName("Delete")]
        public ActionResult DeleteConfirmed(string ID_service)
        {
            Service service = db.Services.Find(ID_service);
            db.Services.Remove(service);
            db.SaveChanges();
            return RedirectToAction("List");
        }
        [HttpGet]
        public ActionResult Lock(string ID_service)
        {
            Service service = db.Services.Find(ID_service);
            service.Available = false;
            db.Entry(service).State = EntityState.Modified;
            db.SaveChanges();
            return PartialView("TableRow", service);
        }
        [HttpGet]
        public ActionResult UnLock(string ID_service)
        {
            Service service = db.Services.Find(ID_service);
            service.Available = true;
            db.Entry(service).State = EntityState.Modified;
            db.SaveChanges();
            return PartialView("TableRow", service);
        }
        public ActionResult CheckID(string ID_service)
        {
            if (db.Services.Find(ID_service) != null)
            {
                return Json(ID_service);
            }
            return null;
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
