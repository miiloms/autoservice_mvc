﻿using AutoserviceDiplom.Helpers;
using AutoserviceDiplom.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web.Mvc;
namespace AutoserviceDiplom.Controllers
{
     [Authorize]
    public class StoreSparePartsController : Controller
    {
        private autoserviceDEntities db = new autoserviceDEntities();
        //
        // GET: /StoreSpareParts/
        public ActionResult List()
        {
            Employee emptyEmpl=new Employee{ NameSerName="Выбрать сотрудника..."};
            List<Employee> collectionList = db.Employees.Where(f => f.Post.ID_post == "mngparts" && f.ContractToEmployees.FirstOrDefault(d => d.DismissDate == null) != null).ToList();
            collectionList.Add(emptyEmpl);
            ViewBag.ListEmployee = new SelectList(collectionList, "PersonnelNumber", "NameSerName", 0);
            return View();
        }
        public ActionResult Search(SearchPartsOfStoreViewModel model)
        {
            Session["searchParams"] = model;
            ObjectParameter countOfRows = new ObjectParameter("countOfRows", typeof(int));
            List<FindSpareParts_Result> result = db.FindSpareParts(model.ID_part,
                                                                    model.Name,
                                                                    model.Manufacture,
                                                                    model.PriceBefore,
                                                                    model.PriceAfter,
                                                                    model.ID_invoice,
                                                                    model.StockPercent,
                                                                    model.DateIvoiceFrom,
                                                                    model.DateIvoiceBefore,
                                                                    model.PersonnelNumber,
                                                                    "Name", true, 1, 8, countOfRows //для сортировки и пейджинга
                                                                    ).ToList();
            ViewBag.ListEmployee = new SelectList(db.Employees, "PersonnelNumber", "NameSerName");
                return PartialView("ResultSearch", result.ToCustomPageList(1, 8, countOfRows.Value as int?));
            
        }

        [HttpPost]
        public ActionResult SortByParam(int? nmb_clmn, bool? is_accend, int? PageNumber)
        {
            //save sort params in case those variables haven't values 
            if (nmb_clmn.HasValue && is_accend.HasValue)
            {
                Session["nmb_clmn"] = nmb_clmn;  // save at session prop
                Session["is_accend"] = is_accend;
            }
            else
            {
                nmb_clmn = Session["nmb_clmn"] as int?;  // get from session prop if vars is null
                is_accend =Session["is_accend"] as bool?;
            }
            ObjectParameter countOfRows = new ObjectParameter("countOfRows", typeof(int));
            is_accend= is_accend??true;
            string nameSortColumn = String.Empty;
            int pageSize = 8;
            int pageNumber = (PageNumber ?? 1);
            switch (nmb_clmn)
            {
                case 1: nameSortColumn = "Name"; break;
                case 2: nameSortColumn = "ID_part"; break;
                case 3: nameSortColumn = "LotNumber"; break;
                case 4: nameSortColumn = "Manufacture"; break;
                case 5: nameSortColumn = "Unit"; break;
                case 6: nameSortColumn = "Stock"; break;
                case 7: nameSortColumn = "Price"; break;
                case 8: nameSortColumn = "Trade_Increase"; break;
                case 9: nameSortColumn = "Cost"; break;
                case 10: nameSortColumn = "PersonnelNumber"; break;
                case 11: nameSortColumn = "DeliveryDate"; break;
                default: nameSortColumn = "Name"; break;
            }

            var model = Session["searchParams"] as SearchPartsOfStoreViewModel;
            if (model != null)
            {
                List<FindSpareParts_Result> result = db.FindSpareParts(model.ID_part,
                                                                        model.Name,
                                                                        model.Manufacture,
                                                                        model.PriceBefore,
                                                                        model.PriceAfter,
                                                                        model.ID_invoice,
                                                                        model.StockPercent,
                                                                        model.DateIvoiceFrom,
                                                                        model.DateIvoiceBefore,
                                                                        model.PersonnelNumber,
                                                                        nameSortColumn, is_accend, pageNumber, pageSize, countOfRows //для сортировки и пейджинга
                                                                        ).ToList();

                ViewBag.ListEmployee = new SelectList(db.Employees, "PersonnelNumber", "NameSerName");

                return PartialView("ResultSearch", result.ToCustomPageList(pageNumber, pageSize, countOfRows.Value as int?));
            }
            else
                return null;
        }

        [HttpPost]
        public ActionResult ChangeTradeIncrease(int[] ID_position, byte? trd_incr)
        {
            if (ID_position.Length != 0 && trd_incr.HasValue)
            {
                bool isError = false;
                foreach (var item in ID_position)
                {
                    var pos= db.Acceptance_Invoice.Find(item);
                    if (pos != null)
                    {
                        pos.Trade_Increase = trd_incr.Value;
                    }
                    else
                        isError = true;
                }
                if (!isError)
                {
                    try { db.SaveChanges(); return Json(1); }
                    catch (Exception ex) { return Json(-1); }
                }

                else
                    return Json(-1);
            }
            else
                return Json(-1);
        }
	}
}