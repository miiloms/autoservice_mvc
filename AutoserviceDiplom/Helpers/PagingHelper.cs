﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace AutoserviceDiplom.Helpers
{
    //шаблонный хелпер  пейджинга для локальных коллекций
    public static class PagingHelpersForModel
    {
        public static MvcHtmlString PageLinksFor<T>(this HtmlHelper html,
        PagedList.IPagedList<T> pageInfo, Func<int, string> onclickEvent)
        {
            StringBuilder result = new StringBuilder();
            if (pageInfo.PageCount >= 10)
            {
                for (int i = 1; i <= pageInfo.PageCount; i++)
                {
                    if (i == 1 || i <= pageInfo.PageNumber + 2 && i >= pageInfo.PageNumber - 2 || i == pageInfo.PageCount)
                    {
                        TagBuilder tag = new TagBuilder("button");
                        tag.MergeAttribute("onclick", onclickEvent(i));
                        tag.InnerHtml = i.ToString();
                        // если текущая страница, то выделяем ее,
                        // например, добавляя класс
                        if (i == pageInfo.PageNumber)
                        {
                            tag.AddCssClass("selected");
                            tag.AddCssClass("btn-primary");
                        }
                        tag.AddCssClass("btn btn-default");
                        result.Append(tag.ToString());
                    }
                    //else if (result[result.Length - 4] != 's')
                    //    result.Append("<span class='glyphicon glyphicon-minus'/>");
                }
            }
            else
            for (int i = 1; i <= pageInfo.PageCount; i++)
            {
                TagBuilder tag = new TagBuilder("button");
                tag.MergeAttribute("onclick", onclickEvent(i));
                tag.InnerHtml = i.ToString();
                // если текущая страница, то выделяем ее,
                // например, добавляя класс
                if (i == pageInfo.PageNumber)
                {
                    tag.AddCssClass("selected");
                    tag.AddCssClass("btn-primary");
                }
                tag.AddCssClass("btn btn-default");
                result.Append(tag.ToString());
            }
            return MvcHtmlString.Create(result.ToString());
        }


        //шаблонный хелпер  пейджинга для sql процедур
        public static MvcHtmlString PageProcLinksFor(this HtmlHelper html,
        PagingInfoProcedure pageInfo, Func<int, string> onclickEvent)
        {
            StringBuilder result = new StringBuilder();
            if (pageInfo.PageCount >= 10)
            {
                for (int i = 1; i <= pageInfo.PageCount; i++)
                {
                    if (i == 1 || i <= pageInfo.PageNumber + 2 && i >= pageInfo.PageNumber - 2 || i == pageInfo.PageCount)
                    {
                        TagBuilder tag = new TagBuilder("button");
                        tag.MergeAttribute("onclick", onclickEvent(i));
                        tag.InnerHtml = i.ToString();
                        // если текущая страница, то выделяем ее,
                        // например, добавляя класс
                        if (i == pageInfo.PageNumber)
                        {
                            tag.AddCssClass("selected");
                            tag.AddCssClass("btn-primary");
                        }
                        tag.AddCssClass("btn btn-default");
                        result.Append(tag.ToString());
                    }
                    //else if (result[result.Length - 4] != 's')
                    //    result.Append("<span class='glyphicon glyphicon-minus'/>");
                }
            }
            else
                for (int i = 1; i <= pageInfo.PageCount; i++)
                {
                    TagBuilder tag = new TagBuilder("button");
                    tag.MergeAttribute("onclick", onclickEvent(i));
                    tag.InnerHtml = i.ToString();
                    // если текущая страница, то выделяем ее,
                    // например, добавляя класс
                    if (i == pageInfo.PageNumber)
                    {
                        tag.AddCssClass("selected");
                        tag.AddCssClass("btn-primary");
                    }
                    tag.AddCssClass("btn btn-default");
                    result.Append(tag.ToString());
                }
            return MvcHtmlString.Create(result.ToString());
        }
    }
}