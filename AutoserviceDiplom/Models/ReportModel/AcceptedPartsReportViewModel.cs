﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoserviceDiplom.Models.ReportModel
{
    public class AcceptedPartsReportViewModel
    {
        public string NameClient { get; set; }
        public string Adress { get; set; }
        public string Phone { get; set; }
        public string NameEmployee { get; set; }
        public int ID_doc { get; set; }
        public DateTime Date { get; set; }
        public List<AcceptanceCustomSPart> SpareParts { get; set; }
    }
}