﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoserviceDiplom.Models
{
    public class TableLoad_Result
    {
        public Nullable<double> h09_00 { set; get; }
        public Nullable<double> h10_00 { set; get; }
        public Nullable<double> h11_00 { set; get; }
        public Nullable<double> h12_00 { set; get; }
        public Nullable<double> h13_00 { set; get; }
        public Nullable<double> h14_00 { set; get; }
        public Nullable<double> h15_00 { set; get; }
        public Nullable<double> h16_00 { set; get; }
        public Nullable<double> h17_00 { set; get; }
        public Nullable<double> h18_00 { set; get; }
        public int PersonnelNumber{set;get;}
        public string NameSerName{set;get;}
        public string NamePost { set; get; }
        public Nullable<DateTime> CurrentDate { set; get;}

    }
}