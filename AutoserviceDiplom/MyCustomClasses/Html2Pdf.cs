﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Web.Mvc;

using AutoserviceDiplom.Controllers;
//////////////////////////// для работы с pdf
using SelectPdf;
using System.Text;
/////////////////////////////////
namespace AutoserviceDiplom.MyCustomClasses
{
    public static  class Html2Pdf
    {
        private  static string RenderRazorViewToString(string viewName, object model, ControllerContext controllerContext, ViewDataDictionary ViewData, TempDataDictionary TempData)
        {
            ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(controllerContext, viewName);
                var viewContext = new ViewContext(controllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(controllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }

        //рендеринг и ковертация  html в pdf с обратной отсылкой на запрос
        public static void ConvertHtml2PdfResponse(Controller controller, string viewName, object model, PdfPageSize sizePage = PdfPageSize.A4, PdfPageOrientation orientationPage = PdfPageOrientation.Portrait)
        {
            string htmlString = RenderRazorViewToString(viewName, model, controller.ControllerContext, controller.ViewData, controller.TempData);

            PdfPageSize pageSize = sizePage;
            PdfPageOrientation pdfOrientation = orientationPage;

            int webPageWidth =orientationPage==PdfPageOrientation.Landscape? 1450:1150;
            int webPageHeight = 0;

            // instantiate a html to pdf converter object
            HtmlToPdf converter = new HtmlToPdf();

            // set converter options
            converter.Options.PdfPageSize = pageSize;
            converter.Options.PdfPageOrientation = pdfOrientation;
            converter.Options.WebPageWidth = webPageWidth;
            converter.Options.WebPageHeight = webPageHeight;
            converter.Options.WebPageFixedSize = false;
            converter.Options.AutoFitWidth = HtmlToPdfPageFitMode.ShrinkOnly;
            converter.Options.AutoFitHeight = HtmlToPdfPageFitMode.NoAdjustment;
            converter.Options.MarginLeft = 10;
            converter.Options.MarginRight = 10;
            converter.Options.MarginTop = 20;
            converter.Options.MarginBottom = 20;
            // create a new pdf document converting an url
            string urlCurrent = controller.Request.Url.ToString();
            PdfDocument doc = converter.ConvertHtmlString(htmlString, urlCurrent);
            // save pdf document
            doc.Save(controller.HttpContext.ApplicationInstance.Response, true, viewName+".pdf");

            doc.Close();
            ////////////////////////////
        }

        //рендеринг и ковертация  html в pdf с обратной отсылкой на запрос c пейджингом  и хедерами таблиц в шапке
        public static void ConvertHtml2PdfResponse(Controller controller, string viewName, object model, bool isPaging, string htmlTableHeader , PdfPageSize sizePage = PdfPageSize.A4, PdfPageOrientation orientationPage = PdfPageOrientation.Portrait)
        {
            string htmlString = RenderRazorViewToString(viewName, model, controller.ControllerContext, controller.ViewData, controller.TempData);

            PdfPageSize pageSize = sizePage;
            PdfPageOrientation pdfOrientation = orientationPage;

            int webPageWidth = orientationPage == PdfPageOrientation.Landscape ? 1450 : 1150;
            int webPageHeight = 0;

            // instantiate a html to pdf converter object
            HtmlToPdf converter = new HtmlToPdf();

            // set converter options
            converter.Options.PdfPageSize = pageSize;
            converter.Options.PdfPageOrientation = pdfOrientation;
            converter.Options.WebPageWidth = webPageWidth;
            converter.Options.WebPageHeight = webPageHeight;
            converter.Options.WebPageFixedSize = false;
            converter.Options.AutoFitWidth = HtmlToPdfPageFitMode.ShrinkOnly;
            converter.Options.AutoFitHeight = HtmlToPdfPageFitMode.NoAdjustment;
            converter.Options.MarginLeft = 10;
            converter.Options.MarginRight = 10;
            converter.Options.MarginTop = 20;
            converter.Options.MarginBottom = 20;
            if (isPaging)
            {
                // footer settings
                converter.Options.DisplayFooter = true;
                converter.Footer.DisplayOnFirstPage = true;
                converter.Footer.DisplayOnOddPages = true;
                converter.Footer.DisplayOnEvenPages = true;
                converter.Footer.Height = 30;
                // page numbers can be added using a PdfTextSection object
                PdfTextSection text = new PdfTextSection(5, 5, "Страница: {page_number} из {total_pages}", new System.Drawing.Font("Arial", 8));
                text.HorizontalAlign = PdfTextHorizontalAlign.Center;
                converter.Footer.Add(text);
            }
            // header settings
            converter.Options.DisplayHeader = true;
            converter.Header.DisplayOnFirstPage = false;
            converter.Header.DisplayOnOddPages = true;
            converter.Header.DisplayOnEvenPages = true;
            converter.Header.Height = 30;
             //add some html content to the header
            PdfHtmlSection headerHtml = new PdfHtmlSection(htmlTableHeader, null);
            headerHtml.WebPageWidth = PdfPageOrientation.Landscape ==orientationPage  ? 1450 : 1150;
            headerHtml.AutoFitHeight = HtmlToPdfPageFitMode.NoAdjustment;
            headerHtml.AutoFitWidth = HtmlToPdfPageFitMode.ShrinkOnly;
            converter.Header.Add(headerHtml);
            // create a new pdf document converting an url
            PdfDocument doc = converter.ConvertHtmlString(htmlString);
            // save pdf document
            doc.Save(controller.HttpContext.ApplicationInstance.Response, true, viewName + ".pdf");

            doc.Close();
            ////////////////////////////
        }
    }
}