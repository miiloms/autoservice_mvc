﻿using AutoserviceDiplom.Models;
using AutoserviceDiplom.MyCustomClasses;
using PagedList;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace AutoserviceDiplom.Controllers
{
    [Authorize]
    public class PartsController : Controller
    {
        IdentityContext db_identity = new IdentityContext();
        autoserviceDEntities db = new autoserviceDEntities();
        [HttpGet]
        public ActionResult AttachSpareParts(string ID_client, string ID_service, string ID_order)
        {
            if (ID_order != null && ID_service != null && ID_order != null)
            {
                var attachedParts = db.GetAttachedParts(ID_order, ID_service);
                var attachedCustomParts = db.GetAttachedCustomParts(ID_order, ID_service);
                AttachedSparePartViewModel attachedSparePart = new AttachedSparePartViewModel { ID_service = ID_service, ID_client = ID_client, ID_order = ID_order, Name_service = db.Services.Find(ID_service).ServiceName };
                attachedSparePart.AttachedPartsByService = attachedParts;
                attachedSparePart.AttachedCustomPartsByService = attachedCustomParts;
                ViewBag.IsClosedOrder = db.OrderServs.Find(ID_order).DateFactCompleting != null ? true : false;
                Session.Add("ViewModelAttachedParts", attachedSparePart);
                return PartialView(attachedSparePart);
            }
            else
                 return HttpNotFound();
        }
        [HttpGet]
        public ActionResult PartsSearch(string name, string manufacture, float? price_from, float? price_to)
        {
            var stockSParts = db.StockOfSpareParts.Where(p =>p.Stock>0&& p.Name.Contains(name) && p.Manufacture.Contains(manufacture) && (float)p.Price.Value >= (price_from??0) && (float)p.Price.Value <= (price_to??1000000)).Take(6);
            if (stockSParts.ToArray().Length != 0)
            {
                var model = Session["ViewModelAttachedParts"] as AttachedSparePartViewModel;
                model.StockOfParts = stockSParts;
                return PartialView(model);
            }
            else
                Response.Write("Нет данных по запросу...");
            return null;
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AttachCurrentSparePart(UsingPartMaterial attachingSparePart)
        {
            var user=db_identity.Users.Where(n => n.UserName == User.Identity.Name);
            int? persNumber=null;
            if (user != null)
                persNumber = db_identity.Users.Where(n => n.UserName == User.Identity.Name).FirstOrDefault().PersonnelNumber;// получаем реальный таб номер сотрудника
            int a = db.AttachSPartToService(attachingSparePart.ID_order, attachingSparePart.ID_service, attachingSparePart.ID_position, persNumber, attachingSparePart.Number);
            if (a > 0)
            {
                var model = Session["ViewModelAttachedParts"] as AttachedSparePartViewModel;
                model.AttachedPartsByService = db.GetAttachedParts(model.ID_order, model.ID_service);
                return PartialView("UpdatedTableSpareParts", model);
            }
            else
                return JavaScript("$('#popErorr2').html('Невозможно прикрепить материал!').slideDown(300).delay(2000).slideUp(300); ");
        }
        [HttpPost]
        public ActionResult DeleteAttachedPart(UsingPartMaterial attachingSparePart)
        {
            if (attachingSparePart != null)
            {
                db.Entry(attachingSparePart).State = EntityState.Deleted;
                db.SaveChanges();
                var model = Session["ViewModelAttachedParts"] as AttachedSparePartViewModel;
                model.AttachedPartsByService = db.GetAttachedParts(model.ID_order, model.ID_service);
                return PartialView("UpdatedTableSpareParts", model);
            }
            return HttpNotFound();
        }
        [HttpPost]
        public ActionResult DeleteAttachedCustomPart(UsingCustomSPartMat attachingCustomPart)
        {
            if (attachingCustomPart != null)
            {
                db.Entry(attachingCustomPart).State = EntityState.Deleted;
                db.SaveChanges();
                var model = Session["ViewModelAttachedParts"] as AttachedSparePartViewModel;
                model.AttachedCustomPartsByService = db.GetAttachedCustomParts(model.ID_order, model.ID_service);
                return PartialView("UpdatedTableCustomParts", model);
            }
            return HttpNotFound();
        }
        [HttpGet]
        public ActionResult CustomPartsSearch(string name, string manufacture, int? acceptanceID)
        {
            try
            {
                var model = Session["ViewModelAttachedParts"] as AttachedSparePartViewModel;
                IEnumerable<GetStockCustomPartsById_Result> stockCustomParts = db.GetStockCustomPartsById(model.ID_client).ToList().Where(m => m.Name.Contains(name) || m.Manufacture.Contains(manufacture) || m.ID_acceptance == (acceptanceID ?? 0));
                if (stockCustomParts.ToArray().Length  != 0)
                {
                    model.StockCustomOfParts = stockCustomParts;
                    return PartialView(model);
                }
                else
                    Response.Write("Нет данных по запросу...");
                return null;
            }
            catch (Exception e)
            { return null; }
        }
        [HttpPost]
        public ActionResult AttachCurrentCustomPart(UsingCustomSPartMat model)
        {
            var inputParams = new Dictionary<string, object> { 
                                                                { "id_part", model.ID_part }, 
                                                                { "id_acceptance", model.ID_acceptance }, 
                                                                { "id_service", model.ID_service }, 
                                                                {"id_order", model.ID_order},
                                                                {"number", model.Number}
                                                            };
            ConnectionLayerDB dbConnection = new ConnectionLayerDB("autoserviceDB");
            int stat = dbConnection.ExecutionProcedure("AttachCustomPart", inputParams);
            if (stat > 0)
            {
                var attachedCustomPart = Session["ViewModelAttachedParts"] as AttachedSparePartViewModel;
                attachedCustomPart.AttachedCustomPartsByService = db.GetAttachedCustomParts(model.ID_order, model.ID_service);
                return PartialView("UpdatedTableCustomParts", attachedCustomPart);
            }
            else
            {
                return JavaScript("$('#popErorr2').html('"+dbConnection.InfoMessage+"').slideDown(300).delay(2000).slideUp(300); ");
            }
            
        }

        public ActionResult List()
        {
            return View();
        }
        public ActionResult Search(string namePart, string id, int? page)
        {
            List<SparePartMaterial> parts = db.SparePartMaterials.Where(s => s.Name.Contains(namePart) || s.ID_part.Contains(id)).ToList();
            if (parts.Count == 0)
            { return HttpNotFound(); }
            else
            {
                int pageSize = 12;
                int pageNumber = (page ?? 1);
                return PartialView("SearchResultPartial", parts.ToPagedList(pageNumber, pageSize));
            }
        }

        public ActionResult Details(string ID_part)
        {
            if (ID_part == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var parts = db.SparePartMaterials.Find(ID_part);
            if (parts == null)
            {
                return HttpNotFound();
            }
            return View(parts);
        }
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(SparePartMaterial part)
        {
            if (ModelState.IsValid && db.SparePartMaterials.Find(part.ID_part)==null)
            {
                db.SparePartMaterials.Add(part);
                db.SaveChanges();
                return RedirectToAction("Details", new { @ID_part = part.ID_part });
            }
            return View(part);
        }

        public ActionResult CheckID(string ID_part)
        {
            if (db.SparePartMaterials.Find(ID_part) != null)
                return Json(-1, JsonRequestBehavior.AllowGet);
            return null;
        }

        public ActionResult Edit(string ID_part)
        {
            if (ID_part == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SparePartMaterial part = db.SparePartMaterials.Find(ID_part);
            if (part == null)
            {
                return HttpNotFound();
            }
            return View(part);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(SparePartMaterial part)
        {
            if (ModelState.IsValid)
            {
                db.Entry(part).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Details", new { @ID_part = part.ID_part });
            }
            return View(part);
        }

        [HttpGet, ActionName("Delete")]
        public ActionResult DeleteConfirmed(string ID_part)
        {
            SparePartMaterial part = db.SparePartMaterials.Find(ID_part);
            if (part != null && part.Acceptance_Invoice.Count == 0)
            {
                db.SparePartMaterials.Remove(part);
                db.SaveChanges();
                return Json(1, JsonRequestBehavior.AllowGet);
            }
            return Json(0, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
	}
}