﻿using AutoserviceDiplom.Hubs;
using AutoserviceDiplom.Models;
using AutoserviceDiplom.MyCustomClasses;
using System;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using AutoserviceDiplom.Controllers;

namespace AutoserviceDiplom.Modules
{
    public class TimerModule:IHttpModule
    {
        static Timer timer;
        long interval = 60000;
        static int lastMinute=30;
        static object synclock = new object();
        public void Init(HttpApplication context)
        {
            timer = new Timer(new TimerCallback(CheckPreRecords), null, 0, interval);
        }

        private void CheckPreRecords(object obj) 
        {
            lock (synclock)
            {
               DateTime dd = DateTime.Now;
               if (dd.Minute == lastMinute)
                   return;
               autoserviceDEntities db = new autoserviceDEntities();
               var records = db.CheckReservTimeForOverdue().ToList();
               switch (dd.Minute)
               {
                   case 45: 
                       if (records.Count > 0)
                       {
                           EmptyController controller = ViewRenderer.CreateController<EmptyController>();
                           var context = controller.ControllerContext;
                           controller.ViewBag.WarningType = 1;
                           string messageHtml = ViewRenderer.RenderPartialView("~/views/Notification/CheckedPreOrders_45m_Partial.cshtml", records, context);
                           this.SendMessage(messageHtml);
                           lastMinute = dd.Minute;
                       }
                        break;
                   case 0:    
                           if (records.Count > 0)
                           {
                               EmptyController controller = ViewRenderer.CreateController<EmptyController>();
                               var context = controller.ControllerContext;
                               controller.ViewBag.WarningType = 2;
                               string messageHtml = ViewRenderer.RenderPartialView("~/views/Notification/CheckedPreOrders_45m_Partial.cshtml", records, context);
                               this.SendMessage(messageHtml);
                               lastMinute = dd.Minute;
                           }
                            break;
                   case 15: 
                           if (records.Count > 0)
                               {
                                   EmptyController controller = ViewRenderer.CreateController<EmptyController>();
                                   var context = controller.ControllerContext;
                                   controller.ViewBag.WarningType = 3;
                                   string messageHtml = ViewRenderer.RenderPartialView("~/views/Notification/CheckedPreOrders_45m_Partial.cshtml", records, context);
                                   this.SendMessage(messageHtml);
                                   lastMinute = dd.Minute;
                               }
                                break;
               }
            }
  
        }
        public void Dispose()
        { }
        private void SendMessage(string message)
        {
            // Получаем контекст хаба
            var context =Microsoft.AspNet.SignalR.GlobalHost.ConnectionManager.GetHubContext<NotificationHub>();
            // отправляем сообщение
            context.Clients.All.displayMessage(message);
        }
    }
}