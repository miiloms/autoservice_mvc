﻿using AutoserviceDiplom.Models;
using System.Web.Mvc;

namespace AutoserviceDiplom.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        autoserviceDEntities db = new autoserviceDEntities();
        public ActionResult Index()
        {
            return View();
        }
    }
}