﻿$(function () {
    $('button#info').click(function (event) {
        showDetail();
    })
    $('#dateb').datetimepicker({ language: 'ru', pickTime: false, format: 'YYYY-MM-DD', defaultDate: moment().format('YYYY-MM-DD') });
    //для автозаполения поискового запроса
    $("[data-autocomplete-source]").each(function () {
        var target = $(this);
        target.autocomplete({ minLength: 1, autoFocus: false, source: target.attr("data-autocomplete-source") });
    });
    //для модального окна
    $('#info, #btnAddCar, #btnEditCar').click(function (event) { 
        event.preventDefault(); 
        $('#overlay_dark').fadeIn(400, 
            function () { 
                $('#modal_form')
                    .css('display', 'block') 
                    .animate({ opacity: 1, top: '50%' }, 200);
            });
    });
    $('#modal_close, #overlay_dark').click(function () {
        $('#modal_form')
            .animate({ opacity: 0, top: '45%' }, 200, 
                function () { 
                    $(this).css('display', 'none'); 
                    $('#overlay_dark').fadeOut(400);
                }
            );
    });
});

$.ajaxSetup({ cache: false });

function CreateCar() {
    $.ajax({
        url: "/Car/Create",
        datatype: "json",
        beforeSend: showSpinner,
        complete: hideSpinner,
        success: function (result) { $("#contents").html(result); }
    });
}


function EditCar() {
    var vinID = $("#ddlist").val();
    if (vinID != null)
        $.ajax({
            url: "/Car/Edit",
            data: 'VIN=' + vinID,
            datatype: "json",
            beforeSend: showSpinner,
            complete: hideSpinner,
            success: function (result) { $("#contents").html(result); }
        });
    else
        $('#overlay_dark, #modal_close').click();
}

function showSpinner() {
    $('#loading').show();
}

function hideSpinner() {
    $('#loading').hide();
}

function addVinCar(vinNumb) {
    $.ajax(
        {
            url: '/Car/AttachCar',
            data: $("#search"),
            type: "POST",
            datatype: "json",
            success: function (result) {
                $('#ddlist option:selected').removeAttr('selected');
                $("#ddlist").prepend($('<option value=' + result[0].vnumb + '>' + result[0].model + '(' + result[0].number + ')' + '</option>'));
                $("#ddlist :first").attr("selected", "selected");
            }
        })
};
function showDetail() {
    var vinID = $("#ddlist").val();
    if (vinID != null)
        $.ajax(
            {
                url: '/Car/Detail',
                data: 'VIN=' + vinID,
                type: "Get",
                beforeSend: showSpinner,
                complete: hideSpinner,
                datatype: "json",
                success: function (result) { $("#contents").html(result); }
            });
    else
        $('#overlay_dark, #modal_close').click();
}

//global variables for counter  about item info
var _gCount = 1;
var yClick, xClick;
//for remark_car
$().ready(function () {
    //close window
    $('.my-remark  .close-sign').click(function () {
        //set default values
        $('.my-remark .glyphicon-remove-circle').hide();
        $('#contents_remark div:nth-child(1)').text('Добавить метку');
        $('.my-remark input[name="id_item"]').removeAttr('value');
        $('#addonInfo').val('');
        $('.my-remark').fadeOut(100);
    });
    $('#state_car').click(function (e) {
        var target = $(e.target);
        if (!(target.is('.my-remark, .my-remark *, .item-remark, .item-remark *'))) {
            //gaining click's coords
            xClick = Math.round(e.pageX - $(this).offset().left);
            yClick = Math.round(e.pageY - $(this).offset().top);
            var h = $('.my-remark').height();
            $('.my-remark').css('top', yClick - h * 0.5 - 2).css('left', xClick + 19);
            $('.my-remark').fadeIn(100);
        }
    });
    //forming data and sending to server for event CLICK for adding Items
    $('.my-remark  .glyphicon-ok-circle').click(function () {
        var editId = $('.my-remark input[name="id_item"]').val();
        var jSondata = {
            tempID: editId == '' ? _gCount : editId,
            TypeMark: $('[name="TypeMark"]:checked').val(),
            addonInfo: $('#addonInfo').val(),
            X_Axis: xClick - 10,
            Y_Axis: yClick - 10,
            IsEdit: editId == '' ? false : true
        };
        $.ajax(
            {
                url: '/Order/AddRemarkItem',
                data: jSondata,
                type: "POST",
                datatype: "json",
                success: function (result) {
                    if (result.stat == 1) {
                        if (!result.IsEdit) {
                            var new_mark_item = $('#item-remark-id').clone().removeAttr('id');//clone info item
                            $('#state_car').append(new_mark_item);
                            new_mark_item.css('top', jSondata.Y_Axis).css('left', jSondata.X_Axis);
                            new_mark_item.find('input').attr('value', _gCount);
                        }
                        else {
                            var new_mark_item = $('.item-remark [value="' + jSondata.tempID + '"]').parent();
                        }
                        switch (jSondata.TypeMark) {
                            case '1': new_mark_item.find('span').text("P"); break;
                            case '2': new_mark_item.find('span').text("X"); new_mark_item.css({ borderColor: '#D56161', background: '#FF7474' }); break;
                            case '3': new_mark_item.find('span').text("Y"); new_mark_item.css({ borderColor: '#5E8DC7', background: '#74AFF7' }); break;
                            case '4': new_mark_item.find('span').text("O"); new_mark_item.css({ borderColor: '#A99A2C', background: '#D8C95B' }); break;
                        }
                        new_mark_item.fadeIn(200);
                        _gCount += editId == '' ? 1 : 0;
                        $('.my-remark  .close-sign').click();
                    }
                    else { $('.item-remark:has(input)').remove(); alert("Сессия устарела!"); $('.my-remark').fadeOut(100); }
                }
            });
    });
    //get info about point
    $('#state_car').on('click', '.item-remark', function (e) {
        $.ajax(
            {
                url: '/Order/GetInfoRemarkItem',
                data: { tempID: $(this).find('input').val() },
                type: "POST",
                datatype: "json",
                success: function (result) {
                    $('#addonInfo').val(result.addonInfo);
                    $('input[type="radio"][value="' + result.TypeMark + '"]').click();
                    $('.my-remark').css('top', result.Y_Axis - 120 * 0.5 + 15).css('left', result.X_Axis + 29);
                    $('.my-remark input[name="id_item"]').attr('value', result.tempID);
                    $('#contents_remark div:nth-child(1)').text('Изменить метку');
                    $('.my-remark').fadeIn(100);
                    $('.my-remark .glyphicon-remove-circle').show();

                }
            });
    });
    //delete info about point
    $('.glyphicon-remove-circle').click(function () {
        var temp_id = $('.my-remark').find('input[name="id_item"]').val();
        $.ajax(
            {
                url: '/Order/DeleteInfoRemarkItem',
                data: { tempID: temp_id },
                type: "POST",
                datatype: "json",
                success: function (result) {
                    if (result.stat) {
                        $('.item-remark [value="' + temp_id + '"]').parent().remove();
                        $('.my-remark  .close-sign').click();
                    }
                }
            });
    });
});