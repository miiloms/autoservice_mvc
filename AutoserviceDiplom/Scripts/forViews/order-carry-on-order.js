﻿//global var
var resultJsonRemarks;
// loading information about car's remarks 
$().ready(function () {
    $.ajax(
       {
           url: '/Order/GetInfoRemarkItemFromDB',
           data: 'ID_order=' + $('#ID_order').val(),
           type: "Post",
           datatype: "json",
           success: function (result) {
               resultJsonRemarks = result;
               for (key in result) {
                   var new_mark_item = $('#item-remark-id').clone().removeAttr('id');//clone info item
                   $('#state_car').append(new_mark_item);
                   new_mark_item.css('top', result[key].Y_Axis_pos).css('left', result[key].X_Axis_pos);
                   new_mark_item.find('input').attr('value', result[key].ID_remark);
                   switch (result[key].NumberType) {
                       case 1: new_mark_item.find('span').text("P"); break;
                       case 2: new_mark_item.find('span').text("X"); new_mark_item.css({ borderColor: '#D56161', background: '#FF7474' }); break;
                       case 3: new_mark_item.find('span').text("Y"); new_mark_item.css({ borderColor: '#5E8DC7', background: '#74AFF7' }); break;
                       case 4: new_mark_item.find('span').text("O"); new_mark_item.css({ borderColor: '#A99A2C', background: '#D8C95B' }); break;
                   }
                   new_mark_item.fadeIn(200);
               }
           }
       });
    //set onlclick for remark's item
    $('#state_car').on('click', '.item-remark', function (e) {

        for (key in resultJsonRemarks) {
            if (resultJsonRemarks[key].ID_remark == $(this).find('input').val()) {
                var xClick = resultJsonRemarks[key].X_Axis_pos;
                var yClick = resultJsonRemarks[key].Y_Axis_pos;
                $('#addonInfo').val(resultJsonRemarks[key].RemarkText);
                $('input[type="radio"][value="' + resultJsonRemarks[key].NumberType + '"]').click();
                var h = $('.my-remark').height();
                $('.my-remark').css('top', yClick - h * 0.5 + 8).css('left', xClick + 29);
                $('.my-remark').fadeIn(100);
                break;
            }
        }
    });
    $('.my-remark  .close-sign').click(function () {
        //set default values
        $('#addonInfo').val('');
        $('.my-remark').fadeOut(100);
    });
})
//подгружаем сводную информацию о заказе
function loadInfo() {
    $("#panelInfo").html(''); // предварительно очищаем
    $.ajax(
       {
           url: '/Order/TotalInfoServiceByOrder',
           data: 'id=' + $('#ID_order').val(),
           type: "Get",
           complete: step2,
           datatype: "json",
           success: function (result) { $("#panelInfo").append(result); }
       });

    function step2() {
        $.ajax(
          {
              url: '/Order/TotalInfoAttachedPartByOrder',
              data: 'id=' + $('#ID_order').val(),
              type: "Get",
              complete: step3,
              datatype: "json",
              success: function (result) { $("#panelInfo").append(result); }
          });
        function step3() {
            $.ajax(
              {
                  url: '/Order/TotalInfoAttachedCustomPartByOrder',
                  data: 'id=' + $('#ID_order').val(),
                  type: "Get",
                  beforeSend: showSpinnerLine,
                  complete: hideSpinnerLine,
                  datatype: "json",
                  success: function (result) { $("#panelInfo").append(result); }
              });
        }
    }
}
$(function () {
    if ($('#isclosed').val()=='False') {//выполнить, только если заказ открыт
        $('#toggle-accept').change(function () {
            if (this.checked == true) {
                $('#formCloseRejection').animate({ opacity: 1, right: '80%' }, 1000).show();
                $('#formCloseSuccess').animate({ opacity: 0, right: '80%' }, 1000).hide();
            }
            else {
                $('#formCloseRejection').animate({ opacity: 0, right: '80%' }, 500).hide();
                $('#formCloseSuccess').animate({ opacity: 1, right: '80%' }, 500).show();
            }

        })
        $('#toggle-accept').bootstrapToggle({
            on: 'Отказано',
            off: 'Принято',
            onstyle: 'danger',
            offstyle: 'success',
            width: 150,
            height: 35,
            style: 'my_toggle'
        });
        $('div[class="input-group date col-md-12"]').datetimepicker({ language: 'ru', pickTime: false, format: 'YYYY-MM-DD' });
    }
});

function showSpinner() {
    $('#loading').show();
}

function hideSpinner() {
    $('#loading').hide();
}
function showSpinnerLine() {
    $('#loading_line2').show();
}

function hideSpinnerLine() {
    $('#loading_line2').hide();
}
//для пейджинга-сброс номера отсылаемой страницы при изменении строки поиска услуг
$(document).ready(function () {
    $('input[name="servname"]').change(function () {
        $('#page').removeAttr('value');
    })
})
//редактирование существующей услуги (получение данных)
function EditAttachedService(Serv) {
    $.ajax(
        {
            url: '/Order/EditService',
            data: 'idServ=' + Serv + '&' + 'idOrder=' + $("#ID_order").val(),
            type: "GET",
            datatype: "json",
            beforeSend: showSpinner,
            complete: hideSpinner,
            success: function (result) { $("#contents1").html(result); }
        })
};
//функция для  отправки запроса на прикрепленные запчасти к услуге
function AttachedParts(idServ) {
    $.ajax(
    {
        url: '/Parts/AttachSpareParts',
        data: 'ID_client=' + $('#ID_client').val() + '&ID_service=' + idServ + '&ID_order=' + $("#ID_order").val(),
        type: "Get",
        beforeSend: showSpinnerLine,
        complete: hideSpinnerLine,
        datatype: "json",
        success: function (result) { $("#contents2").html(result); }
    });
}
//для удалениия услуги из заказа
function DeleteAttachedService(idServ) {
    $.ajax(
    {
        url: '/Order/DeleteAttachedService',
        data: 'ID_service=' + idServ + '&ID_order=' + $("#ID_order").val(),
        type: "Get",
        datatype: "json"
    });
}
//добавление новой услуги (получение)
function addSertoOrd(Serv) {
    $.ajax(
        {
            url: '/Order/AddService',
            data: 'idServ=' + Serv + '&' + 'idOrder=' + $("#ID_order").val(),
            type: "GET",
            datatype: "json",
            beforeSend: showSpinner,
            complete: hideSpinner,
            success: function (result) { $("#contents1").html(result); }
        })
};
// для добавления услуги(отправка экземпляра услуги)
function addService() {
    $.ajax(
        {
            url: '/Order/AddService',
            data: $("#formAddService").serialize(),
            type: "POST",
            datatype: "json",
            beforeSend: showSpinner,
            complete: hideSpinner,
            success: function (result) {
                //замена  таблицы услуг
                $("#execServices").replaceWith(result);
            }
        })
};

$(document).ready(function () { 
    //для редактирования услуги
    $('#collapseOne, #collapseTwo').on('click', '.glyphicon-edit, button#btnAdd', function (event) {
        event.preventDefault(); 
        $('#overlay_dark').fadeIn(400, 
            function () { 
                $('#modal_form')
                    .css('display', 'block') 
                    .animate({ opacity: 1, top: '50%' }, 200); 
            });
    });
    /* Зaкрытие мoдaльнoгo oкнa, тут делaем тo же сaмoе нo в oбрaтнoм пoрядке */
    $('#modal_close, #modal_close_parts, #overlay_dark').click(function () {
        $('#modal_form, #modal_form_attach')
            .animate({ opacity: 0, top: '45%' }, 100, 
                function () { 
                    $(this).css('display', 'none'); 
                    $('#overlay_dark').fadeOut(400); 
                }
            );
        clearInterval(inter_check);
    });
    // окно прикрепления запчастей
    $('#collapseOne').on('click', 'span[class="badge"]', function (event) {
        event.preventDefault(); 
        inter_check = setInterval(posi, 50);
        $('#overlay_dark').fadeIn(400, 
            function () { 
                $('#modal_form_attach')
                    .css('display', 'block')
                    .animate({ opacity: 1, top: '50%' }, 200);
            });
    });
});

//для автовыравнивания  модального окна (добавление запчастей) по вертикали
var inter_check;
function posi(e) {
    var currentHeight = $('#modal_form_attach').css('height');
    var currNumb = Number(currentHeight.replace('px', ''));
    if (currNumb / 2 < 400)
        $('#modal_form_attach').css('marginTop', -currNumb / 2);
}

$(document).ready(function () {
    //подгружаем сводную информацию о заказе после загрузки страницы в три шага только для закрытого заказа
    if ($('#isclosed').val()=='True') {
        $.ajax(
           {
               url: '/Order/TotalInfoServiceByOrder',
               data: 'id=' + $('#ID_order').val(),
               type: "Get",
               complete: step2,
               datatype: "json",
               success: function (result) { $("#panelInfo").append(result); }
           });

        function step2() {
            $.ajax(
              {
                  url: '/Order/TotalInfoAttachedPartByOrder',
                  data: 'id=' + $('#ID_order').val(),
                  type: "Get",
                  complete: step3,
                  datatype: "json",
                  success: function (result) { $("#panelInfo").append(result); }
              });
            function step3() {
                $.ajax(
                  {
                      url: '/Order/TotalInfoAttachedCustomPartByOrder',
                      data: 'id=' + $('#ID_order').val(),
                      type: "Get",
                      beforeSend: showSpinnerLine,
                      complete: hideSpinnerLine,
                      datatype: "json",
                      success: function (result) { $("#panelInfo").append(result); }
                  });
            }
        }
    }
});