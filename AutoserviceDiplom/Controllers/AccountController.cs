﻿using AutoserviceDiplom.Models;
using AutoserviceDiplom.Models.RoleModels;
using AutoserviceDiplom.Models.UserModels;
using AutoserviceDiplom.MyCustomClasses;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
namespace AutoserviceDiplom.Controllers
{
    [Authorize(Roles="admin")]
    public class AccountController : Controller
    {
        autoserviceDEntities db = new autoserviceDEntities();
        private ApplicationUserManager UserManager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
        }
        public  ActionResult Index()
        {
            return View();
        }
        public ActionResult ListUser()
        {
            ViewBag.ListEmployee = db.Employees.ToList();
            ViewBag.UserManager = UserManager;
            return View(UserManager.Users.ToList());
        }

        public  ActionResult ResetPass(string name)
        {
            ApplicationUser user = UserManager.FindByName(name);
            if (user != null)
            {
                NewPasswordModel newPassModel = new NewPasswordModel { LoginName = user.UserName };
                return View(newPassModel);
            }
            return HttpNotFound();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPass(NewPasswordModel newPassModel)
        {
            if (ModelState.IsValid)
            {
                ApplicationUser user= await UserManager.FindByNameAsync(newPassModel.LoginName);

                if (user != null)
                { 
                    PasswordHasher passHash = new PasswordHasher();
                    user.PasswordHash=passHash.HashPassword(newPassModel.Password);
                     IdentityResult result = await UserManager.UpdateAsync(user);
                     if (result.Succeeded)
                     {
                         if (User.Identity.Name == newPassModel.LoginName)
                         {
                             AuthenticationManager.SignOut();
                         }
                         ViewBag.ListEmployee = db.Employees.ToList();
                         return RedirectToAction("ListUser", "Account");
                     }
                     return View(newPassModel);
                }
                return View(newPassModel);
            }
            else
            {
                return View(newPassModel);
            }
        }

        public ActionResult RegisterUser()
        {
            ViewBag.ListEmployee = new SelectList(db.Employees, "PersonnelNumber", "NameSerName");
            ViewBag.ListRoles = new SelectList(RoleManager.Roles, "Name", "Name");
            return View();
        }
        [HttpPost]
        public async Task<ActionResult> RegisterUser(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                ApplicationUser user = new ApplicationUser {UserName = model.LoginName, PersonnelNumber = model.PersonnelNumber};
                IdentityResult result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    IdentityResult resultRole= await UserManager.AddToRolesAsync(user.Id, model.NameRoles);

                    if(resultRole.Succeeded)
                    return RedirectToAction("ListUser", "Account");
                }
                else
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }
            }
            ViewBag.ListEmployee = new SelectList(db.Employees, "PersonnelNumber", "NameSerName");
            ViewBag.ListRoles = new SelectList(RoleManager.Roles, "Name", "Name");
            return View(model);
        }
        public async Task<ActionResult> EditUser(string name)
        {
            ApplicationUser user = await UserManager.FindByNameAsync(name);
            if (user != null)
            {
                ViewBag.ListEmployee = new SelectList(db.Employees, "PersonnelNumber", "NameSerName");
                ViewBag.ListRoles = new SelectList(RoleManager.Roles, "Name", "Name");
                EditModel model = new EditModel {LoginName=user.UserName, PersonnelNumber=user.PersonnelNumber,NameRoles=UserManager.GetRoles(user.Id).ToArray(), NewLoginName=user.UserName};
                
                return View(model);
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        [HttpPost]

        public async Task<ActionResult> EditUser(EditModel model)
        {
            ViewBag.ListEmployee = new SelectList(db.Employees, "PersonnelNumber", "NameSerName");
            ViewBag.ListRoles = new SelectList(RoleManager.Roles, "Name", "Name");
            ApplicationUser user = await UserManager.FindByNameAsync(model.LoginName);
            if (ModelState.IsValid && user != null)
            {
                user.UserName = model.NewLoginName;
                user.PersonnelNumber = model.PersonnelNumber;
                if (User.Identity.Name == model.LoginName) //если данный пользователь залогинин, то делаем выход из аккаунта
                    AuthenticationManager.SignOut();
                IdentityResult result = await UserManager.UpdateAsync(user); //обновляем данные пользователя
                if (result.Succeeded)                                       //если успешно, то обновляем роли
                {
                    string[] listRolesUser = UserManager.GetRoles(user.Id).ToArray(); //получаем список ролей указанного пользователя
                    UserManager.RemoveFromRoles(user.Id, listRolesUser);
                    IdentityResult resultRole = await UserManager.AddToRolesAsync(user.Id, model.NewNameRoles);
                    if (resultRole.Succeeded)
                    {
                        return RedirectToAction("ListUser", "Account");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Ошибка сохранения");
                }
            }
            else
            {
                ModelState.AddModelError("", "Ошибка сохранения");
            }
            return View(model);
        }


        //удалние пользователя
        [HttpPost]
        [ActionName("DeleteUser")]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            ApplicationUser user = await UserManager.FindByIdAsync(id);
            if (user != null)
            {
                IdentityResult result = await UserManager.DeleteAsync(user);
                if (result.Succeeded)
                {
                    return Json(1, JsonRequestBehavior.DenyGet);
                }
            }
            return null;
        }

        //для входа в систему
        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.returnUrl = returnUrl;
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                ApplicationUser user = await UserManager.FindAsync(model.LoginName, model.Password);
                if (user == null)
                {
                    ModelState.AddModelError("", "Неверный логин или пароль.");
                }
                else
                {
                    string fullNameUser=string.Empty;
                    if (UserManager.IsInRole(user.Id, "admin"))
                        fullNameUser = "Администратор";
                    else
                        fullNameUser = db.Employees.Find(user.PersonnelNumber).NameSerName.ConvertToShortName();     
                    HttpCookie emplName = new HttpCookie("MyCookies");
                    emplName["shortNameEmpl"] = fullNameUser;
                    emplName.Expires = DateTime.Now.AddDays(1);
                    Response.Cookies.Add(emplName);
                    ClaimsIdentity claim = await UserManager.CreateIdentityAsync(user,
                                            DefaultAuthenticationTypes.ApplicationCookie);
                    AuthenticationManager.SignOut();
                    AuthenticationManager.SignIn(new AuthenticationProperties{IsPersistent = true}, claim);
                    if (String.IsNullOrEmpty(returnUrl))
                        return RedirectToAction("Index", "Home");
                    return Redirect(returnUrl);
                }
            }
            ViewBag.returnUrl = returnUrl;
            return View(model);
        }
        //для выхода из системы
        [AllowAnonymous]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Index", "Home");
        }



        //контроллеры ролей
        private ApplicationRoleManager RoleManager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<ApplicationRoleManager>();
            }
        }

        public ActionResult ListRoles()
        {
            return View(RoleManager.Roles.ToList());
        }

        public ActionResult CreateRole()
        {
            return View();
        }
        [HttpPost]
        public async Task<ActionResult> CreateRole(CreateRoleModel model)
        {
            if (ModelState.IsValid)
            {
                IdentityResult result = await RoleManager.CreateAsync(new ApplicationRole
                {
                    Name = model.Name,
                    Description = model.Description
                });
                if (result.Succeeded)
                {
                    return RedirectToAction("ListRoles");
                }
                else
                {
                    ModelState.AddModelError("", "Что-то пошло не так");
                }
            }
            return View(model);
        }

        public async Task<ActionResult> EditRole(string id)
        {
            ApplicationRole role = await RoleManager.FindByIdAsync(id);
            if (role != null)
            {
                return View(new EditRoleModel { Id = role.Id, Name = role.Name, Description = role.Description });
            }
            return RedirectToAction("ListRoles");
        }
        [HttpPost]
        public async Task<ActionResult> EditRole(EditRoleModel model)
        {
            if (ModelState.IsValid)
            {
                ApplicationRole role = await RoleManager.FindByIdAsync(model.Id);
                if (role != null)
                {
                    role.Description = model.Description;
                    role.Name = model.Name;
                    IdentityResult result = await RoleManager.UpdateAsync(role);
                    if (result.Succeeded)
                    {
                        return RedirectToAction("ListRoles");
                    }
                    else
                    {
                        ModelState.AddModelError("", "Что-то пошло не так");
                    }
                }
            }
            return View(model);
        }

        public async Task<ActionResult> DeleteRole(string id)
        {
            ApplicationRole role = await RoleManager.FindByIdAsync(id);
            if (role != null)
            {
                IdentityResult result = await RoleManager.DeleteAsync(role);
                return Json(1, JsonRequestBehavior.AllowGet);
            }
            return null;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
                UserManager.Dispose();
                RoleManager.Dispose();
            }
            base.Dispose(disposing);
        }

	}
}