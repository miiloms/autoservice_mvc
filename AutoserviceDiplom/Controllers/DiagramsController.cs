﻿using AutoserviceDiplom.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace AutoserviceDiplom.Controllers
{
     [Authorize]
    public class DiagramsController : Controller
    {
        autoserviceDEntities db = new autoserviceDEntities();
        Dictionary<int, string> listmonth;
        List<int> listyear;
        public DiagramsController ()
	    {  
            listmonth = new Dictionary<int, string> { { 0, "За весь год" },{ 1, "Январь" }, { 2, "Февраль" }, { 3, "Март" }, { 4, "Апрель" }, { 5, "Май" }, { 6, "Июнь" }, { 7, "Июль" }, { 8, "Август" }, { 9, "Сентябрь" }, { 10, "Октябрь" }, { 11, "Ноябрь" }, { 12, "Декабрь" } };
            listyear = new List<int>();
            for (int y = 2015; y <= DateTime.Now.Year; y++)
                listyear.Add(y);
	    }
        
        // GET: Diagrams
        public ActionResult ShowDiagramCountOfOrders()
        {
            ViewBag.selelctListyear = new SelectList(listyear, "", "", DateTime.Now.Year);
            ViewBag.selelctListmonth = new SelectList(listmonth, "Key", "Value", new Dictionary<int, string> { { 0, "За весь год" } });
            return View();
        }
        public JsonResult GetDiagram(int? year, int? month)
        {
            year = year ?? DateTime.Now.Year;
            month = month ?? DateTime.Now.Month;
            if (month == null || month == 0)
            {
                 var list_source = db.OrderServs.Where(f => f.DateMakingOrder.Value.Year == year).GroupBy(e => e.DateMakingOrder.Value.Month).
                    Select(n => new { y = n.Count(), x = n.FirstOrDefault().DateMakingOrder.Value.Month }).
                    OrderBy(o => o.x).ToList();
                 var list_source2 = list_source.Select(h => new { y = h.y, nameMonth = listmonth[h.x] }).ToArray();
                 return Json(list_source2);
            }
            else 
            {
                 var list_source = db.OrderServs.Where(f => f.DateMakingOrder.Value.Year == year&&f.DateMakingOrder.Value.Month==month).GroupBy(e => e.DateMakingOrder.Value.Day).
                    Select(n => new { y = n.Count(), x = n.FirstOrDefault().DateMakingOrder.Value.Day }).
                    OrderBy(o => o.x).ToList();
                 return Json(list_source);
            }
            
        }
        public ActionResult ShowDiagramTotalIncomeByServices()
        {
            ViewBag.selelctListyear = new SelectList(listyear, "", "", DateTime.Now.Year);
            ViewBag.selelctListmonth = new SelectList(listmonth, "Key", "Value", new Dictionary<int, string> { { 0, "За весь год" } });
            return View();
        }
        public JsonResult GetDiagramIncome(int? year, int? month)
        {
            year = year ?? DateTime.Now.Year;
            month = month ?? DateTime.Now.Month;
            if (month == null || month == 0)
            {
                //for services
                var list_source_1 = db.ExecutingServices.Where(f => f.OrderServ.DateMakingOrder.Value.Year == year && f.ID_service != "mngopen" && f.ID_service != "mngclose").
                   GroupBy(e => e.OrderServ.DateMakingOrder.Value.Month).
                   Select(n => new { y = n.Sum(s => (int)((double)s.Price * (s.TaxAddedValue * 0.01 + 1))), x = n.FirstOrDefault().OrderServ.DateMakingOrder.Value.Month }).
                   OrderBy(o => o.x).ToList();
                //for spare parts
                var list_source_3 = db.UsingPartMaterials.Where(f => f.ExecutingService.OrderServ.DateMakingOrder.Value.Year == year).
                  GroupBy(e => e.ExecutingService.OrderServ.DateMakingOrder.Value.Month).
                  Select(n => new { y = n.Sum(s => (int)s.Cost_part), x = n.FirstOrDefault().ExecutingService.OrderServ.DateMakingOrder.Value.Month }).
                  OrderBy(o => o.x).ToList();
                var list_source2 = list_source_1.Select(h => new { y = h.y, nameMonth = listmonth[h.x] }).ToArray();
                object[] arr = new object[] { list_source2, list_source_3 };
                return Json(arr);
            }
            else
            {
                //for services
                var list_source = db.ExecutingServices.Where(f => f.OrderServ.DateMakingOrder.Value.Year == year && f.OrderServ.DateMakingOrder.Value.Month == month && f.ID_service != "mngopen" && f.ID_service != "mngclose").
                  GroupBy(e => e.OrderServ.DateMakingOrder.Value.Day).
                  Select(n => new { y = n.Sum(s => (int)((double)s.Price * (s.TaxAddedValue * 0.01 + 1))), x = n.FirstOrDefault().OrderServ.DateMakingOrder.Value.Day}).
                  OrderBy(o => o.x).ToList();
                //for spare parts
                var list_source_2 = db.UsingPartMaterials.Where(f => f.ExecutingService.OrderServ.DateMakingOrder.Value.Year == year && f.ExecutingService.OrderServ.DateMakingOrder.Value.Month == month).
                 GroupBy(e => e.ExecutingService.OrderServ.DateMakingOrder.Value.Day).
                 Select(n => new { y = n.Sum(s => (int)s.Cost_part), x = n.FirstOrDefault().ExecutingService.OrderServ.DateMakingOrder.Value.Day}).
                 OrderBy(o => o.x).ToList();
                object[] arr = new object[] { list_source, list_source_2 };
                return Json(arr);
            }

        }
    }
}