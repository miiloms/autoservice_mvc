﻿$(document).ready(function () {
    //для модального окна
    /* Зaкрытие мoдaльнoгo oкнa, тут делaем тo же сaмoе нo в oбрaтнoм пoрядке */
    $('#modal_close, #overlay').click(function () { 
        $('#modal_form')
            .animate({ opacity: 0, top: '45%' }, 200,  
                function () { 
                    $(this).css('display', 'none'); 
                    $('#overlay2').fadeOut(400); 
                }
            );
    });
})

//удаление заказа
function DeleteRecord(idOrd) {
    $.ajax(
       {
           url: '/PreRecord/DeleteRecord',
           data: 'ID_record=' + idOrd,
           type: "GET",
           datatype: "json"
       })
}
//
function openModalWindow() {
    $('#overlay2').fadeIn(400,
        function () {
            $('#modal_form')
                .css('display', 'block')
                .animate({ opacity: 1, top: '50%' }, 200);
        });
}
//
function CastRecord(id_record) {
    $.ajax({
        url: '/PreRecord/CastToOrder',
        data: 'id_record=' + id_record,
        type: "GET",
        datatype: "json",
        success: function (result) {
            var df = result.indexOf("javascript");
            if (result.indexOf("javascript") == -1) {
                $("#contents2").html(result);
                openModalWindow();
            }

        }
    });
}