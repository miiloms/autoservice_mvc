﻿using AutoserviceDiplom.Models;
using AutoserviceDiplom.MyCustomClasses;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web.Mvc;

namespace AutoserviceDiplom.Controllers
{
    [Authorize]
    public class AcceptanceCustomController : Controller
    {
        private autoserviceDEntities db = new autoserviceDEntities();
        public ActionResult Manage()
        {
            AcceptanceCustomParamsViewModel paramAccept = new AcceptanceCustomParamsViewModel();
            var empls = db.Employees.Where(f => f.Post.ID_post == "mngparts" && f.ContractToEmployees.FirstOrDefault(d => d.DismissDate == null) != null).ToList();
            paramAccept.ListEmployee = new SelectList(empls, "PersonnelNumber", "NameSerName");
            paramAccept.ListClients = new SelectList(db.Clients.Take(5), "ID_client", "NameSerName");
            return View(paramAccept);
        }
        [HttpPost]
        public ActionResult SearchParts(string ID_part, string NamePart)
        {
            List<SparePartMaterial> sParts = db.SparePartMaterials.Where(s => s.ID_part.Contains(ID_part) && s.Name.Contains(NamePart)).Take(10).ToList();
            if (sParts.Count != 0)
                return PartialView("SearchPartResults", sParts);
            else
                Response.Write("Нет данных по запросу...");
            return null;
        }
        [HttpPost]
        public ActionResult AddPart(string Number, string State, int? ID_doc, string ID_part)
        {
            if (!ID_doc.HasValue)
                return null;
            double _Number;
            if (double.TryParse(Number, out _Number))
            {
                    int status = db.AddCustomPartInvoice(_Number, State, ID_part, ID_doc);
                    if (status == -1)
                        return Json(ID_part);
                    var viewModel = this.GetAcceptedParts(ID_doc.Value);
                
                return PartialView("UpdateTable", viewModel);

            }
            else
            {
                return null;
            }
        }

        [HttpPost]
        public ActionResult AddInvoice(AcceptDocument docmnt)
        {
            if (ModelState.IsValid)
            {
                var foundDoc = db.AcceptDocuments.Find(docmnt.ID_acceptance);
                if (foundDoc == null)
                {
                    if (docmnt.AcceptDate == null)
                    {
                        docmnt.AcceptDate = DateTime.Now;
                    }
                    ObjectParameter id_accept= new ObjectParameter("id_accept", typeof(int));
                    db.AddCustomInvoice(docmnt.ID_client, docmnt.PersonnelNumber, docmnt.AcceptDate,id_accept);
                    docmnt.ID_acceptance = (id_accept.Value as int?).Value;
                    if((id_accept.Value as int?).Value==-1)
                        return Json("Неверно введены параметры");
                    var infoInvoice = db.GetInfoCustomInvoiceByIdDoc(docmnt.ID_acceptance).FirstOrDefault(); ;
                    return PartialView("EmptyTableInvoice", infoInvoice);
                }
                else
                {
                    if (foundDoc.AcceptanceCustomSParts.Count > 0)
                    {
                        var viewModel = this.GetAcceptedParts(docmnt.ID_acceptance);
                        return PartialView("UpdateTable", viewModel);
                    }
                    else
                    {
                        var infoInvoice = db.GetInfoCustomInvoiceByIdDoc(docmnt.ID_acceptance).FirstOrDefault();
                        return PartialView("EmptyTableInvoice", infoInvoice);
                    }

                }
            }
            else return Json("Неверно введены параметры");
        }

        [HttpPost]
        public ActionResult SearchInvoice(int? ID_acceptance)
        {
            if (ID_acceptance.HasValue)
            {
                var viewModel = this.GetAcceptedParts(ID_acceptance.Value);
                if (viewModel.InfoInvoice == null)
                    return JavaScript("$('#popErorr').html('Акт сдачи-приемки под номером № " + ID_acceptance + " отсутсвует в БД').slideDown(1000).delay(3000).slideUp(1000); ");
                if (viewModel.AcceptencedParts.Count > 0)
                    return PartialView("UpdateTable", viewModel);
                else
                {
                    return PartialView("EmptyTableInvoice", viewModel.InfoInvoice);
                }
            }
            else
                return null;
        }

        public ActionResult Delete(string id_prt, int? id_accept)
        {
            if (id_prt != "" && id_accept.HasValue)
            {
                var delPart=db.AcceptanceCustomSParts.Find(id_prt, id_accept);
                if (delPart != null && delPart.UsingCustomSPartMats.Count == 0)
                {
                    db.Entry(delPart).State = EntityState.Deleted;
                    db.SaveChanges();
                }
                else
                { 
                     return JavaScript("$('#popErorr').html('Данный материал (запчасть) не может быть исключена из документа, так как уже задействована в услугах (работах)').slideDown(1000).delay(3000).slideUp(1000); ");
                }
                var viewModel = this.GetAcceptedParts(id_accept.Value);
                if (viewModel.AcceptencedParts.Count > 0)
                    return PartialView("UpdateTable", viewModel);
                else
                    return PartialView("EmptyTableInvoice", viewModel.InfoInvoice);
            }
            else return null;
        }

        public ActionResult DeleteInvoice(int? id_accept)
        {
            if (id_accept.HasValue)
            {
                var inputParams = new Dictionary<string, object> { { "id_accept", id_accept } };
                ConnectionLayerDB dbConnection = new ConnectionLayerDB("autoserviceDB");
                int stat = dbConnection.ExecutionProcedure("DeleteCustomInvoice", inputParams);
                switch (stat)
                {
                    case -1: return null;
                    case -2: return JavaScript("$('#popErorr').html('" + dbConnection.InfoMessage + "').slideDown(1000).delay(3000).slideUp(1000); ");
                    default: return JavaScript("$('#updTable').html(''); $('#popSuccess').html('Акт сдачи-приемки №" + id_accept + " удален из БД!').slideDown(1000).delay(3000).slideUp(1000); ");
                }
            }
            return null;
        }

        public ActionResult EditInvoice(int? id_accept)
        {
            if (id_accept.HasValue)
            {
                var infoInvoice = db.GetInfoCustomInvoiceByIdDoc(id_accept).FirstOrDefault();
                if (infoInvoice != null)
                {
                    Session["InfoEditInvoice"] = infoInvoice;
                    ViewBag.ListEmployee = new SelectList(db.Employees, "PersonnelNumber", "NameSerName");
                    ViewBag.ListClients = new SelectList(db.Clients.Take(5), "ID_client", "NameSerName");
                    return PartialView("EditInvoice", infoInvoice);
                }
            }
            return null;
        }

        [HttpPost]
        public ActionResult EditSaveInvoice(AcceptDocument AcceptDoc)
        {
            if (ModelState.IsValid)
            {
                db.Entry(AcceptDoc).State = EntityState.Modified;
                db.SaveChanges();
                var viewModel = this.GetAcceptedParts (AcceptDoc.ID_acceptance);
                return PartialView("UpdateTable", viewModel);
            }
            return null;
        }


        [HttpGet]
        public ActionResult CancelEditInvoice(int? id_accept)
        {
            if (id_accept.HasValue)
            {
                var infoInvoice = db.GetInfoCustomInvoiceByIdDoc(id_accept).FirstOrDefault();
                if (infoInvoice != null)
                {
                    infoInvoice = Session["InfoEditInvoice"] as GetInfoCustomInvoiceByIdDoc_Result;
                    return PartialView("RefreshInfoInvoice", infoInvoice);
                }
            }
            return null;
        }

        [HttpGet]
        public ActionResult ClearInvoice(int? id_accept)
        {
            if (id_accept.HasValue)
            {
                if (id_accept.HasValue)
                {
                    ConnectionLayerDB dbConnection = new ConnectionLayerDB("autoserviceDB");
                    var inputParams = new Dictionary<string, object> { { "id_accept", id_accept } };
                    int status = dbConnection.ExecutionProcedure("ClearCustomInvoice", inputParams);
                    if (status == 0)
                    {
                        var viewModel = this.GetAcceptedParts(id_accept.Value);
                        return PartialView("EmptyTableInvoice", viewModel.InfoInvoice);
                    }
                    else
                        return JavaScript("$('#popErorr').html('" + dbConnection.InfoMessage + "').slideDown(1000).delay(3000).slideUp(1000); ");
                }
            }
            return null;
        }

        public ActionResult SearchClients(string id_client, string name_client)
        {
            IEnumerable<Client> clients = db.Clients.Where(s => s.ID_client.Contains(id_client) && s.NameSerName.Contains(name_client)).Take(5);
            return PartialView("ResultSearchClient", clients.ToList());
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [ChildActionOnly]
        public AcceptedCustomPartsViewModel GetAcceptedParts(int id_doc)
        {
            List<GetAcceptedCustomParts_Result> accpParts = db.GetAcceptedCustomParts(id_doc) .ToList();
            var infoInvoice = db.GetInfoCustomInvoiceByIdDoc(id_doc).FirstOrDefault();
            AcceptedCustomPartsViewModel viewModel = new AcceptedCustomPartsViewModel { AcceptencedParts = accpParts, InfoInvoice = infoInvoice };
            return viewModel;
        }
	}
}