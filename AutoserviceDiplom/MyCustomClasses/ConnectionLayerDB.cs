﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data.Common;
using System.Configuration;
using System.Data;
namespace AutoserviceDiplom.MyCustomClasses
{
    public  class ConnectionLayerDB  //класс для работы с БД (соединительный уровень) для процедур
    {
        private SqlConnection sqlConnect= null;
        private string nameConnectionString= string.Empty;
        public  string InfoMessage { set; get; }

        public ConnectionLayerDB(string _nameConnectionString)
        {
            nameConnectionString = _nameConnectionString;
        }

        //первая версия метода - только имя процедуры и входящие параметры
        public int ExecutionProcedure(string nameProcedure, Dictionary<string, object> _params_in)
        {
            this.OpenConnection();
            List<SqlParameter> _SqlParams = new List<SqlParameter>();
            using (SqlCommand cmd = new SqlCommand(nameProcedure, sqlConnect))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                foreach (var p in _params_in)
                {
                    SqlParameter inParam = new SqlParameter("@" + p.Key, p.Value);
                    inParam.Direction = ParameterDirection.Input;
                    _SqlParams.Add(inParam);
                }
                SqlParameter returnValueParam = new SqlParameter("@ReturnValue", DbType.Int32);
                returnValueParam.Direction = ParameterDirection.ReturnValue;
                _SqlParams.Add(returnValueParam);

                cmd.Parameters.AddRange(_SqlParams.ToArray());
                cmd.ExecuteNonQuery();
                sqlConnect.Close();
                return (int)cmd.Parameters["@ReturnValue"].Value;
            }
        }

        //вторая версия метода -  имя процедуры, in параметры и out параметры 
        public int ExecutionProcedure(string nameProcedure, Dictionary<string, object> _params_in,  Dictionary<string, object> _params_out)
        {
            this.OpenConnection();
            List<SqlParameter> _SqlParams = new List<SqlParameter>();
            using (SqlCommand cmd = new SqlCommand(nameProcedure, sqlConnect))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                foreach (var p in _params_in)
                {            
                    SqlParameter inParam = new SqlParameter();
                    inParam.Direction = ParameterDirection.Input;
                    inParam.ParameterName = "@" + p.Key;
                    inParam.Value = p.Value;
                    _SqlParams.Add(inParam);
                }

                //return value
                SqlParameter returnValueParam = new SqlParameter("@ReturnValue", DbType.Int32);
                returnValueParam.Direction = ParameterDirection.ReturnValue;
                _SqlParams.Add(returnValueParam);

                //out параметры
                foreach (var p in _params_out)
                {
                    
                    SqlParameter outParam = new SqlParameter();
                    outParam.ParameterName = "@" + p.Key;
                    outParam.Direction = ParameterDirection.Output;
                    outParam.DbType = (DbType)p.Value;
                    outParam.Size = 8;
                    _SqlParams.Add(outParam);
                }

                cmd.Parameters.AddRange(_SqlParams.ToArray());
                cmd.ExecuteNonQuery();
                sqlConnect.Close();
                //возвращаем out переменные
                foreach (var n in _params_out.Keys.ToList())
                {
                    _params_out[n] = cmd.Parameters["@" + n].Value;
                }  
                return (int)cmd.Parameters["@ReturnValue"].Value;
            }
        }
        private void OpenConnection()
        {
            string cnString = ConfigurationManager.ConnectionStrings[nameConnectionString].ConnectionString;
            sqlConnect = new SqlConnection(cnString);
            sqlConnect.InfoMessage += new System.Data.SqlClient.SqlInfoMessageEventHandler(GetInfoMessage);
            sqlConnect.Open();
        }
        void  GetInfoMessage(object sender, System.Data.SqlClient.SqlInfoMessageEventArgs e)
        {
            this.InfoMessage= e.Message;
        }
    }
}