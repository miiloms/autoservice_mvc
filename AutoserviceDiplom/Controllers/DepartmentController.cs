﻿using AutoserviceDiplom.Models;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace AutoserviceDiplom.Controllers
{
    [Authorize]
    public class DepartmentController : Controller
    {
        private autoserviceDEntities db = new autoserviceDEntities();

        public ActionResult List()
        {
            ViewBag.ListEmployees = db.Employees.ToDictionary(m=>m.PersonnelNumber, n=>n.NameSerName);
            return View(db.Departments);
        }


        public ActionResult Create()
        {
            var list = db.Employees.Where(f=>f.ContractToEmployees.FirstOrDefault(g=>g.DismissDate==null)!=null).ToList();
            list.Add(new Employee { PersonnelNumber = 0, NameSerName = "Выбрать..." });
            ViewBag.ListEmployees = new SelectList(list, "PersonnelNumber", "NameSerName", 0);
            return View(new Department());
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="NameDepart,PersNumberChief,ID_department")] Department department)
        {
            if (ModelState.IsValid)
            {
                    db.Entry(department).State = EntityState.Added;
                    db.SaveChanges();
                    return RedirectToAction("List");
            }

            return View(department);
        }

        // GET: /Department/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Department department = db.Departments.Find(id);
            if (department == null)
            {
                return HttpNotFound();
            }
            var list = db.Employees.Where(f => f.ContractToEmployees.FirstOrDefault(g => g.DismissDate == null) != null).ToList();
            list.Add(new Employee { PersonnelNumber = 0, NameSerName = "Выбрать..." });
            ViewBag.ListEmployees = new SelectList(list, "PersonnelNumber", "NameSerName");
            return View(department);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="NameDepart,PersNumberChief,ID_department")] Department department)
        {
            if (ModelState.IsValid)
            {
                db.Entry(department).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("List");
            }
            return View(department);
        }

        [HttpGet, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int? id)
        {
            Department department = db.Departments.Find(id);
            if (id != null&&department.Employees.Count == 0)
            {
                    db.Departments.Remove(department);
                    db.SaveChanges();
                    return RedirectToAction("List");
            }
            else return RedirectToAction("List");
            
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
