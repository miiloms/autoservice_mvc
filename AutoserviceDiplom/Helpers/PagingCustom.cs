﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoserviceDiplom.Helpers
{
    public class PagingInfoProcedure
    {
        public int PageNumber { get; set; } // номер текущей страницы
        public int PageSize { get; set; } // кол-во объектов на странице
        public int TotalItems { get; set; } // всего объектов
        public int PageCount  // всего страниц
        {
            get { return (int)Math.Ceiling((decimal)TotalItems / PageSize); }
        }
    }

    
    public class PageListCustom<T>:IEnumerable<T>
    {
        public IEnumerable<T> _Elements { get; set; }
        public PagingInfoProcedure PageInfo { get; set; }
        public IEnumerator<T> GetEnumerator()
        {
             return this._Elements.GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return this._Elements.GetEnumerator();
        }
    }
    //для процедуры (процедура, которая уже возврщает готовую страницу)//
    public static class ExternPagingMethod
    {
        /// <summary>
        /// Convert List with paging parametrs to PageListCustom
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="_listObject"></param>
        /// <param name="_pageNumber"></param>
        /// <param name="_pageSize"></param>
        /// <param name="_countRows"></param>
        /// <returns></returns>
        public static PageListCustom<T> ToCustomPageList<T>(this IEnumerable<T> _listObject, int _pageNumber, int _pageSize, int? _countRows)
        {
            PageListCustom<T> page = new PageListCustom<T>();
            page._Elements = _listObject;
            PagingInfoProcedure pageInfo = new PagingInfoProcedure {PageNumber=_pageNumber, PageSize=_pageSize, TotalItems=_countRows!=null?_countRows.Value:1 };
            page.PageInfo = pageInfo;
            return page;
        }
    }
}