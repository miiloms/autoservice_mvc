﻿$(document).ready(function () {
    //for horizontal dropdown menu
    $('ul.dropdown-menu [data-toggle="dropdown"]').on('click', function (event) {
        event.preventDefault();
        event.stopPropagation();
        $(this).parent().addClass('open');
        var menu = $(this).parent().find("ul");
        var menupos = menu.offset();
        if ((menupos.left + menu.width()) + 30 > $(window).width()) {
            var newpos = -menu.width();
        } else {
            var newpos = $(this).parent().width();
        }
        menu.css({ left: newpos });
    });
    //for notisfication close (removing element from DOM)
    $('#notification-wrap').on('click', '.glyphicon-remove', function () {
        $(this).parent().fadeOut(300);
        setTimeout(function () {
            $(this).parent().remove();
        }, 300)
        //если не осталось элементов, то скрываем обертку
        if ($('#notification-wrap .notification-body').length == 1)
            $('#notification-wrap').hide();
    })

    //for notifications
    var notificationhub = $.connection.notificationHub;

    notificationhub.client.displayMessage = function (message) {
        $('#notification-wrap').show();
        var notif_item = $('#source-item-notific').clone();//клонируем info элемент
        notif_item.find('#notif-content').html(message);
        notif_item.removeAttr('id');
        $('#notification-wrap').append(notif_item);
        notif_item.fadeIn(300);
    };
    $.connection.hub.start();
})