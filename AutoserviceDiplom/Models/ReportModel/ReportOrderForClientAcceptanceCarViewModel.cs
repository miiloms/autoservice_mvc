﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoserviceDiplom.Models.ReportModel
{
    public class ReportOrderForClientAcceptanceCarViewModel
    {
       public Car Car { get; set; }
       public Client Client { get; set; }
       public List<Get_Report_AcceptedCustomParts_Result> PartCustomList { set; get; }
       public string NameManager { set; get; }
       public OrderServ OrderServ { get; set; }
       public List<RemarkToStateCar> Remarks { get; set; }
       public  int N {get; set; }
    }
}