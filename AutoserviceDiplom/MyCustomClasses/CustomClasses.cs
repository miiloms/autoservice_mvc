﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace AutoserviceDiplom.MyCustomClasses
{
    public static class ExternStringMethod
    {
        //преобразование ФИО в Фамилия И.О.
        public static string ConvertToShortName(this string fullName)
        {
            if (fullName != null)
            {
                string[] arrName = fullName.Split(new char[] { ' ' });
                string shortName = string.Empty;
                for (int i = 0; i < arrName.Length; i++)
                {
                    if (arrName[i] != string.Empty && arrName[i] != " ")
                        if (shortName == string.Empty)
                            shortName = arrName[i];
                        else
                            shortName += " " + arrName[i].ElementAt(0) + ".";
                }
                return shortName;
            }
            else return null;
        }

        //преобразование ФИО в Фамилия ИМЯ  Отчество как массив слов
        public static string[] ConvertToStringNames(this string fullName)
        {
            if (fullName != null)
            {
                string[] arrName = fullName.Split(new char[] { ' ' });
                string[] arrayFullName= new string[3]{"","",""};
                for (int i = 0; i < arrName.Length; i++)
                {
                    if (arrName[i] != string.Empty && arrName[i] != " "&&i<3)
                        arrayFullName[i] = arrName[i].Trim();
                }
                return arrayFullName;
            }
            else return null;
        }
    }
}