﻿$().ready(function () {
    $('#btn-ajax').click();
})
var data = {
    labels: [],
    datasets: [
        {
            label: "Услуги, BYN",
            backgroundColor: 'rgba(211,86,77, 0.8)',
            borderColor: 'rgba(130,47,38,1)',
            borderWidth: 1,
            data: [],
        },
         {
             label: "Материалы, BYN",
             backgroundColor: 'rgba(75,174,240, 0.8)',
             borderColor: 'rgba(61,142,196,1)',
             borderWidth: 1,
             data: [],
         }
    ]
};
var ctx = document.getElementById("myChart");
var myLineChart = new Chart(ctx, {
    type: 'bar',
    data: data,
    options: {
        responsive: true
    }
});
function UpdateDiagram(result) {
    var listMonth = new Array();
    var listDataPoints1 = new Array();
    var listDataPoints2 = new Array();
    for (key in result[0]) {
        listMonth.push(result[0][key].nameMonth == null ? result[0][key].x + ' ' + $('#month option:selected').text() : result[0][key].nameMonth);
        listDataPoints1.push(result[0][key].y);
    }
    for (key in result[1]) {
        listDataPoints2.push(result[1][key].y);
    }
    data.datasets[0].data = listDataPoints1;
    data.datasets[1].data = listDataPoints2;
    data.labels = listMonth;
    myLineChart.update();
}