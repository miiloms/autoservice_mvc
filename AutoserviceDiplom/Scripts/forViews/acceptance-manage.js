﻿//функция для  отправки запроса на добавление  инвойс
function AddInvoice() {
    if ($('#row_find_part').css('display') == 'none') {
        $('i[onclick="extend()"]').click();
    }
    $.ajax(
    {
        url: '/Acceptance/AddInvoice',
        data: $('#Params').serialize(),
        type: "Post",
        datatype: "json",
        success: function (result) {
            $('#updTable').html(result);
        }
    });
}
//для расширения таблицы инвойса
function extend() {
    if ($('#resultSearch').css('display') != 'none') {
        $('#resultSearch, #updTable, #row_find_part, #row_find_invoice').fadeOut(200);
        setTimeout(function () {
            $('#updTable table th:nth-child(3), #updTable table td:nth-child(3)').show();
            $('#updTable, #row_find_invoice').attr('class', 'col-md-12');
            $('#updTable, #row_find_invoice').fadeIn(200);
            $('.hide-group').show(); $('#search-inv').removeClass('col-md-10').addClass('col-md-4');
        }, 200)
        $('i[onclick="extend()"]').attr('class', 'glyphicon glyphicon-circle-arrow-right');
        $('#extendIcon').removeClass('glyphicon-chevron-left').addClass('glyphicon-chevron-right');

    }
    else {
        $(' #updTable, #row_find_invoice').fadeOut(200);
        setTimeout(function () {
            $('#updTable table th:nth-child(3), #updTable table td:nth-child(3)').hide();
            $('#updTable, #row_find_invoice').attr('class', 'col-md-6');
            $('#resultSearch, #updTable, #row_find_part, #row_find_invoice').fadeIn(200);
            $('.hide-group').hide(); $('#search-inv').removeClass('col-md-4').addClass('col-md-10');
        }, 200)
        $('i[onclick="extend()"]').attr('class', 'glyphicon glyphicon-circle-arrow-left');
        $('#extendIcon').removeClass('glyphicon-chevron-right').addClass('glyphicon-chevron-left');
    }

}

//функция для  отправки запроса на добавление запчасти в инвойс
function AddPart(btn) {
    $.ajax(
    {
        url: '/Acceptance/AddPart',
        data: $(btn).parent().parent().find('input').add('#idLotNumb').serialize(),
        type: "Post",
        datatype: "json",
        success: function (result) {
            if (result.indexOf('table') == -1) {
                $('#popErorr').html('Данная запчасть ID (' + result + ') уже есть в документе').slideDown(1000).delay(3000).slideUp(1000); return 0;
            }
            if (result != '') {
                $('#updTable').html(result);
                $('#popErorr').fadeOut(1000);
            }
        }
    });
}

function showbar() {
    if ($('#createInvoice').css('display') == 'none') {
        $('#createInvoice').fadeIn(300);
        $('button[onclick="showbar()"]').css('borderRadius', '10px').html('Добавление нового акта сдачи-приемки материалов');
    }
    else {
        $("#findProviderControl").fadeOut(300);
        $('#createInvoice').fadeOut(300);
        $('button[onclick="showbar()"]').css('borderRadius', '0px').html('Внести новый акт сдачи-приемки материалов');
    }

}
//показать контрол с поиском поставщиков
function addProvider(elem) {
    var coord = $(elem).offset();
    if ($('#findProviderControl').css('display') == "none")
        $('#findProviderControl').css('top', coord.top + 35).css('left', coord.left).slideDown(300);
    else
        $('#findProviderControl').slideUp(300);
}
//добавление новго провайдерав комбобокс
function addToSet(row) {
    var name = $(row).children().eq(0).text();
    var id = $(row).children().eq(2).text();
    $("#ID_provider").prepend($('<option selected="selected" value=' + id + '>' + name + '</option>'));
    $("#findProviderControl").slideUp(500);
}
$(function () {
    $('.date').datetimepicker({ language: 'ru', pickTime: false, format: 'YYYY-MM-DD' });
    //При изменении даты в dtPick_From, она устанавливается как минимальная для dtPick_Before
    $("#dtPick_From").on("dp.change", function (e) {
        $("#dtPick_Before").data("DateTimePicker").setMinDate(e.date);
    });
    //При изменении даты в dtPick_Before, она устанавливается как максимальная для dtPick_From
    $("#dtPick_Before").on("dp.change", function (e) {
        $("#dtPick_From").data("DateTimePicker").setMaxDate(e.date);
    });
});