﻿$(document).ready(function () {
    //для календаря
    $('.date').datetimepicker({ language: 'ru', pickTime: false, format: 'YYYY-MM-DD'});
    //выделяем по цветам график загрузки мастеров
    $('.tbl-load  td:not(:first-child)').each(function () {
        if ($(this).children(':last-child').val() == 0)
            $(this).addClass('vacancy');
        else {
            if ($(this).hasClass('busy-pre-reserv')) {
                $(this).append('<span>' + $(this).children(':last-child').val() + ':00' + '</span>'); return 0;
            }
            $(this).addClass('busy');
            $(this).append($(this).children(':last-child').val() + ':00');
        }

    })
    //вешаем на календарь  на его изменение
    $('.datepicker tbody, .backward-day, .forward-day').click(function () {
        var culpritObject = $(this);
        setTimeout(function () {
            var currentDay = new Date($('#CurrentDate').val());

            if (culpritObject.hasClass('backward-day'))
                currentDay.setDate(currentDay.getDate() - 1)
            else
                if (culpritObject.hasClass('forward-day'))
                    currentDay.setDate(currentDay.getDate() + 1)
            var stringDate = currentDay.getFullYear() + '-' + (currentDay.getMonth() + 1) + '-' + (currentDay.getDate() >= 10 ? currentDay.getDate() : '0' + currentDay.getDate());

            window.location = '/WorkLoading/TableLoad_Common/?CurrentDate=' + stringDate;
        }, 1)

    })
    /////для закрытия инфо-окна
    $('.close-sign').click(function () {
        $('.my-comment').fadeOut(400);
    })
    //вешаем на ячейки на таблице загрузок для подгрузки инфы
    $('.busy, .busy-pre-reserv').click(function () {
        if ($(this).hasClass('busy') || $(this).hasClass('busy-pre-reserv'))
            var my_data = $(this).children('input[name="vb"]').add('input[name="CurrentDate"]').add($(this).parent().children().eq(0).find('input'));
        var elem = $(this);
        $.ajax(
        {
            url: '/WorkLoading/GetDetailsForTime',
            data: my_data.serialize(),
            type: "POST",
            datatype: "json",
            success: function (content) {
                $('#contents_info').html(content);
                var coord = elem.offset();
                var w = elem.width();
                var h = elem.height();
                var h_box = $('.my-comment').height();
                $('.my-comment').hide();
                $('.my-comment').css('top', coord.top + h * 0.5 - 0.5 * h_box + 5).css('left', coord.left + w + 12);
                $('.my-comment').fadeIn(400);
            }
        });
    })
})