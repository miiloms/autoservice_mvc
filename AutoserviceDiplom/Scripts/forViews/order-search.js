﻿$(document).ready(function () {
    //Инициализация dtPick_From и dtPick_Before
    $(".date").datetimepicker({ language: 'ru', pickTime: false, format: 'YYYY-MM-DD' });
    //При изменении даты в dtPick_From, она устанавливается как минимальная для dtPick_Before
    $("#dtPick_From").on("dp.change", function (e) {
        $("#dtPick_Before").data("DateTimePicker").setMinDate(e.date);
    });
    //При изменении даты в dtPick_Before, она устанавливается как максимальная для dtPick_From
    $("#dtPick_Before").on("dp.change", function (e) {
        $("#dtPick_From").data("DateTimePicker").setMaxDate(e.date);
    });

    //для автозаполнения строки поиска номер заказа
    $("[data-autocomplete-source]").each(function () {
        var target = $(this);
        target.autocomplete({ minLength: 1, autoFocus: true, source: target.attr("data-autocomplete-source") });
    });
    //для чекбоксов
    $('#IsRejection').change(function () {
        if (!$('#IsClosed').prop('checked') && this.checked)
            $('#IsClosed').prop('checked', true);
        else
            $('#IsClosed').prop('checked', false);
    })
    //для пейджинга-сброс номера отсылаемой страницы при изменении строки поиска
    $('#search, #search2').change(function () {
        $('input[name=page]').removeAttr('value');
    })
})