﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoserviceDiplom.Models.ReportModel
{
    public class ReportOrderForClientModel
    {
        public List<GetTotalInfoServicesByOrder_Result> ServiceList { set; get; }
        public List<GetAllInfoAttachedPartById_Result> PartList { set; get; }
        public List<UsingCustomSPartMat> PartCustomList { set; get; }
        public OrderServ Order { set; get; }
    }
}