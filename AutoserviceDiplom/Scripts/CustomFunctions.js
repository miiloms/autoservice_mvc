﻿//*******для модального confirm окна*******//
function confirmDialog(text, func, params) {
    $(".DialogComfirms").fadeIn(200);
    $('#overlay').css('display', 'block').click(function () { shake($(".DialogComfirms")) });
    $('#contents').html(text);
    $('#ok, #cancel').click(function () {
        if ($(this).attr('id') == "ok")
            func(params);
        $('.DialogComfirms').fadeOut(200);
        $('#overlay').css('display', 'none');
        $('#ok, #cancel').off('click');
        $('#overlay').off();
    })
}
//шейкер окна
function shake(obj) {
    var coord = obj.offset();
    for (var step = 30; step > 0; step -= 5) {
        obj.animate({ 'left': '+=' + step }, 100);
        obj.animate({ 'left': '-=' + step }, 100);
    }
    setTimeout(function () {
        obj.css('left', '50%')
    }, 1500);
}
//*****************************************//