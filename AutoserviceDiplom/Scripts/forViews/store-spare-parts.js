﻿$(document).ready(function () {
    //Инициализация dtPick_From и dtPick_Before
    $(".date").datetimepicker({ language: 'ru', pickTime: false, format: 'YYYY-MM-DD' });
    //При изменении даты в dtPick_From, она устанавливается как минимальная для dtPick_Before
    $("#dtPick_From").on("dp.change", function (e) {
        $("#dtPick_Before").data("DateTimePicker").setMinDate(e.date);
    });
    //При изменении даты в dtPick_Before, она устанавливается как максимальная для dtPick_From
    $("#dtPick_Before").on("dp.change", function (e) {
        $("#dtPick_From").data("DateTimePicker").setMaxDate(e.date);
    });

    //для сортировки по столбцам
    $('#results').on('click', 'table th', function () {
        var index = $(this).index() + 1;
        var stringClassSpan;
        var isAccend;
        if ($(this).find('input').val() == 'True') {
            stringClassSpan = 'glyphicon glyphicon-sort-by-attributes-alt';
            isAccend = 'False';
            $('#results table th').eq(index - 1).find('input').attr('value', isAccend);
        }
        else {
            stringClassSpan = 'glyphicon glyphicon-sort-by-attributes';
            isAccend = 'True';
            $('#results table th').eq(index - 1).find('input').attr('value', isAccend);
        }
        $.ajax(
            {
                url: '/StoreSpareParts/SortByParam',
                data: { 'is_accend': $(this).find('input').val(), 'nmb_clmn': index },
                type: "POST",
                datatype: "json",
                success: function (result) {
                    $('#results').html(result);
                    $('#results table th').eq(index - 1).addClass('is-sorted');
                    $('#results table th').eq(index - 1).find('input').attr('value', isAccend);
                    $('#results table th').eq(index - 1).children('span').addClass(stringClassSpan);
                }
            });
    })


    //для выделения строк
    $('#results').on('click', 'table tr:not(:first-child)', function () {
        if ($(this).hasClass('selected-row-part')) {
            $(this).removeClass('selected-row-part');
            var isExists = false;
            $(this).parent().children().each(function () {
                if ($(this).hasClass('selected-row-part'))
                { isExists = true; }
            })
            if (!isExists)
                $('#trade_incr').hide();
        }
        else {
            $(this).addClass('selected-row-part');
            $('#trade_incr').show();
        }

    })
})
// для смены страницы
function nextUp(nPage) {
    var sortClmn = $('.is-sorted');
    var index = $(sortClmn).index();
    var isAccend = $(sortClmn).find('input').attr('value');
    var stringClassSpan = 'glyphicon glyphicon-sort-by-attributes' + (isAccend == 'False' ? '-alt' : '');
    $.ajax(
        {
            url: '/StoreSpareParts/SortByParam',
            data: { 'PageNumber': nPage },
            type: "POST",
            datatype: "json",
            success: function (result) {
                $('#results').html(result);
                if (sortClmn.length == 1) {
                    $('#results table th').eq(index).addClass('is-sorted');
                    $('#results table th').eq(index).find('input').attr('value', isAccend);
                    $('#results table th').eq(index).children('span').addClass(stringClassSpan);
                }
            }
        });
}
// for apply new Trade Increase
function applyIncrease() {
    $.ajax(
       {
           url: '/StoreSpareParts/ChangeTradeIncrease',
           data: $('#results .selected-row-part input').add('#trd_incr').serialize(),
           type: "POST",
           datatype: "json",
           success: function (result) {
               if (result == 1)
                   $('#results .selected-row-part input').each(function () {
                       var trd_incr = $('#trd_incr').val();
                       var price;
                       var cost = $(this).parent().children().eq(7).html();
                       price = Number(cost.replace(",", ".")) * (Number(trd_incr.replace(",", ".")) / 100 + 1);

                       $(this).parent().children().eq(8).html(trd_incr);
                       $(this).parent().children().eq(9).html(price.toFixed(2).replace(".", ","));
                   })
               else
                   $('#popErorr').html("Ошибка выполнения запроса...").slideDown(200).delay(1000).slideUp(200);
           }
       });
}