﻿using AutoserviceDiplom.Helpers;
using AutoserviceDiplom.Models;
using AutoserviceDiplom.MyCustomClasses;
using PagedList;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Net;
using System.Web.Mvc;


namespace AutoserviceDiplom.Controllers
{
    [Authorize]
    public class OrderController : Controller
    {
        autoserviceDEntities db = new autoserviceDEntities();
        [HttpGet]
        public ActionResult AddOrder(string ID, long? id_record)
        {
            if (ID != null)
            {
                Client client = db.Clients.Where(c => c.ID_client == ID).FirstOrDefault();
                OrderViewModel order = new OrderViewModel();
                order.ID_client = ID;
                order.ID_record = id_record as long?;
                order.NameSerName = client.NameSerName;
                var empls = db.Employees.Where(f => f.Post.ID_post == "mng01" && f.ContractToEmployees.FirstOrDefault(d => d.DismissDate == null) != null).ToList();
                order.ListEmplyees = new SelectList(empls, "PersonnelNumber", "NameSerName");
                ViewBag.CarVINs = new SelectList(db.GetIncludedCarByIdclient(ID), "VIN_number", "Model_Numb");
                ViewBag.ShortName = client.NameSerName.ConvertToShortName();
                return View(order);
            }
            else return RedirectToAction("Search");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddOrder(OrderViewModel order)
        {
            if (ModelState.IsValid)
            {
                ObjectParameter id_order = new ObjectParameter("id_order", typeof(string));
                if (order.ID_record == null)
                {
                    db.AddPartOrder(order.DateMakingOrder, order.ID_client, order.VIN_number, order.Descriptions, order.CurrentMileageCar, order.PersonnelNumber, (byte)(order.NumberWheelCaps != null ? order.NumberWheelCaps .Count(): 0), (byte)(order.NumberWipers!=null?order.NumberWipers.Count():0),
                        (byte)(order.NumberWipersArms!=null?order.NumberWipersArms.Count():0), order.IsAntenna, order.IsSpareWheel, order.IsCoverDecorEngine, order.IsTuner, order.FluelLevelPercent, id_order);
                }
                else
                {
                    int e = db.AddPartialOrderForRecord(order.DateMakingOrder, order.ID_client, order.VIN_number, order.Descriptions, order.CurrentMileageCar, order.PersonnelNumber, order.ID_record, (byte)(order.NumberWheelCaps != null ? order.NumberWheelCaps.Count() : 0), (byte)(order.NumberWipers != null ? order.NumberWipers.Count() : 0),
                        (byte)(order.NumberWipersArms != null ? order.NumberWipersArms.Count() : 0), order.IsAntenna, order.IsSpareWheel, order.IsCoverDecorEngine, order.IsTuner, order.FluelLevelPercent, id_order);      
                }
                List<CarMarkViewModel> car_marks = Session["car_marks"] as List<CarMarkViewModel>;
                if (car_marks != null)
                {
                    List<RemarkToStateCar> listRemarks = new List<RemarkToStateCar>();
                    foreach (var item in car_marks)
                    {
                        RemarkToStateCar remarkCar = new RemarkToStateCar { X_Axis_pos = item.X_Axis.Value, Y_Axis_pos = item.Y_Axis.Value, ID_order = id_order.Value as string, NumberType = item.TypeMark, RemarkText = item.addonInfo };
                        listRemarks.Add(remarkCar);
                    }
                    db.RemarkToStateCars.AddRange(listRemarks);
                    db.SaveChanges();
                }
                

                return RedirectToAction("CaryOnOrder", new { ID_order = id_order.Value });
            }
            else
                return HttpNotFound("Либо модель не валидна, либо нет заказа!");
        }
        
        [HttpGet]
        public ActionResult CaryOnOrder(string ID_order)
        {
            if (ID_order != null)
            {
                
                OrderServ order = db.OrderServs.Where(o => o.ID_order == ID_order).FirstOrDefault();
                ViewBag.ModelName ="["+order.Car.carModification.carSery.carModel.carBrand.brand_name+"] "+order.Car.carModification.carSery.series_name;
                var empls = db.Employees.Where(f => f.Post.ID_post == "mng01" && f.ContractToEmployees.FirstOrDefault(d => d.DismissDate == null) != null).ToList();
                SelectList listEmpls = new SelectList(empls, "PersonnelNumber", "NameSerName");
                ViewBag.ShortName = order.Client.NameSerName.ConvertToShortName();
                ViewBag.ListTypeServ = new SelectList(db.ServiceTypes, "IDtype", "TypeName");
                ViewBag.ListOfPayment = new SelectList(db.TypeOfPayments, "ID_Payment", "PaymentName");
                ViewBag.ListEmployee = listEmpls;
                ViewBag.TotalInfoForTable = db.GetTotalInfoExecServicesById(ID_order).FirstOrDefault();
                // входящие переменные для процедуры
                var totMoneyServ = new ObjectParameter("totMoneyServices", typeof(decimal));
                var totMoneyServFull = new ObjectParameter("totMoneyServicesFull", typeof(decimal));
                var totTimeServ = new ObjectParameter("totTimeServices", typeof(float));
                var totMoneyPartsFull = new ObjectParameter("totMoneyPartsFull", typeof(decimal));
                totMoneyPartsFull.Value = null;
                //выполнение процедуры
                db.GetCalculatedTotalMoneyTimeByOrder(ID_order, totMoneyServ, totTimeServ, totMoneyServFull, totMoneyPartsFull);
                //вносим в репозиторий c округлением до 2х знаков
                ViewData.Add("totMoneyServ", decimal.Round((decimal)totMoneyServ.Value, 2) );
                ViewData.Add("totMoneyServFull", decimal.Round((decimal)totMoneyServFull.Value,2));
                ViewData.Add("totTimeServ", totTimeServ.Value);
                ViewData.Add("totMoneyPartsFull", decimal.Round((decimal)(totMoneyPartsFull.Value), 2));
                ViewData.Add("totMoneyFullByOrder", decimal.Round((decimal)(totMoneyPartsFull.Value) + (decimal)totMoneyServFull.Value, 2));
                //////////////////////////////////////////////////////////////
                //
                
                if (order.DateFactCompleting == null)
                    return View(order);
                else
                    return View("CaryOnOrderClosed", order);
            }
            else
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        

        [HttpPost]
        public ActionResult ServiceSearch(string servname, int IDtype, int? page)
        {
            List<Service> services;
            services = db.Services.Where(f => f.ServiceName.Contains(servname) && f.IDtype == IDtype&&f.Available).ToList();

            if (services.Count == 0)
            { return HttpNotFound(); }
            else
            {
                int pageSize = 5;
                int pageNumber = (page ?? 1);
                return PartialView(services.ToPagedList(pageNumber, pageSize));
            }
        }
        [HttpGet]
        public ActionResult AddService(string idServ, string idOrder)
        {
            Service service = db.Services.Where(s => s.ID_service == idServ).FirstOrDefault();
            SelectList listEmployees = new SelectList(db.GetListAppropriateEmployees(idServ), "PersonnelNumber", "NameSerName");
            ExecServViewModel execServ = new ExecServViewModel{Service=service, ListEmployees=listEmployees, ID_order=idOrder, TaxAddedValue=20};
            return PartialView(execServ);
        }
        [HttpPost]
        public ActionResult AddService(ExecServViewModel serv)
        {
            int d=0;
            if (ModelState.IsValid)
            {
                d = db.AddServToOrder(serv.ID_order, serv.ID_service, serv.DateCompleting, serv.TakeTime, serv.Notes, serv.PersonnelNumber, serv.TaxAddedValue, false);
            }
            ViewBag.IsSuccess = d > 0 ? true.ToString() : false.ToString();
            ViewBag.IsAdding = true;
            IQueryable<ExecutingService> execServices = db.ExecutingServices.Where(e => e.ID_order == serv.ID_order);
            ViewBag.TotalInfoForTable = db.GetTotalInfoExecServicesById(serv.ID_order).FirstOrDefault();
            return PartialView("AddServiceToTable", execServices);
            
        }
        [HttpGet]
        public ActionResult EditService(string idServ, string idOrder)
        {
            Service service = db.Services.Where(s => s.ID_service == idServ).FirstOrDefault();
            SelectList listEmployees = new SelectList(db.GetListAppropriateEmployees(idServ), "PersonnelNumber", "NameSerName");
            ExecutingService attachedService= db.ExecutingServices.Where(c => c.ID_service == idServ && c.ID_order == idOrder).FirstOrDefault();
            ExecServViewModel execServ = new ExecServViewModel { Service = service, ListEmployees = listEmployees, ID_order = idOrder, PersonnelNumber = attachedService.PersonnelNumber, 
                TakeTime=attachedService.TakeTime, DateCompleting=attachedService.DateCompleting, Notes=attachedService.Notes, TaxAddedValue=attachedService.TaxAddedValue??0, IsEnableChange=attachedService.DateStart==null?true:false};
            return PartialView(execServ);
        }
        [HttpPost]
        public ActionResult EditService(ExecServViewModel serv)
        {

            int d = 0;
            if (ModelState.IsValid)
            {
                d = db.AddServToOrder(serv.ID_order, serv.ID_service, serv.DateCompleting, serv.TakeTime, serv.Notes, serv.PersonnelNumber, serv.TaxAddedValue, true);
            }
            ViewBag.IsSuccess = d > 0 ? true.ToString() : false.ToString();
            ViewBag.IsAdding = false;
            IQueryable<ExecutingService> execServices = db.ExecutingServices.Where(e => e.ID_order == serv.ID_order);
            ViewBag.TotalInfoForTable = db.GetTotalInfoExecServicesById(serv.ID_order).FirstOrDefault();
            return PartialView("AddServiceToTable", execServices);
        }


        [HttpGet]
        public ActionResult DisplayOrdersByClient(string ID_client)
        {
            IQueryable<OrderServ> orders = from ord in db.OrderServs where ord.ID_client == ID_client select ord;
            Client client= db.Clients.Find(ID_client);
            if(client!=null)
            ViewBag.ShortName = client.NameSerName.ConvertToShortName();
            ViewBag.CountOfOrders = orders.Count();
            SearchOrdersViewModel searchModel = new SearchOrdersViewModel();
            searchModel.ID_client = ID_client;
            return View(searchModel);
        }
        [HttpGet]
        public ActionResult DeleteOrder(string ID_order)
        {
            if (ID_order != null)
            {
                if (db.DeleteOrder(ID_order) > 0)
                    return JavaScript("$('#popSuccess').html('Заказа удален!').slideDown(300).delay(2000).slideUp(300); location.reload(); ");
                else return JavaScript("$('#popErorr').html('Невозможно удалить заказ! В нем уже задействованы услуги.').slideDown(300).delay(2000).slideUp(300); ");
            }
            return RedirectToAction("Index", "Home");
        }
        [HttpGet]
        public ActionResult DeleteAttachedService(string ID_order, string ID_service)
        {
            if (ID_order != null&&ID_service!=null)
            {
                if (db.DeleteAttachedService(ID_service, ID_order) > 0)
                    return JavaScript("$('#popSuccess').html('Услуга удалена!').slideDown(300).delay(2000).slideUp(300); location.reload(); ");
                else return JavaScript("$('#popErorr').html('Невозможно удалить услугу!').slideDown(300).delay(2000).slideUp(300); ");
            }
            return RedirectToAction("CaryOnOrder", "Order", new {@ID_order=ID_order });
        }
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Close(OrderServ order, int PersonnelNumber)
        {
            if (order.RejectionReason == null)
                if (db.CloseOrder(order.ID_order, order.DateFactCompleting, order.DateCompleting, order.ID_Payment, PersonnelNumber, true, null) > 0)
                    return JavaScript("$('#popSuccess').html('Заказ [" + order.ID_order + "] успешно закрыт!').slideDown(300).delay(2000).slideUp(300); location.reload(); ");
                else
                    return JavaScript("$('#popErorr').html('Заказ [" + order.ID_order + "] невозможно закрыть!').slideDown(300).delay(2000).slideUp(300); ");
            else
                if (db.CloseOrder(order.ID_order, order.DateFactCompleting, order.DateCompleting, null, PersonnelNumber, false, order.RejectionReason) > 0)
                    return JavaScript("$('#popSuccess').html('Заказ [" + order.ID_order + "] успешно закрыт!').slideDown(300).delay(2000).slideUp(300); location.reload(); ");
                else
                    return JavaScript("$('#popErorr').html('Заказ [" + order.ID_order + "] невозможно закрыть!').slideDown(300).delay(2000).slideUp(300); ");

        }
        public ActionResult AutocompleteSearch(string term, string ID_client)
        {

            ID_client = ID_client ?? "";
            term = term ?? "";
            var id_order = db.OrderServs.Where(a => a.ID_client.Contains(ID_client)&&a.ID_order.Contains(term))
                        .Select(a => new { value = a.ID_order })
                        .Distinct().Take(20);
            return Json(id_order, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Search()
        {

            return View();
        }
        [HttpPost]
        public ActionResult Search(SearchOrdersViewModel searchOrder, int? page)
        {
            int pageSize = 8;
            int pageNumber = (page ?? 1);
            string fieldSort;
            switch (searchOrder.NumbSortRow)
            {
                case 1: fieldSort="NameSerName";break;
                case 3: fieldSort="DateMakingOrder";break;
                case 2: fieldSort="Model";break;
                default: fieldSort="DateMakingOrder";break;
            }
               var  searchResult = db.SearchOrdersByParam(searchOrder.ID_client,
                                                    searchOrder.NameSerName,
                                                    searchOrder.ID_order,
                                                    searchOrder.DateMakingFrom,
                                                    searchOrder.DateMakingBefore,
                                                    searchOrder.IsClosed,
                                                    searchOrder.IsRejection,
                                                    fieldSort,
                                                    searchOrder.IsAsc,
                                                   pageNumber,
                                                    pageSize).ToList();
            int _countRows=searchResult.Count!=0?searchResult.FirstOrDefault().CountRows.Value:0;
            return PartialView("SearchResults", searchResult.ToCustomPageList(pageNumber, pageSize, _countRows));
        }

        public ActionResult TotalInfoServiceByOrder(string id)
        {
            if (id != null)
            {
                var execServices = db.GetTotalInfoServicesByOrder(id);
                if (execServices != null)
                    return PartialView("TableTotalInfoService", execServices.ToList());
                return null;
            }
            else
                return null;
        }

        public ActionResult TotalInfoAttachedPartByOrder(string id)
        {
            if (id != null)
            {
                var attachedParts = db.GetAllInfoAttachedPartById(id);
                if (attachedParts != null)
                    return PartialView("TableTotalAttachedParts", attachedParts.ToList());
                return null;
            }
            else
                return null;
        }
        public ActionResult TotalInfoAttachedCustomPartByOrder(string id)
        {
            if (id != null)
            {
                var attachedParts = db.UsingCustomSPartMats.Where(i => i.ID_order == id).ToList();
                if (attachedParts != null)
                    return PartialView("TableTotalAttachedCustomParts", attachedParts.ToList());
                return null;
            }
            else
                return null;
        }

        [HttpPost]
        public JsonResult AddRemarkItem(CarMarkViewModel car_mark)
        {
            if (car_mark.tempID == 1 && !car_mark.IsEdit) //добаляем новый список элементов если это первый элемент
            {
                List<CarMarkViewModel> car_marks = new List<CarMarkViewModel>();
                car_marks.Add(car_mark);
                Session["car_marks"] = car_marks;
            }
            else
            {
                if (Session["car_marks"] != null)// иначе проверяем есть ли сессия и добавляем в уже существующий список
                {
                    List<CarMarkViewModel> car_marks = Session["car_marks"] as List<CarMarkViewModel>;
                    if (car_mark.IsEdit)
                    { 
                        var remark = car_marks.Find(f => f.tempID == car_mark.tempID.Value);
                        remark.TypeMark = car_mark.TypeMark;
                        remark.addonInfo = car_mark.addonInfo;
                    }
                    else
                    car_marks.Add(car_mark);
                }
                else return Json(new { stat = 0 });
            }
            return Json(new { stat = 1, IsEdit = car_mark.IsEdit });
        }
        [HttpPost]
        public JsonResult GetInfoRemarkItem(byte? tempID)
        {
            if (Session["car_marks"] != null && tempID.HasValue)
            {
                List<CarMarkViewModel> car_marks = Session["car_marks"] as List<CarMarkViewModel>;
                var car_remark = car_marks.Find(f=>f.tempID==tempID.Value);
                return Json(car_remark);
            }
            return null;
        }
        [HttpPost]
        public JsonResult DeleteInfoRemarkItem(byte? tempID) 
        {
            if (Session["car_marks"] != null && tempID.HasValue)
            {
                List<CarMarkViewModel> car_marks = Session["car_marks"] as List<CarMarkViewModel>;
                var car_remark = car_marks.Find(f=>f.tempID==tempID.Value);
                bool stat = car_marks.Remove(car_remark);
                return Json(new { stat = stat });
            }
            return Json(new { stat = false }); ;
        }
        [HttpPost]
        public JsonResult GetInfoRemarkItemFromDB(string ID_order)
        {
            var remarkList = db.RemarkToStateCars.Where(f => f.ID_order == ID_order).ToArray();
            List<RemarkToStateCarViewModel> remarkListJson = new List<RemarkToStateCarViewModel>();
            foreach (var item in remarkList)
            {
                remarkListJson.Add(new RemarkToStateCarViewModel
                {
                    X_Axis_pos = item.X_Axis_pos,
                    Y_Axis_pos = item.Y_Axis_pos,
                    ID_order = item.ID_order,
                    ID_remark = item.ID_remark,
                    NumberType = item.NumberType,
                    RemarkText = item.RemarkText
                });
            }
            return Json(remarkListJson);
        }
	}
}