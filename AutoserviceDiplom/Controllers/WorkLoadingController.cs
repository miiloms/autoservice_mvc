﻿using AutoserviceDiplom.Models;
using AutoserviceDiplom.MyCustomClasses;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;

namespace AutoserviceDiplom.Controllers
{
     [Authorize]
    public class WorkLoadingController : Controller
    {
        //
        autoserviceDEntities db = new autoserviceDEntities();
        //общая таблица загрузки
        public ActionResult TableLoad_Common(DateTime? CurrentDate)
        {
            DateTime? d;
            if (CurrentDate.HasValue)
                d = CurrentDate.Value;
            else
                d = DateTime.Now;
            var dateParameter = new SqlParameter("@selDate", d);

            //System.Data.SqlClient.SqlParameter param = new System.Data.SqlClient.SqlParameter("@selDate", selectedDate);
            var tableLoad = db.Database.SqlQuery<TableLoad_Result>("TableLoadByDay @selDate", dateParameter).ToList();
            TableLoadViewModel model = new TableLoadViewModel();
            model.LoadEmployees = tableLoad;
            model.PreRecordServices = db.PreRecordServices.ToList();
            return View(model);
        }
        public ActionResult TableLoad(string id_order, DateTime? CurrentDate)
        {
            if (id_order == null)
                return HttpNotFound();
            DateTime? d;
            if (CurrentDate.HasValue)
                d = CurrentDate.Value;
            else
                d = DateTime.Now;
            var dateParameter = new SqlParameter("@selDate", d);
            
            //System.Data.SqlClient.SqlParameter param = new System.Data.SqlClient.SqlParameter("@selDate", selectedDate);
            var tableLoad = db.Database.SqlQuery<TableLoad_Result>("TableLoadByDay @selDate", dateParameter).ToList();
            TableLoadViewModel model=new TableLoadViewModel();
            OrderServ order = db.OrderServs.Find(id_order);
            if (order != null)
            {
                model.Order = order;
                model.LoadEmployees = tableLoad;
                model.PreRecordServices = db.PreRecordServices.ToList();
                try
                {
                    model.NameCar = "[" + order.Car.carModification.carSery.carModel.carBrand.brand_name + "] " + order.Car.carModification.carSery.series_name;
                }
                catch{}
                return View(model);
            }
            return HttpNotFound(); 
        }

        //для основных заказов
        [HttpPost]
        public ActionResult ReservTime(string[] ID_service, string ID_order, DateTime? CurrentDate, int? PersonnelNumber, int? hourSelect)
        {
            DateTime? date;
            if (ID_service != null && ID_order != null && CurrentDate.HasValue && PersonnelNumber.HasValue && hourSelect.HasValue)
            {
                date = new DateTime(CurrentDate.Value.Year, CurrentDate.Value.Month, CurrentDate.Value.Day, hourSelect.Value, 0, 0);
                string stringID_serv=string.Join("|", ID_service);
                var inputParams = new Dictionary<string, object> { 
                                                                    { "id_order", ID_order }, 
                                                                    { "dateTimeReserv", date  },
                                                                    { "persNumber", PersonnelNumber },
                                                                    { "charArrayID_services", stringID_serv }
                                                                 };
                ConnectionLayerDB dbConnection = new ConnectionLayerDB("autoserviceDB");
                int stat = dbConnection.ExecutionProcedure("ReservHourForService", inputParams);
                if (stat != 1)
                    return JavaScript("$('#popErorr').html('" + dbConnection.InfoMessage + "').slideDown(400).delay(1500).slideUp(400); ");
                else
                    return JavaScript("window.location.reload();");
            }
            return JavaScript("$('#popErorr').html('Ошибка запроса...').slideDown(400).delay(1500).slideUp(400); ");
        }

        [HttpPost]
        public ActionResult DeleteReservTime(DateTime? CurrentDate, int? PersonnelNumber, int? vb)
        {
            DateTime? date;
            if (CurrentDate.HasValue && PersonnelNumber.HasValue && vb.HasValue)
            {
                date = new DateTime(CurrentDate.Value.Year, CurrentDate.Value.Month, CurrentDate.Value.Day, vb.Value, 0, 0);

                int stat = db.CancelReservHourForService(date, PersonnelNumber);
                if (stat <0)
                    return JavaScript("$('#popErorr').html('Удалить данный промежуток времени не удалось...').slideDown(400).delay(1500).slideUp(400); ");
                else
                    return JavaScript("window.location.reload();");
            }
            return JavaScript("$('#popErorr').html('Ошибка запроса...').slideDown(400).delay(1500).slideUp(400); ");
        }
        [HttpPost]
        public ActionResult GetDetailsForTime(DateTime? CurrentDate, int? PersonnelNumber, int? vb)
        {
            DateTime? date;
            if (CurrentDate.HasValue && PersonnelNumber.HasValue && vb.HasValue)
            {
                date = new DateTime(CurrentDate.Value.Year, CurrentDate.Value.Month, CurrentDate.Value.Day, vb.Value, 0, 0);
                
                List<ExecutingService> execServList= db.ExecutingServices.Where(s => s.PersonnelNumber == PersonnelNumber && s.DateStart.Value == date.Value).ToList();
                List<PreRecordService> preServList = db.PreRecordServices.Where(s => s.PersonnelNumber == PersonnelNumber && s.DateReserv.Value == date.Value).ToList();
                ViewBag.SelectedDate = new DateTime(CurrentDate.Value.Year, CurrentDate.Value.Month, CurrentDate.Value.Day).ToString("yyyy-MM-dd");
                //для основных
                if (execServList.Count > 0 && preServList.Count == 0)
                {

                     try
                     {
                        OrderServ order = execServList.FirstOrDefault().OrderServ;
                        ViewBag.NameCar = "[" + order.Car.carModification.carSery.carModel.carBrand.brand_name + "] " + order.Car.carModification.carSery.series_name;
                        return PartialView("InfoTimePartial", execServList);
                     }
                     catch { ViewBag.NameCar = "Error"; }
                }
                else
                {   //для предзаказа
                    if (execServList.Count == 0 && preServList.Count > 0)
                    {
                        return PartialView("InfoPreTimePartial", preServList);
                    }
                }
                return JavaScript("$('#popErorr').html('В данном заказе еще нет услуг').slideDown(400).delay(1500).slideUp(400); ");
            }
            return JavaScript("$('#popErorr').html('Ошибка запроса...').slideDown(400).delay(1500).slideUp(400); ");
        }

        // страница нагрузок  мастеров для предварительной записи 
        public ActionResult TableLoadPreReserv(long? id_pre_record, DateTime? CurrentDate) 
        {
            if (id_pre_record == null)
                return HttpNotFound();
            DateTime? d;
            if (CurrentDate.HasValue)
                d = CurrentDate.Value;
            else
                d = DateTime.Now;
            var dateParameter = new SqlParameter("@selDate", d);
            var tableLoad = db.Database.SqlQuery<TableLoad_Result>("TableLoadByDay @selDate", dateParameter).ToList();

            TableLoadPreRecordViewModel model = new TableLoadPreRecordViewModel { Record = db.PreRecords.Find(id_pre_record), LoadEmployees = tableLoad, PreRecordServices=db.PreRecordServices.ToList() };
            return View(model);
        }

        //для предварительных заказов
        [HttpPost]
        public ActionResult ReservPreTime(string[] ID_service, string ID_record, DateTime? CurrentDate, int? PersonnelNumber, int? hourSelect)
        {
            DateTime? date;
            if (ID_service != null && ID_record != null && CurrentDate.HasValue && PersonnelNumber.HasValue && hourSelect.HasValue)
            {
                date = new DateTime(CurrentDate.Value.Year, CurrentDate.Value.Month, CurrentDate.Value.Day, hourSelect.Value, 0, 0);
                string stringID_serv = string.Join("|", ID_service);
                var inputParams = new Dictionary<string, object> { 
                                                                    { "id_record", ID_record }, 
                                                                    { "dateTimeReserv", date  },
                                                                    { "persNumber", PersonnelNumber },
                                                                    { "charArrayID_services", stringID_serv }
                                                                 };
                ConnectionLayerDB dbConnection = new ConnectionLayerDB("autoserviceDB");
                int stat = dbConnection.ExecutionProcedure("PreReservHourForService", inputParams);
                if (stat != 1)
                    return JavaScript("$('#popErorr').html('" + dbConnection.InfoMessage + "').slideDown(400).delay(1500).slideUp(400); ");
                else
                    return JavaScript("window.location.reload();");
            }
            return JavaScript("$('#popErorr').html('Ошибка запроса...').slideDown(400).delay(1500).slideUp(400); ");
        }

        [HttpPost]
        public ActionResult SearchParts(string id_part, string name_part)
        {
            List<Service> services;
            services = db.Services.Where(f => f.ServiceName.Contains(name_part) && f.ID_service.Contains(id_part)&&f.Available).Take(8).ToList();

            return PartialView("ServiceSearchPartial", services);
        }
	}
}