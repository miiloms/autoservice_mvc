﻿using AutoserviceDiplom.Models;
using AutoserviceDiplom.MyCustomClasses;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace AutoserviceDiplom.Controllers
{
     [Authorize]
    public class ReportViewController : Controller
    {
        //
        private autoserviceDEntities db = new autoserviceDEntities();
        private IdentityContext dbi = new IdentityContext();
        public ActionResult AnylisePartsTradeABC_XYZ()
        {
            ChoosingParamsAnalysis prms = new ChoosingParamsAnalysis();
            return View(prms);
        }
        public ActionResult AnyliseServicesTradeABC_XYZ()
        {
            ChoosingParamsAnalysis prms = new ChoosingParamsAnalysis();
            return View(prms);
        }
        public ActionResult GetResultAnlsParts(ChoosingParamsAnalysis prms, int? page)
        {
            int pageSize = 12;
            int pageNumber = (page ?? 1);
            if (ModelState.IsValid)
            {
                switch (prms.TypePeriodId)
                {
                    case 1:
                        {
                            var  model = db.ABC_XYZ_parts_by_month(prms.EndDate, prms.StartDate).ToList();
                            return PartialView("ResultsAnlsPartsByMonthPartial", model.ToPagedList(pageNumber, pageSize));
                        }
                    case 2:
                        {
                            var model = db.ABC_XYZ_parts_by_quarter(prms.EndDate, prms.StartDate).ToList();
                            return PartialView("ResultsAnlsPartsByQuaterPartial", model.ToPagedList(pageNumber, pageSize));
                        }
                    case 3: 
                        {
                            var model = db.ABC_XYZ_parts_by_day(prms.EndDate, prms.StartDate).ToList();
                            return PartialView("ResultsAnlsPartsByDayPartial", model.ToPagedList(pageNumber, pageSize));
                        }
                    default:
                        {
                            var model = db.ABC_XYZ_parts_by_month(prms.EndDate, prms.StartDate).ToList();
                            return PartialView("ResultsAnlsPartsByMonthPartial", model.ToPagedList(pageNumber, pageSize));
                        }
                }
                
            }
            else
                return HttpNotFound();
        }

        public ActionResult GetResultAnlsServices(ChoosingParamsAnalysis prms, int? page)
        {
            int pageSize = 12;
            int pageNumber = (page ?? 1);
            if (ModelState.IsValid)
            {
                switch (prms.TypePeriodId)
                {
                    case 1:
                        {
                            var model = db.ABC_XYZ_services_by_month(prms.EndDate, prms.StartDate).ToList();
                            return PartialView("ResultsAnlsServicesByMonthPartial", model.ToPagedList(pageNumber, pageSize));
                        }
                    case 2:
                        {
                            var model = db.ABC_XYZ_services_by_quater(prms.EndDate, prms.StartDate).ToList();
                            return PartialView("ResultsAnlsServicesByQuaterPartial", model.ToPagedList(pageNumber, pageSize));
                        }
                    case 3:
                        {
                            var model = db.ABC_XYZ_services_by_day(prms.EndDate, prms.StartDate).ToList();
                            return PartialView("ResultsAnlsServicesByDayPartial", model.ToPagedList(pageNumber, pageSize));
                        }
                    default:
                        {
                            var model = db.ABC_XYZ_services_by_month(prms.EndDate, prms.StartDate).ToList();
                            return PartialView("ResultsAnlsServicesByMonthPartial", model.ToPagedList(pageNumber, pageSize));
                        }
                }

            }
            else
                return HttpNotFound();
        }

        public ActionResult ReportByExecServByMonthForEmployee(int? year, int? month)
        {
            year = year ?? DateTime.Now.Year;
            month = month ?? DateTime.Now.Month;
            Dictionary<int,string> listmonth = new Dictionary<int,string>{{1,"Январь"},{2,"Февраль"}, {3,"Март"},{4,"Апрель"},{5,"Май"},{6,"Июнь"},{7,"Июль"},{8,"Август"},{9,"Сентябрь"},{10,"Октябрь"},{11,"Ноябрь"},{12,"Декабрь"}};
            List<int> listyear = new List<int>();
            for (int y = 2015; y <= DateTime.Now.Year; y++)
                listyear.Add(y);
            ViewBag.selelctListyear = new SelectList(listyear, "", "", DateTime.Now.Year);
            ViewBag.selelctListmonth = new SelectList(listmonth, "Key", "Value", DateTime.Now.Month);
            var report = db.ReportByExecServByMonthForEmployees.Where(f => f.Year == year && f.Month == month).OrderByDescending(m=>m.TotalMoney).ToList();
            return View(report);
        }
        public void PrintReportByExecServByMonthForEmployee(int? year, int? month)
        {
            year = year ?? DateTime.Now.Year;
            month = month ?? DateTime.Now.Month;
            ViewBag.SelectedDate = new DateTime(year.Value, month.Value, 1);
            ApplicationUser user=dbi.Users.Where(u=>u.UserName==User.Identity.Name).FirstOrDefault();
            if(user!=null)
                ViewBag.NamePrinterMan = db.Employees.Find(user.PersonnelNumber).NameSerName.ConvertToShortName();
            var report = db.ReportByExecServByMonthForEmployees.Where(f => f.Year == year && f.Month == month).OrderByDescending(m => m.TotalMoney).ToList();
            Html2Pdf.ConvertHtml2PdfResponse(this, "PrintReportByExecServByMonthForEmployee", report);
        }

        public ActionResult ReportByExecOrders()
        {
            Dictionary<int, string> listmonth = new Dictionary<int, string> { { 1, "Январь" }, { 2, "Февраль" }, { 3, "Март" }, { 4, "Апрель" }, { 5, "Май" }, { 6, "Июнь" }, { 7, "Июль" }, { 8, "Август" }, { 9, "Сентябрь" }, { 10, "Октябрь" }, { 11, "Ноябрь" }, { 12, "Декабрь" } };
            List<int> listyear = new List<int>();
            for (int y = 2015; y <= DateTime.Now.Year; y++)
                listyear.Add(y);
            ViewBag.selelctListyear = new SelectList(listyear, "", "", DateTime.Now.Year);
            ViewBag.selelctListmonth = new SelectList(listmonth, "Key", "Value", DateTime.Now.Month);
            return View();
        }
        [HttpPost]
        public ActionResult ReportByExecOrders(int? year, int? month, int? page, int? NumbSortRow, bool? IsAsc)
        {
            year = year ?? DateTime.Now.Year;
            month = month ?? DateTime.Now.Month;
            NumbSortRow = NumbSortRow ?? 3;
            IsAsc = IsAsc ?? false;
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            List<ReportByExecOrder> report;
            switch (NumbSortRow.Value)
            {
                case 1: 
                    report = IsAsc.Value?db.ReportByExecOrders.Where(d => d.DateMaking.Value.Year == year && d.DateMaking.Value.Month == month).OrderBy(o => o.NameSerName).ToList():
                                         db.ReportByExecOrders.Where(d => d.DateMaking.Value.Year == year && d.DateMaking.Value.Month == month).OrderByDescending(o => o.NameSerName).ToList();
                    break;
                case 2:
                        report = IsAsc.Value ? db.ReportByExecOrders.Where(d => d.DateMaking.Value.Year == year && d.DateMaking.Value.Month == month).OrderBy(o => o.TotallPriceOrder).ToList() :
                                             db.ReportByExecOrders.Where(d => d.DateMaking.Value.Year == year && d.DateMaking.Value.Month == month).OrderByDescending(o => o.TotallPriceOrder).ToList();
                        break;
                case 3:
                        report = IsAsc.Value ? db.ReportByExecOrders.Where(d => d.DateMaking.Value.Year == year && d.DateMaking.Value.Month == month).OrderBy(o => o.DateMaking).ToList() :
                                             db.ReportByExecOrders.Where(d => d.DateMaking.Value.Year == year && d.DateMaking.Value.Month == month).OrderByDescending(o => o.DateMaking).ToList();
                        break;
                default: report = db.ReportByExecOrders.Where(d => d.DateMaking.Value.Year == year && d.DateMaking.Value.Month == month).OrderBy(o => o.DateMaking).ToList(); break;
            }

            if ((int)Math.Ceiling((decimal)report.Count / pageSize) == page)
            {
                List<decimal> listMoney= new List<decimal>(5);
                listMoney.Add(Math.Round(report.Sum(s => s.CostServ ?? 0)));
                listMoney.Add(Math.Round(report.Sum(s => s.TaxAddedValue ?? 0)));
                listMoney.Add(Math.Round(report.Sum(s => s.PriceServ ?? 0)));
                listMoney.Add(Math.Round(report.Sum(s => s.TotallPriceParts ?? 0)));
                listMoney.Add(Math.Round(report.Sum(s => s.TotallPriceOrder?? 0)));
                ViewBag.listMoney = listMoney;
            }
            return PartialView("ReportByExecOrdersResultPartial", report.ToPagedList(pageNumber, pageSize));
        }

        public void PrintReportByExecOrders(int? year, int? month, int? NumbSortRow, bool? IsAsc)
        {
            year = year ?? DateTime.Now.Year;
            month = month ?? DateTime.Now.Month;
            NumbSortRow = NumbSortRow ?? 3;
            IsAsc = IsAsc ?? false;
            List<ReportByExecOrder> report;
            ViewBag.SelectedDate = new DateTime(year.Value, month.Value, 1);
            ApplicationUser user = dbi.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            if (user != null)
                ViewBag.NamePrinterMan = db.Employees.Find(user.PersonnelNumber).NameSerName.ConvertToShortName();
            switch (NumbSortRow.Value)
            {
                case 1:
                    report = IsAsc.Value ? db.ReportByExecOrders.Where(d => d.DateMaking.Value.Year == year && d.DateMaking.Value.Month == month).OrderBy(o => o.NameSerName).ToList() :
                                         db.ReportByExecOrders.Where(d => d.DateMaking.Value.Year == year && d.DateMaking.Value.Month == month).OrderByDescending(o => o.NameSerName).ToList();
                    break;
                case 2:
                    report = IsAsc.Value ? db.ReportByExecOrders.Where(d => d.DateMaking.Value.Year == year && d.DateMaking.Value.Month == month).OrderBy(o => o.TotallPriceOrder).ToList() :
                                         db.ReportByExecOrders.Where(d => d.DateMaking.Value.Year == year && d.DateMaking.Value.Month == month).OrderByDescending(o => o.TotallPriceOrder).ToList();
                    break;
                case 3:
                    report = IsAsc.Value ? db.ReportByExecOrders.Where(d => d.DateMaking.Value.Year == year && d.DateMaking.Value.Month == month).OrderBy(o => o.DateMaking).ToList() :
                                         db.ReportByExecOrders.Where(d => d.DateMaking.Value.Year == year && d.DateMaking.Value.Month == month).OrderByDescending(o => o.DateMaking).ToList();
                    break;
                default: report = db.ReportByExecOrders.Where(d => d.DateMaking.Value.Year == year && d.DateMaking.Value.Month == month).OrderBy(o => o.DateMaking).ToList(); break;
            }

            List<decimal> listMoney = new List<decimal>(5);
            listMoney.Add(Math.Round(report.Sum(s => s.CostServ ?? 0)));
            listMoney.Add(Math.Round(report.Sum(s => s.TaxAddedValue ?? 0)));
            listMoney.Add(Math.Round(report.Sum(s => s.PriceServ ?? 0)));
            listMoney.Add(Math.Round(report.Sum(s => s.TotallPriceParts ?? 0)));
            listMoney.Add(Math.Round(report.Sum(s => s.TotallPriceOrder ?? 0)));
            ViewBag.listMoney = listMoney;
            string headerHtml = ViewRenderer.RenderPartialView("~/views/reportview/table_header.cshtml", null, ControllerContext);

            Html2Pdf.ConvertHtml2PdfResponse(this, "PrintReportByExecOrders", report, true, headerHtml, SelectPdf.PdfPageSize.A4, SelectPdf.PdfPageOrientation.Landscape);
        }

        public ActionResult ReportByProvidersByMoney()
        {
            Dictionary<int, string> listmonth = new Dictionary<int, string> {{ 0, "За весь год" }, { 1, "Январь" }, { 2, "Февраль" }, { 3, "Март" }, { 4, "Апрель" }, { 5, "Май" }, { 6, "Июнь" }, { 7, "Июль" }, { 8, "Август" }, { 9, "Сентябрь" }, { 10, "Октябрь" }, { 11, "Ноябрь" }, { 12, "Декабрь" } };
            List<int> listyear = new List<int>();
            for (int y = 2015; y <= DateTime.Now.Year; y++)
                listyear.Add(y);
            ViewBag.selelctListyear = new SelectList(listyear, "", "", DateTime.Now.Year);
            ViewBag.selelctListmonth = new SelectList(listmonth, "Key", "Value", new Dictionary<int, string> { {0, "За весь год"} });
            return PartialView();
        }
        [HttpPost]
        public ActionResult ReportByProvidersByMoney(int? year, int? month, int? page)
        {  
            year = year ?? DateTime.Now.Year;
            month = month ?? DateTime.Now.Month;
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            ViewBag.DictMonthName= new  Dictionary<int, string> { { 1, "Январь" }, { 2, "Февраль" }, { 3, "Март" }, { 4, "Апрель" }, { 5, "Май" }, { 6, "Июнь" }, { 7, "Июль" }, { 8, "Август" }, { 9, "Сентябрь" }, { 10, "Октябрь" }, { 11, "Ноябрь" }, { 12, "Декабрь" } };
            List<AmountMoneyByProvider> report;
            if(month==0)
                report = db.AmountMoneyByProviders.Where(f => f.Year == year).ToList();
            else
            report = db.AmountMoneyByProviders.Where(f=>f.Year==year&&f.Month==month).ToList();
            return PartialView("ReportByProvidersByResultPartial", report.ToPagedList(pageNumber, pageSize));
        }

        public void PrintReportByProvidersByMoney(int? year, int? month)
        {
            year = year ?? DateTime.Now.Year;
            month = month ?? DateTime.Now.Month;
            List<AmountMoneyByProvider> report;
            ViewBag.SeletedMonthName = month.Value != 0 ? (new DateTime(year.Value, month.Value, 1)).ToString("MMMM"):null;
            var dfds = month.Value != 0 ? (new DateTime(year.Value, month.Value, 1)).ToString("MMMM") : null; 
            ViewBag.SelectedYear = year; 
            ApplicationUser user = dbi.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            if (user != null)
                ViewBag.NamePrinterMan = db.Employees.Find(user.PersonnelNumber).NameSerName.ConvertToShortName();
            ViewBag.DictMonthName = new Dictionary<int, string> { { 1, "Январь" }, { 2, "Февраль" }, { 3, "Март" }, { 4, "Апрель" }, { 5, "Май" }, { 6, "Июнь" }, { 7, "Июль" }, { 8, "Август" }, { 9, "Сентябрь" }, { 10, "Октябрь" }, { 11, "Ноябрь" }, { 12, "Декабрь" } };
            if (month == 0)
                report = db.AmountMoneyByProviders.Where(f => f.Year == year).ToList();
            else
                report = db.AmountMoneyByProviders.Where(f => f.Year == year && f.Month == month).ToList();
            string headerHtml = ViewRenderer.RenderPartialView("~/views/reportview/table_header_for_provider.cshtml", null, ControllerContext);

            Html2Pdf.ConvertHtml2PdfResponse(this, "PrintReportByProvidersByMoney", report, true, headerHtml, SelectPdf.PdfPageSize.A4, SelectPdf.PdfPageOrientation.Portrait);
        }
	}
}