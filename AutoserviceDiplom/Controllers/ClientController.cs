﻿using AutoserviceDiplom.Models;
using AutoserviceDiplom.MyCustomClasses;
using PagedList;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web.Mvc;

namespace AutoserviceDiplom.Controllers
{
    [Authorize]
    public class ClientController : Controller
    {
        autoserviceDEntities db = new autoserviceDEntities();
        public ActionResult List()
        {
            return View();
        }
        public ActionResult AutocompleteSearch(string term)
        {
            bool IsContainsDigit=false;
            foreach (char ch in term.ToCharArray())
            {
                if (char.IsDigit(ch))
                    IsContainsDigit = true;
            }
            if (IsContainsDigit)
            {
                var nameClient = db.Clients.Where(a => a.ID_client.Contains(term))
                           .Select(a => new { value = a.ID_client }).Take(15).Distinct();
                return Json(nameClient, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var nameClient = db.Clients.Where(a => a.NameSerName.Contains(term))
                            .Select(a => new { value = a.NameSerName }).Take(15)
                            .Distinct();
                return Json(nameClient, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult ClientSearch(string name, string id, int? page)
        {
            List<Client> clients;
            if(name!=null)
                clients = db.Clients.Where(f => f.NameSerName.Contains(name)).ToList(); 
            else
                clients = db.Clients.Where(f => f.ID_client.Contains(id)).ToList();
            if (clients.Count == 0)
            { return HttpNotFound(); }
            else
            {
                int pageSize = 10;
                int pageNumber = (page ?? 1);
                 return PartialView(clients.ToPagedList(pageNumber, pageSize));
            }
        }

        public ActionResult CreateNewClient(long? id_record)
        {
            PreRecord record = db.PreRecords.Find(id_record);
            if (record != null)
            {
                ViewData.Add("idRecord", record.ID_record);
                Client model = new Client { NameSerName = record.NameSerName, Phone = record.Phone_number };
                return View(model);
            }
            return View();
        }
        [HttpPost]
        public ActionResult CreateNewClient(Client client, long? id_record)
        {
            if (ModelState.IsValid)
            {
                ObjectParameter id_client = new ObjectParameter("id_client", typeof(string));
                int stat = db.AddClient(client.NameSerName, client.NumberDriveLicense, client.Phone, client.ExpirationDateLicense, client.DateBirth, client.Adress, id_client);
                if (stat<=0)
                    return View(client);
                if(id_record!=null)
                    return RedirectToAction("AddOrder", "Order", new { ID = id_client.Value, id_record = id_record });
                return RedirectToAction("List", "Client");
            }
            else
                return View(client);   
        }
        [HttpGet]
        public ActionResult ShowClientDetail(string ID)
        {
            if (ID != null)
            {
                var client = db.Clients.Where(id => id.ID_client == ID).FirstOrDefault();
                ViewBag.ShortName = client.NameSerName.ConvertToShortName();
                if (client != null)
                    return View(client);
                else
                    return HttpNotFound();
            }
            else
                return HttpNotFound();
        }
        [HttpGet]
        public ActionResult EditClient(string ID)
        {
            if (ID!= null)
            {
                var client = db.Clients.First(c => c.ID_client == ID);
                if(client==null)
                    return HttpNotFound();
                ViewBag.ShortName = client.NameSerName.ConvertToShortName();
                return View(client); 
            }
                
            else
              return HttpNotFound();
        }
        [HttpPost]
        public ActionResult EditClient(Client client)
        {
            if (ModelState.IsValid)
            {
                db.Entry(client).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("ShowClientDetail", new {ID=client.ID_client});
            }

            else
                return HttpNotFound();
        }
        public ActionResult Delete(string ID)
        {
            if (ID!= null)
            {
                Client client = new Client { ID_client = ID };
                db.Entry(client).State = EntityState.Deleted;
                db.SaveChanges();
                return RedirectToAction("Index","Home");
            }

            else
                return HttpNotFound();
        }
	}
}