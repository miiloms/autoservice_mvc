//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AutoserviceDiplom.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ReportByExecOrder
    {
        public string ID_order { get; set; }
        public Nullable<System.DateTime> DateMaking { get; set; }
        public string NameSerName { get; set; }
        public string Model { get; set; }
        public string Registr_Number { get; set; }
        public Nullable<int> IssueYear { get; set; }
        public Nullable<decimal> CostServ { get; set; }
        public Nullable<decimal> TaxAddedValue { get; set; }
        public Nullable<decimal> PriceServ { get; set; }
        public Nullable<decimal> TotallPriceParts { get; set; }
        public Nullable<decimal> TotallPriceOrder { get; set; }
    }
}
