﻿using AutoserviceDiplom.Models;
using AutoserviceDiplom.MyCustomClasses;
using PagedList;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
namespace AutoserviceDiplom.Controllers
{
    [Authorize]
    public class EmployeeController : Controller
    {
        autoserviceDEntities db = new autoserviceDEntities();
        public ActionResult Search()
        {
            return View();
        }
        public ActionResult AutocompleteSearch(string term)
        {
            bool IsContainsDigit = false;
            foreach (char ch in term.ToCharArray())
            {
                if (char.IsDigit(ch))
                    IsContainsDigit = true;
            }
            if (IsContainsDigit)
            {
                var nameClient = db.Employees.Where(a => a.PersonnelNumber==int.Parse(term))
                           .Select(a => new { value = a.PersonnelNumber })
                           .Distinct();
                return Json(nameClient, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var nameClient = db.Employees.Where(a => a.NameSerName.Contains(term))
                            .Select(a => new { value = a.NameSerName })
                            .Distinct();
                return Json(nameClient, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult Search(string name, string id, int? page)
        {
            List<Employee> employees;
            if (name != null)
                employees = db.Employees.Where(f => f.NameSerName.Contains(name)).ToList();
            else
            {
                int tempId = int.TryParse(id, out tempId)? tempId : 0;   
                employees = db.Employees.Where(f => f.PersonnelNumber==tempId).ToList();
            }
                
            if (employees.Count == 0)
            { return HttpNotFound(); }
            else
            {
                int pageSize = 5;
                int pageNumber = (page ?? 1);
                return PartialView("ResultsSearch",employees.ToPagedList(pageNumber, pageSize));
            }
        }

        [HttpGet]
        public ActionResult ShowDetails(int? PersonnelNumber)
        {
            Employee employee=null;
            if(PersonnelNumber!=null)
                employee = db.Employees.Where(p => p.PersonnelNumber == PersonnelNumber).FirstOrDefault();
            if (employee != null)
            {
                ViewBag.ShortName = employee.NameSerName.ConvertToShortName();
                return View(employee);
            }
            return HttpNotFound();
        }
        [HttpGet]
        public ActionResult Edit(int? PersonnelNumber)
        {
            Employee empl = db.Employees.Find(PersonnelNumber);
            if (PersonnelNumber != null)
            {
                ViewBag.ShortName = empl.NameSerName.ConvertToShortName();
                ViewBag.ListPost = new SelectList(db.Posts, "ID_post", "NamePost");
                ViewBag.ListDepart = new SelectList(db.Departments, "ID_department", "NameDepart");
                return View(empl);
            }
            else return HttpNotFound();
        }
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Edit(Employee empl)
        {
            if (ModelState.IsValid)
            {
                db.Entry(empl).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("ShowDetails", new { empl.PersonnelNumber });
            }
            else return HttpNotFound();
        }

        public ActionResult ShowContracts(int? PersonnelNumber)
        {
            if (PersonnelNumber != null)
            {
                Employee empl = db.Employees.Find(PersonnelNumber);
                IEnumerable<ContractToEmployee> contractsByEmpl = empl.ContractToEmployees;
                ContractsViewModel contracts = new ContractsViewModel() { contracts = contractsByEmpl, NameSerName = empl.NameSerName, PersonnelNumber = empl.PersonnelNumber};
                ViewBag.ShortName = empl.NameSerName.ConvertToShortName();
                return View(contracts);
            }
            else return HttpNotFound();
        }
        [HttpGet]
        public ActionResult Create()
        {
            ViewBag.ListPost = new SelectList(db.Posts, "ID_post", "NamePost");
            ViewBag.ListDepart = new SelectList(db.Departments, "ID_department", "NameDepart");
            return View(new Employee());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Employee empl)
        {
            if (empl != null && ModelState.IsValid)
            {
                db.Entry(empl).State = EntityState.Added;
                db.SaveChanges();
                return RedirectToAction("ShowDetails",empl);
            }
            return HttpNotFound(); ;
        }

        public ActionResult Delete(int? PersonnelNumber) 
        {
            Employee empl = db.Employees.Find(PersonnelNumber);
            if(empl!=null)
            {
                db.Employees.Remove(empl);
                try
                {
                    db.SaveChanges();
                }
                catch
                {
                    return JavaScript("$('#popErorr').html('Данный сотрудник не может быть удален!').slideDown(500).delay(3000).slideUp(500); ");
                }
                return JavaScript("$('#popSuccess').html('Сотрудник, " + empl.NameSerName + ", удален!').slideDown(500).delay(3000).slideUp(500); setInterval(function () {window.location.href='/Employee/Search';},4000)");
            }
            return null;
        }
	}
}