﻿$(document).ready(function () {
    //для строки поиска услуг
    $('#seach-serv-pre-reserv .glyphicon-search').click(function () {
        if ($('#seach-serv-pre-reserv div input').css('display') == 'none') {
            $('#seach-serv-pre-reserv').animate({ 'width': '450px' }, 300);
            $('#seach-serv-pre-reserv div input').delay(300).fadeIn(300);
            $('#arrow_close').show();
        }
    });
    $('#arrow_close').click(function () {
        $('#result_search_parts').slideUp(100);
        $(this).hide();
        $('#seach-serv-pre-reserv div input').hide();
        $('#seach-serv-pre-reserv').delay(100).animate({ 'width': '50px' }, 300);
    });


    //для календаря
    $('.date').datetimepicker({ language: 'ru', pickTime: false, format: 'YYYY-MM-DD' });
    //выделяем по цветам график загрузки мастеров
    $('.tbl-load  td:not(:first-child)').each(function () {
        if ($(this).children(':last-child').val() == 0)
            $(this).addClass('vacancy');
        else {
            if (!$(this).hasClass('busy-current-order')) {
                if ($(this).hasClass('busy-pre-reserv')) {
                    $(this).append('<span>' + $(this).children(':last-child').val() + ':00' + '</span>'); return 0;
                }
                $(this).addClass('busy');
                $(this).append($(this).children(':last-child').val() + ':00');

            }
            else
                $(this).append('<span>' + $(this).children(':last-child').val() + ':00' + '</span><div class="circle"></div>');
        }

    })
    //вешаем на строки с услугами выделение при нажатии
    $('.tbl-service').on('click', 'tr:has(td) td:not(:last-child)', (function () {
        var summTime = 0;
        if ($(this).parent().children().eq(3).html() == '') //можно выделять только те, что еще не задействаованны
            if ($(this).parent().hasClass('selected-row'))
                $(this).parent().removeClass('selected-row')
            else
                $(this).parent().addClass('selected-row');
        $(".selected-row td:nth-child(3)").each(function () {
            summTime += Number($(this).html().replace(",", "."));
        })
        $('#totTime').html(summTime.toFixed(2));

    })
    )

    //вешаем на ячейки на таблице загрузок выделение для добавления
    $('.tbl-load td:not(:first-child)').click(function () {
        if ($(this).hasClass('vacancy')) {
            var sender = $(this).children('input:first-child').add($(this).parent().children().eq(0).find('input'));
            $.ajax(
            {
                url: '/WorkLoading/ReservPreTime',
                data: $(".selected-row td input").add(sender).add('input[name="CurrentDate"]').add('#ID_record').serialize(),
                type: "POST",
                datatype: "json"
            });
        }

    })
    //вешаем на ячейки на таблице загрузок для удаления услуги
    $('.circle').click(function () {
        $.ajax(
        {
            url: '/WorkLoading/DeleteReservTime',
            data: $(this).parent().children('input[name="vb"]').add('input[name="CurrentDate"]').add($(this).parent().parent().children().eq(0).find('input')).serialize(),
            type: "POST",
            datatype: "json"
        });
    })
    //вешаем на календарь  на его изменение
    $('.datepicker tbody, .backward-day, .forward-day').click(function () {
        var culpritObject = $(this);
        setTimeout(function () {
            var currentDay = new Date($('#CurrentDate').val());

            if (culpritObject.hasClass('backward-day'))
                currentDay.setDate(currentDay.getDate() - 1)
            else
                if (culpritObject.hasClass('forward-day'))
                    currentDay.setDate(currentDay.getDate() + 1)
            var stringDate = currentDay.getFullYear() + '-' + (currentDay.getMonth() + 1) + '-' + (currentDay.getDate() >= 10 ? currentDay.getDate() : '0' + currentDay.getDate());

            window.location = '/WorkLoading/TableLoadPreReserv/?id_pre_record=' + $('#ID_record').val() + '&CurrentDate=' + stringDate;
        }, 1)

    })
    /////для закрытия инфо-окна
    $('.close-sign').click(function () {
        $('.my-comment').fadeOut(400);
    })
    //вешаем на ячейки на таблице загрузок для подгрузки инфы
    $('.busy, .busy-current-order span, .busy-pre-reserv').click(function () {
        if ($(this).hasClass('busy') || $(this).hasClass('busy-pre-reserv'))
            var my_data = $(this).children('input[name="vb"]').add('input[name="CurrentDate"]').add($(this).parent().children().eq(0).find('input'));
        else
            var my_data = $(this).parent().children('input[name="vb"]').add('input[name="CurrentDate"]').add($(this).parent().parent().children().eq(0).find('input'));
        var elem = $(this);
        $.ajax(
        {
            url: '/WorkLoading/GetDetailsForTime',
            data: my_data.serialize(),
            type: "POST",
            datatype: "json",
            success: function (content) {
                $('#contents_info').html(content);
                var coord = elem.offset();
                var w = elem.width();
                var h = elem.height();
                var h_box = $('.my-comment').height();
                $('.my-comment').hide();
                $('.my-comment').css('top', coord.top + h * 0.5 - 0.5 * h_box + 10).css('left', coord.left + w + 12);
                $('.my-comment').fadeIn(400);
            }
        });
    })
    //прикрепить услугу из поиска
    $('#result_search_parts').on('click', '.glyphicon-plus', function () {
        var flag = null;
        var id_serv = $(this).parent().parent().children().eq(1).html();
        $('.tbl-service tr').each(function () {
            var id_s = $(this).children().eq(1).find('input').val();
            if (id_serv == id_s)
                flag = true;
        });

        if (flag)// если уже есть в таблице, то не добавляем
            return null;
        var fd = $(this).parent().parent().children().find('input');
        $.ajax(
            {
                url: '/PreRecord/AttachService',
                data: $(this).parent().parent().children().find('input').add('#ID_record').serialize(),
                type: "POST",
                datatype: "json",
                success: function (result) {
                    $('.tbl-service tr:has(td)').remove();
                    $('.tbl-service').append(result);
                }
            });
    })
    // открепить услугу
    $('.tbl-service').on('click', '.glyphicon-remove', function () {
        var targetRow = $(this).parent().parent();
        $.ajax(
            {
                url: '/PreRecord/DeleteAttachService',
                data: $(this).parent().parent().children().find('input').add('#ID_record').serialize(),
                type: "POST",
                datatype: "json",
                success: function () {
                    targetRow.remove();
                }
            });
    })
})