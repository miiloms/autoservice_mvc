//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AutoserviceDiplom.Models
{
    using System;
    
    public partial class FindContractByParams_Result
    {
        public int IDcontract { get; set; }
        public System.DateTime RecruitDate { get; set; }
        public Nullable<System.DateTime> DismissDate { get; set; }
        public string Type { get; set; }
        public string NameSerName { get; set; }
        public Nullable<int> Term { get; set; }
        public int PersonnelNumber { get; set; }
    }
}
