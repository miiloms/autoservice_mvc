//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AutoserviceDiplom.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class AcceptDocument
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AcceptDocument()
        {
            this.AcceptanceCustomSParts = new HashSet<AcceptanceCustomSPart>();
        }
    
        public int ID_acceptance { get; set; }
        public int PersonnelNumber { get; set; }
        public string ID_client { get; set; }
        public Nullable<System.DateTime> AcceptDate { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AcceptanceCustomSPart> AcceptanceCustomSParts { get; set; }
        public virtual Employee Employee { get; set; }
        public virtual Client Client { get; set; }
    }
}
