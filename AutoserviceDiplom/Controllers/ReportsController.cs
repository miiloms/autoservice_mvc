﻿using AutoserviceDiplom.Models;
using AutoserviceDiplom.Models.ReportModel;
using AutoserviceDiplom.MyCustomClasses;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web.Mvc;

namespace AutoserviceDiplom.Controllers
{
     [Authorize]
    public class ReportsController : Controller
    {
        autoserviceDEntities db = new autoserviceDEntities();
        public void GetReportOrderPdf(string ID_order)
        {
            ReportOrderForClientModel report = new ReportOrderForClientModel();
            report.ServiceList = db.GetTotalInfoServicesByOrder(ID_order).ToList();
            report.PartList = db.GetAllInfoAttachedPartById(ID_order).ToList();
            report.PartCustomList = db.UsingCustomSPartMats.Where(f => f.ID_order == ID_order).ToList();
            report.Order = db.OrderServs.Where(f => f.ID_order == ID_order).FirstOrDefault();
            ///
            // входящие переменные для процедуры
            var totMoneyServ = new ObjectParameter("totMoneyServices", typeof(decimal));
            var totMoneyServFull = new ObjectParameter("totMoneyServicesFull", typeof(decimal));
            var totTimeServ = new ObjectParameter("totTimeServices", typeof(float));
            var totMoneyPartsFull = new ObjectParameter("totMoneyPartsFull", typeof(decimal));
            //выполнение процедуры
            db.GetCalculatedTotalMoneyTimeByOrder(ID_order, totMoneyServ, totTimeServ, totMoneyServFull, totMoneyPartsFull);
            //вносим в хранилище c округлением до 2х знаков
            ViewData.Add("totMoneyServ", (double)decimal.Round((decimal)totMoneyServ.Value, 2));
            ViewData.Add("totMoneyServFull", (double)decimal.Round((decimal)totMoneyServFull.Value, 2));
            ViewData.Add("totTimeServ", totTimeServ.Value);
            ViewData.Add("totMoneyPartsFull", (double)decimal.Round((decimal)totMoneyPartsFull.Value, 2));
            ViewData.Add("totMoneyFullByOrder", (double)decimal.Round((decimal)totMoneyPartsFull.Value + (decimal)totMoneyServFull.Value, 2));
            ///
            try
            {
                ViewBag.NameManagerComplete = db.ExecutingServices.Where(s => s.ID_order == ID_order && s.ID_service == "mngclose").FirstOrDefault().Employee.NameSerName.ConvertToShortName();
                ViewBag.PostManagerComplete = db.ExecutingServices.Where(s => s.ID_order == ID_order && s.ID_service == "mngclose").FirstOrDefault().Employee.Post.NamePost;
            }
            catch { }
            Html2Pdf.ConvertHtml2PdfResponse(this, "OrderForClientReport", report);     
        }

        // получить Заказ-заявку
        public void GetReportQueryClientPdf(string ID_order)
        {
            ReportOrderForClientCustomQueryModel report = new ReportOrderForClientCustomQueryModel();
            report.ServiceList = db.GetTotalInfoServicesByOrder(ID_order).ToList();
            report.Order = db.OrderServs.Where(f => f.ID_order == ID_order).FirstOrDefault();
            ///
            //try
            //{
            //    ViewBag.NameManagerComplete = db.ExecutingServices.Where(s => s.ID_order == ID_order && s.ID_service == "mngclose").FirstOrDefault().Employee.NameSerName.ConvertToShortName();
            //    ViewBag.PostManagerComplete = db.ExecutingServices.Where(s => s.ID_order == ID_order && s.ID_service == "mngclose").FirstOrDefault().Employee.Post.NamePost;
            //}
            //catch { }
            Html2Pdf.ConvertHtml2PdfResponse(this, "OrderForClientCustomQueryReport", report);
        }

        // получить Акт-сдачи приемки автомобиля
        public void GetReportAcceptanceCarPdf(string ID_order, string id_client)
        {
            
            ReportOrderForClientAcceptanceCarViewModel report = new ReportOrderForClientAcceptanceCarViewModel();
            report.PartCustomList = db.Get_Report_AcceptedCustomParts(ID_order, id_client).ToList();
            OrderServ order = db.OrderServs.Find(ID_order);
            report.Client = db.Clients.Find(id_client);
            report.Car = order.Car;
            report.OrderServ= order;
            report.Remarks = order.RemarkToStateCars.ToList();
            report.N = 1;
            try
            {
            report.NameManager = order.ExecutingServices.Where(m => m.ID_service == "mngopen" && m.ID_order == ID_order).FirstOrDefault().Employee.NameSerName;
            }
            catch { }
            Html2Pdf.ConvertHtml2PdfResponse(this, "OrderForClientAcceptanceCar", report);
        }

        // получить Акт-сдачи приемки запчастей
        public void GetReportAcceptanceCustomPartsPdf(int? ID_accept)
        {

            AcceptedPartsReportViewModel report = new AcceptedPartsReportViewModel();
            AcceptDocument doc = db.AcceptDocuments.Find(ID_accept);
            try
            {
                if (doc != null)
                { 
                    Client client= doc.Client;
                    report.NameClient = client.NameSerName.ConvertToShortName();
                    report.Phone = client.Phone;
                    report.Adress = client.Adress;
                    report.ID_doc = ID_accept.Value;
                    report.NameEmployee = doc.Employee.NameSerName.ConvertToShortName();
                    report.Date = doc.AcceptDate.Value;
                    report.SpareParts = doc.AcceptanceCustomSParts.ToList();
                }
            }
            catch { }
            Html2Pdf.ConvertHtml2PdfResponse(this, "ClientPartsAcceptReport", report);
        }
	}
}