﻿using AutoserviceDiplom.Models;
using AutoserviceDiplom.MyCustomClasses;
using PagedList;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace AutoserviceDiplom.Controllers
{
     [Authorize]
    public class PreRecordController : Controller
    {
        autoserviceDEntities db = new autoserviceDEntities();
        IdentityContext idb=new IdentityContext();
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PreRecordRecivedDataViewModel model_recived)
        {
            ApplicationUser user=  idb.Users.Where(i=>i.UserName==User.Identity.Name).FirstOrDefault();
            if (ModelState.IsValid&&user!=null)
            {
                string stringID_serv = string.Join("|", model_recived.Id_serv);
                var inputParams = new Dictionary<string, object> { 
                                                                    { "name_sern", model_recived.NameSerName}, 
                                                                    { "phone", model_recived.Phone_number??(object)DBNull.Value},
                                                                    { "mark_model", model_recived.MarkModelCar },
                                                                    { "issue_year", model_recived.IssueYear??(object)DBNull.Value},
                                                                    { "reg_nubm", model_recived.RegNumberCar??(object)DBNull.Value},
                                                                    { "pers_numb", user.PersonnelNumber},
                                                                    { "charArrayID_services", stringID_serv }
                                                                 };

                var outputParams = new Dictionary<string, object>{
                                                                    {"id_record",SqlDbType.BigInt}
                                                                 };
                ConnectionLayerDB dbConnection = new ConnectionLayerDB("autoserviceDB");
                int stat = dbConnection.ExecutionProcedure("AddPreRecord", inputParams,  outputParams);
                return RedirectToAction("Details", new { id_record = outputParams["id_record"] });
            }
            return View();
        }
        [HttpPost]
        public ActionResult SearchService(string servname, string id_serv)
        {
            List<Service> services;
            services = db.Services.Where(f => f.ServiceName.Contains(servname) && f.ID_service.Contains(id_serv)&&f.Available).Take(8).ToList();
            return PartialView("ServiceSearchPartial", services);
        }

        [HttpPost]
        public async Task<ActionResult> AttachService(string id_serv, long? id_record)
        {
            if (id_record.HasValue && id_serv != null)
            {
                if (db.PreRecordServices.Find(id_record, id_serv) == null)
                {
                   // PreRecordService pre_rec_serv= new PreRecordService{ID_record=id_record.Value, ID_service=id_serv};
                    db.Entry(new PreRecordService{ID_record=id_record.Value, ID_service=id_serv}).State=EntityState.Added;
                    await db.SaveChangesAsync();
                    List<PreRecordService> services = db.PreRecords.AsNoTracking().Where(g => g.ID_record ==id_record).FirstOrDefault().PreRecordServices.ToList();
                   return PartialView("ServicesAttachedPartial", services);
                }
                else
                    return HttpNotFound();  
            }
            else
            return HttpNotFound();      
        }

        [HttpPost]
        public ActionResult DeleteAttachService(string ID_service, long? id_record) 
        {
            if (ID_service != null && id_record.HasValue)
            {
                var preserv=db.PreRecordServices.Find(id_record, ID_service);
                if (preserv!= null&&preserv.DateReserv==null)
                {
                    db.PreRecordServices.Remove(preserv);
                    db.SaveChanges();
                    return null;
                }
                else return JavaScript("$('#popErorr').html('Невозможно удалить услугу, так как она уже  в таблице загрузок мастеров или удалена').slideDown(300).delay(1500).slideUp(300); ");
            }
            return HttpNotFound();
        }

        public ActionResult List()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Search(SearchPreRecordsViewModel model, int? page)
        {

            long recordIntervalDown, recordIntervalUp;
            if (model.ID_record.HasValue)/// для получения диапазона номеров записей, при Null в запросе
            {
                recordIntervalDown = model.ID_record.Value;
                recordIntervalUp = model.ID_record.Value;
            }
            else
            {
                recordIntervalDown = 0;
                recordIntervalUp = 10000000; 
            }
            var pre_records = (from resp in db.PreRecords.AsNoTracking() where (resp.NameSerName.Contains(model.NameClient) || string.IsNullOrEmpty(resp.NameSerName)) &&
                                                                (resp.RegNumberCar.Contains(model.RegNumber) || string.IsNullOrEmpty(resp.RegNumberCar)) &&
                                                                (resp.MarkModelCar.Contains(model.MarkCar) || string.IsNullOrEmpty(resp.MarkModelCar)) &&
                                                                resp.ID_record>=recordIntervalDown&&resp.ID_record<=recordIntervalUp&&
                                                                resp.DateMakingRecord>=model.DateMakingFrom&&resp.DateMakingRecord<=model.DateMakingBefore&&
                                                                (resp.IsRejection == (model.IsRejection ?? false) || resp.IsRejection == (model.IsRejection ?? true))
                                                        select resp).ToList();
            int pageSize = 6;
            int pageNumber = (page ?? 1);
            return PartialView("SearchResultPartial", pre_records.ToPagedList(pageNumber, pageSize));
        }

        public ActionResult DeleteRecord(long? ID_record)
        {
            if(ID_record.HasValue)
            {
                var record = db.PreRecords.Find(ID_record);
                if (record != null)
                {
                    db.PreRecords.Remove(record);
                    db.SaveChanges();
                    if (Request.UrlReferrer.AbsolutePath == "/PreRecord/Details")
                        return JavaScript("window.location.href='/PreRecord/List';");
                    else
                        return JavaScript("window.location.reload();");
                }
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        public ActionResult Details(long? id_record)
        {
            PreRecord preRecord = db.PreRecords.Find(id_record);
            if (preRecord != null)
            {
                return View(preRecord);
            }
            else
                return HttpNotFound();
        }

        public ActionResult CastToOrder(long? id_record)
        {
            var record = db.PreRecords.Find(id_record);
            if (record!=null)
            {
                ViewBag.Id_record = id_record;
                string[] namesClient = record.NameSerName.ConvertToStringNames();// имена раздельно
                string serName, name;
                List<Client> targetClients, unionSetClients;
                ClientComparer clntComparer = new ClientComparer();
                serName= namesClient[0];
                name=namesClient[1];
                targetClients = (from rez in db.Clients where rez.NameSerName.Contains(serName) && rez.NameSerName.Contains(name) select rez).ToList();//содержит ли поле NameSerName имя и фамилию
                if (targetClients.Count == 0)// если нет, то проверяем по 3 м буквам фамилии 
                {
                    string subStringName;
                    int sizePart = serName.Length/3;

                    if (serName.Length >= 3)//если фамилия не менее 3 символов
                    {
                        if (sizePart < 3)
                            sizePart = 3;
                        subStringName = serName.Substring(0, sizePart);
                        unionSetClients = (from rez in db.Clients where rez.NameSerName.Contains(subStringName) select rez).ToList();
                        for (int n = sizePart; n < serName.Length - sizePart; n += sizePart) //разделяем фамилию на три части и ищем вхождения по каждой части объединяя наборы данных в один
                        {
                            subStringName = serName.Substring(n, sizePart);
                            unionSetClients = unionSetClients.Union((from rez in db.Clients where rez.NameSerName.Contains(subStringName) select rez).ToList(), clntComparer).ToList();
                        }
                    }
                    else
                    {
                        subStringName = serName;
                        unionSetClients = (from rez in db.Clients where rez.NameSerName.Contains(subStringName) select rez).ToList();
                    }
                    if (unionSetClients.Count >= 1)// если нашли по части фамилии, то показываем пользователю варианты для выбора
                        return PartialView("ListClients", unionSetClients.Take(6));
                    else
                        return JavaScript("javascript:window.location.href='"+Url.Action("CreateNewClient", "Client", new { id_record = id_record })+"'");// если нет вариантов, то переходим сразу на регистрацию нового клиента
                }
                else
                    if (targetClients.Count > 0)// если сразу нашли по имени и фамилии, то показываем пользователю кого нашли
                        return PartialView("ListClients", targetClients);
                    else
                        return null;
            }
            else
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        public ActionResult ChangeStatlRecord(long? id_pre_record)
        {
            PreRecord record = db.PreRecords.Find(id_pre_record);
            if (record != null)
            {
                if (record.IsRejection.Value)
                {
                    record.IsRejection = false;
                    db.Entry(record).State = EntityState.Modified;
                    db.SaveChanges();
                }
                else
                {
                    record.IsRejection = true;
                    db.CancelPreRecord(record.ID_record);
                }

                return View("Details", record);
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
	}
    class ClientComparer:IEqualityComparer<Client>// компаратор для сравнения объектов модели Client
    {
        public bool Equals(Client _first, Client _second)
        {
            return _first.ID_client==_second.ID_client?true:false;
        }
        public int GetHashCode(Client obj)
        {
            return obj.GetHashCode();
        }
    }
}